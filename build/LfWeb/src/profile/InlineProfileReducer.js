"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
// import assoc from "ramda/src/assoc";
// import assocPath from "ramda/src/assocPath";
// import pipe from "ramda/src/pipe";
const ramda_1 = require("ramda");
const InlineProfileState_1 = require("./InlineProfileState");
function MakeInlineProfileReducer() {
    return (state = new InlineProfileState_1.InlineProfileState(), action) => {
        switch (action.type) {
            case "ApiGetUserGoalSuccess":
                return ramda_1.assocPath(["memberSummary", "goal"], action.goal, state);
            case "UserViewsInlineProfile":
                return ramda_1.pipe(ramda_1.assoc("activeMemberId", action.memberId), ramda_1.assoc("isLoading", true))(new InlineProfileState_1.InlineProfileState());
            case "ApiGetMemberSummarySuccess":
                return ramda_1.pipe(ramda_1.assoc("memberSummary", action.memberSummary), ramda_1.assoc("isLoading", false))(state);
            case "UserSendsMessage":
                return ramda_1.pipe(ramda_1.assocPath(["messageModal", "isSendingMessage"], true), ramda_1.assocPath(["messageModal", "messageSent"], false))(state);
            case "ApiPostMessageSuccess":
                return ramda_1.pipe(ramda_1.assocPath(["messageModal", "isSendingMessage"], false), ramda_1.assocPath(["messageModal", "message"], ""), ramda_1.assocPath(["messageModal", "messageSent"], true))(state);
            case "UserTypesInMessageInput":
                return ramda_1.assocPath(["messageModal", "message"], action.message, state);
            case "UserOpensMessageModal":
                return ramda_1.assocPath(["messageModal", "isModalVisible"], true, state);
            case "UserClosesMessageModal":
                return ramda_1.pipe(ramda_1.assocPath(["messageModal", "isModalVisible"], false), ramda_1.assocPath(["messageModal", "messageSent"], false))(state);
            default:
                return state;
        }
    };
}
exports.MakeInlineProfileReducer = MakeInlineProfileReducer;

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.apiGetLogDetailsSuccess = (logDetails) => {
    return {
        type: "ApiGetLogDetailsSuccess",
        logDetails,
    };
};
exports.apiGetTimelineSuccess = (timeline) => {
    return {
        type: "ApiGetTimelineSuccess",
        timeline,
    };
};
exports.userSelectsTimelineFilter = (timelineFilter) => {
    return {
        type: "UserSelectsTimelineFilter",
        timelineFilter,
    };
};
exports.userViewsInlineLogDetails = (logIndex) => {
    return {
        type: "UserViewsInlineLogDetails",
        logIndex,
    };
};
exports.userViewsMoreTimeline = () => {
    return {
        type: "UserViewsMoreTimeline",
    };
};

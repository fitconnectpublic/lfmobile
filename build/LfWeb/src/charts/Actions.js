"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.apiGetChartSuccess = (chart) => {
    return {
        type: "ApiGetChartSuccess",
        chart,
    };
};
exports.userSelectsChart = (chartId) => {
    return {
        type: "UserSelectsChart",
        chartId,
    };
};
exports.userSelectsChartGranularity = (chartGranularity) => {
    return {
        type: "UserSelectsChartGranularity",
        chartGranularity,
    };
};

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const redux_1 = require("redux");
const ExerciseReportsReducer_1 = require("../../src/widgets/reports/ExerciseReportsReducer");
const InlineProfileChartReducer_1 = require("../../src/widgets/charts/InlineProfileChartReducer");
const LeaderboardReducer_1 = require("../../src/widgets/leaderboard/LeaderboardReducer");
const InlineProfileTimelineReducer_1 = require("../../src/widgets/timeline/InlineProfileTimelineReducer");
const SelectSiteReducer_1 = require("../../src/widgets/sites/SelectSiteReducer");
const react_localize_redux_1 = require("react-localize-redux");
const InlineProfileReducer_1 = require("../../src/widgets/profile/InlineProfileReducer");
const InlineLogDetailsReducer_1 = require("../../src/widgets/timeline/InlineLogDetailsReducer");
const PageReducer_1 = require("../../src/widgets/routing/PageReducer");
const ApiKeysReducer_1 = require("../../src/widgets/apiKeys/ApiKeysReducer");
const PromotionReducer_1 = require("../../src/widgets/promotions/PromotionReducer");
function MakeRootReducer() {
    return redux_1.combineReducers({
        locale: react_localize_redux_1.localeReducer,
        ExerciseReports: ExerciseReportsReducer_1.MakeExerciseReportsReducer(),
        SelectSite: SelectSiteReducer_1.MakeSelectSiteReducer(),
        InlineProfile: InlineProfileReducer_1.MakeInlineProfileReducer(),
        InlineLogDetails: InlineLogDetailsReducer_1.MakeInlineLogDetailsReducer(),
        LeaderBoard: LeaderboardReducer_1.MakeLeaderboardReducer(),
        Chart: InlineProfileChartReducer_1.MakeInlineProfileChartReducer(),
        Timeline: InlineProfileTimelineReducer_1.MakeInlineProfileTimelineReducer(),
        Page: PageReducer_1.MakePageReducer(),
        ApiKeys: ApiKeysReducer_1.MakeApiKeysReducer(),
        Promotion: PromotionReducer_1.MakePromotionReducer(),
    });
}
exports.MakeRootReducer = MakeRootReducer;

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.TReportCategoryIds = [
    "AllExercisers",
    "ExerciserInsights",
    "GoalReports",
    "AccountReports",
    "Heatmap",
];

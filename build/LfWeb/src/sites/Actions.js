"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.apiGetSitesSuccess = (sites) => {
    return {
        type: "ApiGetSitesSuccess",
        sites,
    };
};
exports.userSelectsSite = (siteId) => {
    return {
        type: "UserSelectsSite",
        siteId,
    };
};

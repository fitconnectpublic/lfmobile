"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.apiAuthWithFirebaseSuccess = (apiKey, firebaseToken) => {
    return {
        type: "ApiAuthWithFirebaseSuccess",
        apiKey,
        firebaseToken,
    };
};

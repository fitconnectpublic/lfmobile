"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Observable_1 = require("rxjs/Observable");
require("rxjs/add/operator/switchMap");
require("rxjs/add/operator/filter");
require("rxjs/add/operator/map");
require("rxjs/add/operator/catch");
require("rxjs/add/observable/empty");
// import propEq from "ramda/src/propEq";
// import contains from "ramda/src/contains";
// import values from "ramda/src/values";
// import reject from "ramda/src/reject";
// import equals from "ramda/src/equals";
// import assocPath from "ramda/src/assocPath";
// import invertObj from "ramda/src/invertObj";
const ramda_1 = require("ramda");
// Misc
const fetchFitconnectAndReturnJson_1 = require("../lib/fetchFitconnectAndReturnJson");
// Actions
const Actions_1 = require("./Actions");
const TReportId_1 = require("../../src/models/TReportId");
const IReportResponse_IReport_1 = require("../../src/maps/IReportResponse_IReport");
exports.getReportEpic = (action$, stateMiddleware) => {
    return action$
        .filter((action) => {
        return (action.type === "UserSelectsReportCategory"
            || action.type === "UserSelectsSite"
            || action.type === "UserSelectsReport"
            || action.type === "ApiGetSitesSuccess"
            || action.type === "GoToNextExerciseReportPage"
            || action.type === "GoToPreviousExerciseReportPageAction"
            || action.type === "GoToSpecificExerciseReportPageAction"
            || action.type === "GoToFirstExerciseReportPage"
            || action.type === "GoToLastExerciseReportPage");
    })
        .filter((action) => {
        const state = stateMiddleware.getState();
        return state.SelectSite.selectedSiteId !== 0;
    })
        .filter((action) => {
        return ramda_1.contains(stateMiddleware.getState().ExerciseReports.reportControls.selectedReportNumericId, ramda_1.reject(ramda_1.equals(-1), ramda_1.values(TReportId_1.TReportIdToTReportNumericId)));
    })
        .switchMap(() => {
        const state = stateMiddleware.getState();
        const siteId = state.SelectSite.selectedSiteId;
        const reportId = state.ExerciseReports.reportControls.selectedReportNumericId;
        const offset = state.ExerciseReports.reportControls.pageIndex * state.ExerciseReports.reportControls.pageSize;
        const size = state.ExerciseReports.reportControls.pageSize;
        const reportIdName = ramda_1.invertObj(TReportId_1.TReportIdToTReportNumericId)[reportId];
        return fetchFitconnectAndReturnJson_1.fetchFitconnectAndReturnJson(state.ApiKeys.apiKey)(`/v3/sites/${siteId}/reports/${reportId}?$skip=${offset}&$top=${size}`).map(ramda_1.assocPath(["body", "reportId"], reportIdName))
            .filter(ramda_1.propEq("status", 200))
            .map((response) => {
            return IReportResponse_IReport_1.IReportResponse_IReport(response.body);
        })
            .catch(() => Observable_1.Observable.empty());
    })
        .map(Actions_1.apiGetReportsSuccess);
};

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const map = {
    NumberOfWorkouts: "number-of-workouts",
    CaloriesConsumed: "calories-consumed",
    Steps: "steps",
    Sleep: "sleep",
    WorkoutTime: "workout-time",
    Weight: "weight",
    BodyFat: "body-fat",
};
exports.TChartId_TChartTypeRequest = (chartId) => {
    const defaultChartTypeRequest = "number-of-workouts";
    try {
        if (map.hasOwnProperty(chartId)) {
            return map[chartId];
        }
        return defaultChartTypeRequest;
    }
    catch (e) {
        return defaultChartTypeRequest;
    }
};

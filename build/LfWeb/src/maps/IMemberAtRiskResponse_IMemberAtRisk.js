"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mergeDefaultAndResponse_1 = require("./../lib/mergeDefaultAndResponse");
const IMemberAtRiskResponse_1 = require("./../../../src/responses/IMemberAtRiskResponse");
exports.IMemberAtRiskResponse_IMemberAtRisk = (value) => {
    const validValue = mergeDefaultAndResponse_1.mergeDefaultAndResponse(new IMemberAtRiskResponse_1.IMemberAtRiskResponse(), value);
    return {
        id: validValue.attributes.data["_userid_"],
        firstName: validValue.attributes.data["First Name"],
        lastName: validValue.attributes.data["Last Name"],
        last30Days: validValue.attributes.data["Last 30 days_chartx_"],
        last30To60Days: validValue.attributes.data["30 - 60 Days"],
        reductionInWorkouts: validValue.attributes.data["Reduction In Workouts_charty_"],
        bestMonth: validValue.attributes.data["Best Month"],
        lastActivity: validValue.attributes.data["Last Activity"],
    };
};

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mergeDefaultAndResponse_1 = require("./../lib/mergeDefaultAndResponse");
const IMemberNewSignupResponse_1 = require("./../../../src/responses/IMemberNewSignupResponse");
exports.IMemberNewSignupResponse_IMemberNewSignup = (value) => {
    const validValue = mergeDefaultAndResponse_1.mergeDefaultAndResponse(new IMemberNewSignupResponse_1.IMemberNewSignupResponse(), value);
    return {
        id: validValue.attributes.data["_userid_"],
        firstName: validValue.attributes.data["First Name"],
        lastName: validValue.attributes.data["Last Name"],
        signUpDay: validValue.attributes.data["Sign Up Day"],
        site: validValue.attributes.data["Site"],
    };
};

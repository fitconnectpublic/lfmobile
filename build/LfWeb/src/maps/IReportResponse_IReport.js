"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mergeDefaultAndResponse_1 = require("./../lib/mergeDefaultAndResponse");
// Models
const IReportResponse_1 = require("../../../src/responses/IReportResponse");
// Maps
const IMemberTrainingForAnEventResponse_IMemberTrainingForAnEvent_1 = require("./../maps/IMemberTrainingForAnEventResponse_IMemberTrainingForAnEvent");
const IMemberDoingWellResponse_IMemberDoingWell_1 = require("./../maps/IMemberDoingWellResponse_IMemberDoingWell");
const IMemberGetInShapeResponse_IMemberGetInShape_1 = require("./../maps/IMemberGetInShapeResponse_IMemberGetInShape");
const IMemberImproveHealthResponse_IMemberImproveHealth_1 = require("./../maps/IMemberImproveHealthResponse_IMemberImproveHealth");
const IMemberLoseWeightResponse_IMemberLoseWeight_1 = require("./../maps/IMemberLoseWeightResponse_IMemberLoseWeight");
const IMemberNewSignupResponse_IMemberNewSignup_1 = require("./../maps/IMemberNewSignupResponse_IMemberNewSignup");
const IMemberNotProgressingResponse_IMemberNotProgressing_1 = require("./../maps/IMemberNotProgressingResponse_IMemberNotProgressing");
const IMemberRewardPointsResponse_IMemberRewardPoints_1 = require("./../maps/IMemberRewardPointsResponse_IMemberRewardPoints");
const IMemberAtRiskResponse_IMemberAtRisk_1 = require("./../maps/IMemberAtRiskResponse_IMemberAtRisk");
const IMemberBuildMuscleResponse_IMemberBuildMuscle_1 = require("./../maps/IMemberBuildMuscleResponse_IMemberBuildMuscle");
const IMemberAbandonedAccountResponse_IMemberAbandonedAccount_1 = require("./../maps/IMemberAbandonedAccountResponse_IMemberAbandonedAccount");
exports.IReportResponse_IReport = (value) => {
    const validValue = mergeDefaultAndResponse_1.mergeDefaultAndResponse(new IReportResponse_1.IReportResponse(), value);
    switch (validValue.reportId) {
        case "TrainingForAnEvent":
            return {
                members: validValue.data.map(IMemberTrainingForAnEventResponse_IMemberTrainingForAnEvent_1.IMemberTrainingForAnEventResponse_IMemberTrainingForAnEvent),
                totalCount: validValue.meta.pagination.total,
                reportId: validValue.reportId,
            };
        case "ImproveHealth":
            return {
                members: validValue.data.map(IMemberImproveHealthResponse_IMemberImproveHealth_1.IMemberImproveHealthResponse_IMemberImproveHealth),
                totalCount: validValue.meta.pagination.total,
                reportId: validValue.reportId,
            };
        case "AtRisk":
            return {
                members: validValue.data.map(IMemberAtRiskResponse_IMemberAtRisk_1.IMemberAtRiskResponse_IMemberAtRisk),
                totalCount: validValue.meta.pagination.total,
                reportId: validValue.reportId,
            };
        case "LoseWeight":
            return {
                members: validValue.data.map(IMemberLoseWeightResponse_IMemberLoseWeight_1.IMemberLoseWeightResponse_IMemberLoseWeight),
                totalCount: validValue.meta.pagination.total,
                reportId: validValue.reportId,
            };
        case "RewardPoints":
            return {
                members: validValue.data.map(IMemberRewardPointsResponse_IMemberRewardPoints_1.IMemberRewardPointsResponse_IMemberRewardPoints),
                totalCount: validValue.meta.pagination.total,
                reportId: validValue.reportId,
            };
        case "GetInShape":
            return {
                members: validValue.data.map(IMemberGetInShapeResponse_IMemberGetInShape_1.IMemberGetInShapeResponse_IMemberGetInShape),
                totalCount: validValue.meta.pagination.total,
                reportId: validValue.reportId,
            };
        case "NotProgressing":
            return {
                members: validValue.data.map(IMemberNotProgressingResponse_IMemberNotProgressing_1.IMemberNotProgressingResponse_IMemberNotProgressing),
                totalCount: validValue.meta.pagination.total,
                reportId: validValue.reportId,
            };
        case "DoingWell":
            return {
                members: validValue.data.map(IMemberDoingWellResponse_IMemberDoingWell_1.IMemberDoingWellResponse_IMemberDoingWell),
                totalCount: validValue.meta.pagination.total,
                reportId: validValue.reportId,
            };
        case "BuildMuscle":
            return {
                members: validValue.data.map(IMemberBuildMuscleResponse_IMemberBuildMuscle_1.IMemberBuildMuscleResponse_IMemberBuildMuscle),
                totalCount: validValue.meta.pagination.total,
                reportId: validValue.reportId,
            };
        case "NewSignup":
            return {
                members: validValue.data.map(IMemberNewSignupResponse_IMemberNewSignup_1.IMemberNewSignupResponse_IMemberNewSignup),
                totalCount: validValue.meta.pagination.total,
                reportId: validValue.reportId,
            };
        case "AbandonedAccounts":
            return {
                members: validValue.data.map(IMemberAbandonedAccountResponse_IMemberAbandonedAccount_1.IMemberAbandonedAccountResponse_IMemberAbandonedAccount),
                totalCount: validValue.meta.pagination.total,
                reportId: validValue.reportId,
            };
        default:
            return {
                members: [],
                totalCount: validValue.meta.pagination.total,
                reportId: validValue.reportId,
            };
    }
};

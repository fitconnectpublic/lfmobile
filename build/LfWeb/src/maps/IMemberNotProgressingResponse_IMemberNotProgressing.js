"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mergeDefaultAndResponse_1 = require("./../lib/mergeDefaultAndResponse");
const IMemberNotProgressingResponse_1 = require("./../../../src/responses/IMemberNotProgressingResponse");
exports.IMemberNotProgressingResponse_IMemberNotProgressing = (value) => {
    const validValue = mergeDefaultAndResponse_1.mergeDefaultAndResponse(new IMemberNotProgressingResponse_1.IMemberNotProgressingResponse(), value);
    if (validValue.attributes.data.Goal_goaltext_) {
        validValue.attributes.data.goal = JSON.parse(validValue.attributes.data.Goal_goaltext_);
    }
    return {
        id: validValue.attributes.data["_userid_"],
        firstName: validValue.attributes.data["First Name"],
        lastName: validValue.attributes.data["Last Name"],
        goal: {
            type: validValue.attributes.data["goal"]["type"],
        },
        goalProgress: validValue.attributes.data["Goal Progress"],
        daysSinceProgress: validValue.attributes.data["Days Since Progress"],
        lastRelatedActivity: validValue.attributes.data["Last Related Activity"],
    };
};

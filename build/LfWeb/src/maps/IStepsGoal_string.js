"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.IStepsGoal_string = (goal, translate /* tslint:enable */) => {
    return translate("InsightsWidget.UserGoal.Steps.Goal", {
        steps: goal.stepsgoal,
    });
};

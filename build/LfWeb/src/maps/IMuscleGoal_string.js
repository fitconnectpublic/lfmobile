"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.IMuscleGoal_string = (goal, translate /* tslint:enable */) => {
    if (goal.muscleIncrease) {
        return translate("InsightsWidget.UserGoal.Muscle.GoalWithMuscle", {
            percent: parseInt(goal.muscleIncrease, 10) * 100,
        });
    }
    else {
        return translate("InsightsWidget.UserGoal.Muscle.GoalNoMuscle");
    }
};

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mergeDefaultAndResponse_1 = require("./../lib/mergeDefaultAndResponse");
const IMemberImproveHealthResponse_1 = require("./../../../src/responses/IMemberImproveHealthResponse");
exports.IMemberImproveHealthResponse_IMemberImproveHealth = (value) => {
    const validValue = mergeDefaultAndResponse_1.mergeDefaultAndResponse(new IMemberImproveHealthResponse_1.IMemberImproveHealthResponse(), value);
    return {
        id: validValue.attributes.data["_userid_"],
        firstName: validValue.attributes.data["First Name"],
        lastName: validValue.attributes.data["Last Name"],
        wantsToImprove: validValue.attributes.data["_filterselect_Wants To Improve"],
        target: validValue.attributes.data["Target"],
        goalProgress: validValue.attributes.data["Goal Progress"],
    };
};

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ITransformGoal_string = (goal, translate /* tslint:enable */) => {
    if (goal.bodFatDecrease) {
        return translate("InsightsWidget.UserGoal.Transform.GoalWithBodyFat", {
            percent: parseInt(goal.bodFatDecrease, 10) * 100,
        });
    }
    else {
        return translate("InsightsWidget.UserGoal.Transform.GoalNoBodyFat");
    }
};

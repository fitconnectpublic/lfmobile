"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mergeDefaultAndResponse_1 = require("./../lib/mergeDefaultAndResponse");
const IMemberLoseWeightResponse_1 = require("./../../../src/responses/IMemberLoseWeightResponse");
exports.IMemberLoseWeightResponse_IMemberLoseWeight = (value) => {
    const validValue = mergeDefaultAndResponse_1.mergeDefaultAndResponse(new IMemberLoseWeightResponse_1.IMemberLoseWeightResponse(), value);
    return {
        id: validValue.attributes.data["_userid_"],
        firstName: validValue.attributes.data["First Name"],
        lastName: validValue.attributes.data["Last Name"],
        weightLoseAmount: validValue.attributes.data["Weight Lose Amount"],
        goalProgress: validValue.attributes.data["Goal Progress"],
        needsHelpWith: validValue.attributes.data["_filterselect_Needs help with"],
    };
};

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const ramda_1 = require("ramda");
const moment = require("moment");
exports.IBarChartResponse_IBarChart = (response, chartGranularity) => {
    return {
        values: response.data.attributes.yAxis,
        labels: ramda_1.map((label) => {
            switch (chartGranularity) {
                case "days":
                    return moment(label).format("dddd");
                case "weeks":
                    return moment(label).format("MMM Do");
                default:
                    return moment(label).format("MMM");
            }
        }, response.data.attributes.xAxis),
    };
};

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mergeDefaultAndResponse_1 = require("./../lib/mergeDefaultAndResponse");
const IMemberBuildMuscleResponse_1 = require("./../../../src/responses/IMemberBuildMuscleResponse");
exports.IMemberBuildMuscleResponse_IMemberBuildMuscle = (value) => {
    const validValue = mergeDefaultAndResponse_1.mergeDefaultAndResponse(new IMemberBuildMuscleResponse_1.IMemberBuildMuscleResponse(), value);
    return {
        id: validValue.attributes.data["_userid_"],
        firstName: validValue.attributes.data["First Name"],
        lastName: validValue.attributes.data["Last Name"],
        muscleMassIncrease: validValue.attributes.data["Muscle Mass Increase"],
        needsHelpWith: validValue.attributes.data["Needs help with"],
        progress: validValue.attributes.data["Progress"],
    };
};

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
// import uniq from "ramda/src/uniq";
const ramda_1 = require("ramda");
const round_1 = require("../lib/round");
const mergeDefaultAndResponse_1 = require("./../lib/mergeDefaultAndResponse");
const ConnectedAppIcons_1 = require("./../assets/ConnectedAppIcons");
const ITimelineItemResponse_1 = require("./../../../src/responses/ITimelineItemResponse");
const TTimelineItemType_1 = require("./../../../src/responses/TTimelineItemType");
const TGoalJson_string_1 = require("./TGoalJson_string");
exports.ITimelineItemResponse_ITimelineItem = (value, translate /* tslint:enable */) => {
    const validValue = mergeDefaultAndResponse_1.mergeDefaultAndResponse(new ITimelineItemResponse_1.ITimelineItemResponse(), value);
    validValue.attributes = mergeDefaultAndAttributes(new ITimelineItemResponse_1.ITimelineItemAttributes(), validValue.attributes);
    return {
        type: TTimelineItemType_1.TTimelineItemTypeToTTimelineItemTypeText[getLogType(validValue)],
        appImages: getIcons(validValue),
        summary: createSummary(validValue, translate),
        timestamp: validValue.attributes.timestamp,
        // responsibleTable: validValue.responsible_table,
        // responsibleId: validValue.responsible_id.replace("{", "").replace("}", ""),
        responsibleId: validValue.id.toString(),
        payload: validValue.attributes,
    };
};
const mergeDefaultAndAttributes = (defaultResponse, value /* tslint:enable */) => {
    const attributes = value;
    // recursion to merge children in attributes
    if (attributes["children"] && attributes.children.length > 0) {
        for (let index = 0; index < attributes.children.length; index++) {
            const child = attributes.children[index];
            child.attributes = mergeDefaultAndAttributes(new ITimelineItemResponse_1.ITimelineItemAttributes(), child.attributes);
        }
    }
    switch (attributes.logType) {
        case "WEIGHT":
            attributes.data = mergeDefaultAndResponse_1.mergeDefaultAndResponse(new ITimelineItemResponse_1.ITimelineItemWeightPayload(), attributes.data);
            break;
        case "ROUTINE":
            attributes.data = mergeDefaultAndResponse_1.mergeDefaultAndResponse(new ITimelineItemResponse_1.ITimelineItemRoutinePayload(), attributes.data);
            break;
        case "SLEEP":
            attributes.data = mergeDefaultAndResponse_1.mergeDefaultAndResponse(new ITimelineItemResponse_1.ITimelineItemSleepPayload(), attributes.data);
            break;
        case "HEARTRATE":
            attributes.data = mergeDefaultAndResponse_1.mergeDefaultAndResponse(new ITimelineItemResponse_1.ITimelineItemHeartRatePayload(), attributes.data);
            break;
        case "NUTRITION":
            attributes.data = mergeDefaultAndResponse_1.mergeDefaultAndResponse(new ITimelineItemResponse_1.ITimelineItemWeightPayload(), attributes.data);
            break;
        case "FITNESS":
            switch (attributes.logSubType) {
                case "RUN":
                    attributes.data = mergeDefaultAndResponse_1.mergeDefaultAndResponse(new ITimelineItemResponse_1.ITimelineItemFitnessRunPayload(), attributes.data);
                    break;
                case "CYCLE":
                    attributes.data = mergeDefaultAndResponse_1.mergeDefaultAndResponse(new ITimelineItemResponse_1.ITimelineItemFitnessCyclePayload(), attributes.data);
                    break;
                case "SWIM":
                    attributes.data = mergeDefaultAndResponse_1.mergeDefaultAndResponse(new ITimelineItemResponse_1.ITimelineItemFitnessSwimPayload(), attributes.data);
                    break;
                case "WALK":
                    attributes.data = mergeDefaultAndResponse_1.mergeDefaultAndResponse(new ITimelineItemResponse_1.ITimelineItemFitnessWalkPayload(), attributes.data);
                    break;
                case "STRENGTH":
                    attributes.data = mergeDefaultAndResponse_1.mergeDefaultAndResponse(new ITimelineItemResponse_1.ITimelineItemFitnessStrengthPayload(), attributes.data);
                    break;
                default:
                    break;
            }
            break;
        case "FITNESS_STRENGTH":
            attributes.data = mergeDefaultAndResponse_1.mergeDefaultAndResponse(new ITimelineItemResponse_1.ITimelineItemFitnessStrengthSetPayload(), attributes.data);
            break;
        case "MESSAGE":
            attributes.data = mergeDefaultAndResponse_1.mergeDefaultAndResponse(new ITimelineItemResponse_1.ITimelineItemMessagePayload(), attributes.data);
        default:
            break;
    }
    return attributes;
};
const createSummary = (timelineItem, translate) => {
    let summary = "";
    switch (timelineItem.attributes.logType) {
        case "GOAL":
            const goalPayload = timelineItem.attributes.data;
            summary = TGoalJson_string_1.TGoalJson_string(goalPayload, translate);
            break;
        case "MESSAGE":
            const messagePayload = timelineItem.attributes.data;
            summary = messagePayload.message;
            break;
        case "FITNESS":
            // Fitness has many subtypes
            switch (timelineItem.attributes.logSubType) {
                case "RUN":
                    const runPayload = timelineItem.attributes.data;
                    summary = translate("InsightsWidget.ProfileTimeline.Summary.Fitness.Run", {
                        distance: round_1.round(runPayload.distance / 1000, 1),
                        unit: "km",
                        minutes: round_1.round(runPayload.duration / 60),
                    });
                    break;
                case "CYCLE":
                    const cyclePayload = timelineItem.attributes.data;
                    summary = translate("InsightsWidget.ProfileTimeline.Summary.Fitness.Cycle", {
                        distance: round_1.round(cyclePayload.distance / 1000, 1),
                        unit: "km",
                        minutes: round_1.round(cyclePayload.duration / 60),
                    });
                    break;
                case "SWIM":
                    const swimPayload = timelineItem.attributes.data;
                    summary = translate("InsightsWidget.ProfileTimeline.Summary.Fitness.Swim", {
                        distance: round_1.round(swimPayload.distance / 1000, 1),
                        unit: "km",
                        minutes: round_1.round(swimPayload.duration / 60),
                    });
                    break;
                case "WALK":
                    const walkPayload = timelineItem.attributes.data;
                    summary = translate("InsightsWidget.ProfileTimeline.Summary.Fitness.Walk", {
                        distance: round_1.round(walkPayload.distance / 1000, 1),
                        unit: "km",
                        minutes: round_1.round(walkPayload.duration / 60),
                    });
                    break;
                case "STRENGTH":
                    const strengthPayload = timelineItem.attributes.data;
                    summary = translate("InsightsWidget.ProfileTimeline.Summary.Fitness.Strength", {
                        minutes: round_1.round(strengthPayload.duration / 60),
                        calories: round_1.round(strengthPayload.calories, 1),
                    });
                default:
                    const fitnessPayload = timelineItem.attributes.data;
                    summary = translate("InsightsWidget.ProfileTimeline.Summary.Fitness", {
                        minutes: round_1.round(fitnessPayload.duration / 60),
                        calories: round_1.round(fitnessPayload.calories, 1),
                    });
                    break;
            }
            break;
        case "WEIGHT":
            const weightPayload = timelineItem.attributes.data;
            summary = translate("InsightsWidget.ProfileTimeline.Summary.Weight", {
                unit: "kg",
                weight: weightPayload.weight,
            });
            break;
        case "ROUTINE":
            const routinePayload = timelineItem.attributes.data;
            summary = translate("InsightsWidget.ProfileTimeline.Summary.Routine", {
                steps: routinePayload.steps,
            });
            break;
        case "SLEEP":
            const sleepPayload = timelineItem.attributes.data;
            summary = translate("InsightsWidget.ProfileTimeline.Summary.Sleep", {
                hours: Math.floor(sleepPayload.sleepLength / 3600),
                minutes: round_1.round((sleepPayload.sleepLength % 3600) / 60),
            });
            break;
        case "HEARTRATE":
            summary = translate("InsightsWidget.ProfileTimeline.Summary.HeartRate");
            break;
        case "GROUP":
            const groupSubLogType = timelineItem.attributes.children[0].attributes.logType;
            const groupSubLogSubType = timelineItem.attributes.children[0].attributes.logSubType;
            if (groupSubLogType === "NUTRITION") {
                // Summary must sum values for the children
                summary = translate("InsightsWidget.ProfileTimeline.Summary.Nutrition", {
                    calories: ramda_1.reduce((sum, child) => round_1.round(child.attributes.data.calories / 1) + sum, 0, timelineItem.attributes.children),
                });
            }
            else if (groupSubLogType === "FITNESS" && groupSubLogSubType === "STRENGTH") {
                // Summary must sum values for the children
                summary = translate("InsightsWidget.ProfileTimeline.Summary.Fitness.Strength", {
                    calories: ramda_1.reduce((sum, child) => round_1.round(child.attributes.data.calories / 1) + sum, 0, timelineItem.attributes.children),
                    minutes: ramda_1.reduce((sum, child) => round_1.round(child.attributes.data.duration / 60) + sum, 0, timelineItem.attributes.children),
                });
            }
            else if (groupSubLogType === "FITNESS" && groupSubLogSubType === "ACTIVITY") {
                // Summary must sum values for the children
                summary = translate("InsightsWidget.ProfileTimeline.Summary.Fitness", {
                    calories: ramda_1.reduce((sum, child) => round_1.round(child.attributes.data.calories / 1) + sum, 0, timelineItem.attributes.children),
                    minutes: ramda_1.reduce((sum, child) => round_1.round(child.attributes.data.duration / 60) + sum, 0, timelineItem.attributes.children),
                });
            }
    }
    return summary;
};
const getIcons = (timelineItem) => {
    if (timelineItem.attributes.logType === "GROUP") {
        // Return a list of unique app icons from the children of a group
        return ramda_1.uniq(ramda_1.reduce((arr, child) => ramda_1.append(ConnectedAppIcons_1.ConnectedStringAppIcons[child.attributes.app], arr), [], timelineItem.attributes.children));
    }
    else if (ConnectedAppIcons_1.ConnectedStringAppIcons[timelineItem.attributes.app]) {
        return [ConnectedAppIcons_1.ConnectedStringAppIcons[timelineItem.attributes.app]];
    }
    else {
        return [];
    }
};
const getLogType = (timelineItem) => {
    if (timelineItem.attributes.logType === "GROUP") {
        return timelineItem.attributes.children[0].attributes.logType;
    }
    else {
        return timelineItem.attributes.logType;
    }
};

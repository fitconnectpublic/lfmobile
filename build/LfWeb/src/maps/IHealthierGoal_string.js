"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.IHealthierGoal_string = (goal, translate /* tslint:enable */) => {
    if (goal.subtype === "steps") {
        return translate("InsightsWidget.UserGoal.Healthier.GoalSteps", {
            steps: goal.stepsNumber,
        });
    }
    else if (goal.subtype === "workouts") {
        return translate("InsightsWidget.UserGoal.Healthier.GoalWorkouts", {
            workouts: goal.workoutsNumber,
        });
    }
    else if (goal.subtype === "sleep") {
        return translate("InsightsWidget.UserGoal.Healthier.GoalSleep", {
            sleep_hours: goal.sleepHours,
        });
    }
    else if (goal.subtype === "eating") {
        return translate("InsightsWidget.UserGoal.Healthier.GoalEating", {
            calories: goal.eatingCalories,
        });
    }
    else {
        return translate("InsightsWidget.UserGoal.Healthier.GoalGeneral");
    }
};

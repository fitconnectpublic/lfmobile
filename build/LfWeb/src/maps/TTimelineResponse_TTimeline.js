"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const ITimelineItemResponse_ITimelineItem_1 = require("./ITimelineItemResponse_ITimelineItem");
const ramda_1 = require("ramda");
const moment = require("moment");
exports.TTimelineResponse_TTimeline = (response, translate /* tslint:enable */) => {
    let safeResponse = response.data;
    if (!safeResponse) {
        safeResponse = [];
    }
    const bar = ramda_1.values(ramda_1.mapObjIndexed((items, key) => {
        return {
            date: key,
            timelineItems: items,
        };
    }, ramda_1.groupBy((item) => {
        // console.log(moment(item.timestamp).utc().format("YYYY-MM-DD"));
        switch (item.type) {
            case "Routine":
                return moment(item.timestamp).utc().format("YYYY-MM-DD");
            default:
                return moment(item.timestamp).format("YYYY-MM-DD");
        }
    }, ramda_1.map((value) => ITimelineItemResponse_ITimelineItem_1.ITimelineItemResponse_ITimelineItem(value, translate), safeResponse))));
    return bar;
};

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mergeDefaultAndResponse_1 = require("./../lib/mergeDefaultAndResponse");
const IMemberDoingWellResponse_1 = require("./../../../src/responses/IMemberDoingWellResponse");
exports.IMemberDoingWellResponse_IMemberDoingWell = (value) => {
    const validValue = mergeDefaultAndResponse_1.mergeDefaultAndResponse(new IMemberDoingWellResponse_1.IMemberDoingWellResponse(), value);
    if (validValue.attributes.data.Goal_goaltext_) {
        validValue.attributes.data.goal = JSON.parse(validValue.attributes.data.Goal_goaltext_);
    }
    return {
        id: validValue.attributes.data["_userid_"],
        firstName: validValue.attributes.data["First Name"],
        lastName: validValue.attributes.data["Last Name"],
        goal: {
            type: validValue.attributes.data["goal"]["type"],
        },
        goalProgress: validValue.attributes.data["Goal Progress (%)"],
        lastWorkout: validValue.attributes.data["Last Workout"],
    };
};

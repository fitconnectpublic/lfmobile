"use strict";
// import map from "ramda/src/map";
// import chain from "ramda/src/chain";
// import filter from "ramda/src/filter";
// import max from "ramda/src/max";
// import reduce from "ramda/src/reduce";
// import mapObjIndexed from "ramda/src/mapObjIndexed";
// import addIndex from "ramda/src/addIndex";
// import is from "ramda/src/is";
Object.defineProperty(exports, "__esModule", { value: true });
const ramda_1 = require("ramda");
exports.replaceNullProps = (definition, 
    /* tslint:disable */
    object, 
    /* tslint:enable */
    propValueMapArg = [], nestLevel = 0) => {
    /* tslint:enable */
    const propValueMap = nestLevel === 0 ? getPropValueMap(definition) : propValueMapArg;
    return ramda_1.mapObjIndexed((val, key, obj) => {
        return val !== undefined && val !== null ? (ramda_1.is(Object, val) && !ramda_1.is(Array, val) ? exports.replaceNullProps(definition, 
        /* tslint:disable */
        object[key], 
        /* tslint:enable */
        propValueMap, nestLevel + 1) : val) : getReplacement(key, val, propValueMap, nestLevel);
        /* tslint:disable */
    }, object);
    /* tslint:enable */
};
const getReplacement = (key, val, propValueMap, nestLevel) => {
    try {
        return ramda_1.filter((propValueItem) => {
            return propValueItem.nestLevel === nestLevel && propValueItem.prop === key;
        }, propValueMap)[0].value;
    }
    catch (e) {
        return val;
    }
};
const getPropValueMap = (definition) => {
    return ramda_1.chain((definitionItem) => {
        return ramda_1.map((path) => {
            const split = path.split(".");
            const nestLevel = split.length - 1;
            return {
                value: definitionItem.value,
                nestLevel,
                path,
                prop: split[split.length - 1],
            };
        }, definitionItem.props);
    }, definition);
};

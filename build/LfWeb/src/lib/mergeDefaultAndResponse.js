"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
// import mergeDeepRight from "ramda/src/mergeDeepRight";
// import ifElse from "ramda/src/ifElse";
// import always from "ramda/src/always";
const ramda_1 = require("ramda");
const isObject_1 = require("./isObject");
const mergeDeepRightAndReplaceNil_1 = require("./mergeDeepRightAndReplaceNil");
exports.mergeDefaultAndResponse = (defaultResponse, response /* tslint:enable */) => {
    return ramda_1.ifElse(isObject_1.default, mergeDeepRightAndReplaceNil_1.default(defaultResponse /* tslint:enable */), ramda_1.always(defaultResponse))(response);
};

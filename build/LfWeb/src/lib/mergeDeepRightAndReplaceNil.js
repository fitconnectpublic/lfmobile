"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
// import isNil from "ramda/src/isNil";
const ramda_1 = require("ramda");
// import mergeDeepWithKey from "ramda/src/mergeDeepWithKey";
exports.mergeDeepRightAndReplaceNil = ramda_1.curry((leftObject, rightObject) => {
    return ramda_1.mergeDeepWithKey((key, leftValue, rightValue) => {
        return ramda_1.isNil(rightValue) ? leftValue : rightValue;
    }, leftObject, rightObject);
});
exports.default = exports.mergeDeepRightAndReplaceNil;

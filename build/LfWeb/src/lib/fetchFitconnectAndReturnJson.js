"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const fetchAndReturnJson_1 = require("./fetchAndReturnJson");
const ramda_1 = require("ramda");
require("rxjs/add/operator/map");
const config_1 = require("./../../../src/config");
exports.fetchFitconnectAndReturnJson = (apiKey) => (input, init) => {
    return fetchAndReturnJson_1.fetchAndReturnJson(`${config_1.config.baseUrl}${input}`, ramda_1.mergeDeepLeft({
        headers: {
            Authorization: apiKey,
        },
    }, init || {}));
};

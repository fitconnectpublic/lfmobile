"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.AuthenticateAction = (token, secret) => {
    return {
        type: "Authenticate",
        token,
        secret,
    };
};

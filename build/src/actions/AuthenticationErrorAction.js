"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.AuthenticationErrorAction = (error) => {
    return {
        type: "AuthenticationError",
        error,
    };
};

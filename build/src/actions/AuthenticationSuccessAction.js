"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.AuthenticationSuccessAction = (token) => {
    return {
        type: "AuthenticationSuccess",
        token,
    };
};

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.selectGoalType = (goal) => {
    return {
        type: "ISelectGoalType",
        goal,
    };
};

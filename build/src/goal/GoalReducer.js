"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const GoalState_1 = require("./GoalState");
const assoc = require("ramda/src/assoc");
function GoalReducer() {
    return (state = new GoalState_1.GoalState(), action) => {
        switch (action.type) {
            case "ISelectGoalType":
                return assoc("goalType", action.goal, state);
            default:
                return state;
        }
    };
}
exports.GoalReducer = GoalReducer;

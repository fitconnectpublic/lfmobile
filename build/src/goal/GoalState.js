"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class GoalState {
    constructor() {
        this.page = "ChooseGoal";
        this.goalType = "None";
    }
}
exports.GoalState = GoalState;

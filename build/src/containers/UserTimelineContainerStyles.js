"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const react_native_1 = require("react-native");
exports.UserTimelineContainerStyles = react_native_1.StyleSheet.create({
    container: {},
    dayTitle: {
        marginTop: 30,
        marginLeft: 10,
        marginRight: 10,
        marginBottom: 10,
    },
});

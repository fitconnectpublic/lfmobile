"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const React = require("react");
const react_redux_1 = require("react-redux");
const react_localize_redux_1 = require("react-localize-redux");
const react_native_1 = require("react-native");
const mapStateToProps = (state) => {
    return {
        translate: react_localize_redux_1.getTranslate(state.locale),
    };
};
const mapDispatchToProps = (dispatch) => {
    return {
        test: () => null,
    };
};
class YourDetails extends React.Component {
    componentDidCatch(error, info) {
        console.error(error, info);
    }
    render() {
        return (React.createElement(react_native_1.View, null,
            React.createElement(react_native_1.Text, null, "Your details container")));
    }
}
exports.YourDetails = YourDetails;
exports.default = react_redux_1.connect(mapStateToProps, mapDispatchToProps)(YourDetails);

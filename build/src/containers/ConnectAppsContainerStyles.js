"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const react_native_1 = require("react-native");
exports.ConnectAppsContainerStyles = react_native_1.StyleSheet.create({
    container: {
        alignSelf: "stretch",
    },
});

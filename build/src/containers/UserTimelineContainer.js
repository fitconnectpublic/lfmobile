"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const React = require("react");
const react_redux_1 = require("react-redux");
const react_localize_redux_1 = require("react-localize-redux");
const react_native_1 = require("react-native");
const ramda_1 = require("ramda");
const moment = require("moment");
// Components
const TimelineRow_1 = require("../components/TimelineRow");
const TimelineLogDetails_1 = require("../components/TimelineLogDetails");
const UserTimelineContainerStyles_1 = require("./UserTimelineContainerStyles");
// Actions
const DashboardActions_1 = require("./../Dashboard/DashboardActions");
const mapStateToProps = (state) => {
    return {
        translate: react_localize_redux_1.getTranslate(state.locale),
        timeline: state.Dashboard.timeline,
        openedEntryIndex: state.Dashboard.openTimelineEntryIndex,
    };
};
const mapDispatchToProps = (dispatch) => {
    return {
        viewLogDetails: (entryIndex) => dispatch(DashboardActions_1.openTimelineEntry(entryIndex)),
        viewMore: () => dispatch(DashboardActions_1.viewMoreTimeline()),
    };
};
class UserTimeline extends React.Component {
    render() {
        let timelineItemIndex = -1;
        return (React.createElement(react_native_1.View, { style: UserTimelineContainerStyles_1.UserTimelineContainerStyles.container }, ramda_1.map((dateGroup) => {
            const formattedDate = moment(dateGroup.date).format("MMM D, ddd").toUpperCase();
            return (React.createElement(react_native_1.View, { key: timelineItemIndex },
                React.createElement(react_native_1.Text, { style: UserTimelineContainerStyles_1.UserTimelineContainerStyles.dayTitle }, formattedDate !== "Invalid date" ? formattedDate : "Unknown Date"),
                ramda_1.map((timelineItem) => {
                    timelineItemIndex += 1;
                    const index = timelineItemIndex;
                    return (React.createElement(react_native_1.View, { key: timelineItemIndex },
                        React.createElement(TimelineRow_1.TimelineRow, { key: timelineItemIndex, onPress: () => {
                                if (index === this.props.openedEntryIndex) {
                                    this.props.viewLogDetails(-1);
                                }
                                else {
                                    this.props.viewLogDetails(index);
                                }
                            }, appIcons: timelineItem.appImages.map((appIcon) => {
                                return { uri: appIcon };
                            }), time: moment(timelineItem.timestamp).format("HH:mm"), type: timelineItem.type, opened: index === this.props.openedEntryIndex ? true : false },
                            React.createElement(react_native_1.Text, null, timelineItem.summary)),
                        React.createElement(TimelineLogDetails_1.default, { payload: timelineItem.payload, open: index === this.props.openedEntryIndex ? true : false })));
                }, dateGroup.timelineItems)));
        }, this.props.timeline)));
    }
}
exports.UserTimeline = UserTimeline;
exports.default = react_redux_1.connect(mapStateToProps, mapDispatchToProps)(UserTimeline);

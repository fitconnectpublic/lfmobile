"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const React = require("react");
const react_redux_1 = require("react-redux");
const react_localize_redux_1 = require("react-localize-redux");
const react_native_1 = require("react-native");
const TextInputWithLabel_1 = require("../components/TextInputWithLabel");
const ColouredBox_1 = require("../components/ColouredBox");
const WeightInput_1 = require("../components/WeightInput");
const DropdownInput_1 = require("../components/DropdownInput");
const TextBoldTitle_1 = require("../components/TextBoldTitle");
const InputGroupTitle_1 = require("../components/InputGroupTitle");
const NavHeader_1 = require("../components/NavHeader");
const ChooseYourGoalContainerStyles_1 = require("./ChooseYourGoalContainerStyles");
const DateInput_1 = require("../components/DateInput");
const GoalActions_1 = require("../goal/GoalActions");
const mapStateToProps = (state) => {
    return {
        translate: react_localize_redux_1.getTranslate(state.locale),
    };
};
const mapDispatchToProps = (dispatch) => {
    return {
        selectGoal: (goal) => dispatch(GoalActions_1.selectGoalType(goal)),
    };
};
class AboutYou extends React.Component {
    constructor() {
        super(...arguments);
        this.getBeHealthierPrimaryFocusList = () => {
            return [
                {
                    text: this.props.translate("LfMobile.ChooseYourGoal.Eating"),
                    value: "Eating",
                },
                {
                    text: this.props.translate("LfMobile.ChooseYourGoal.Sleeping"),
                    value: "Sleeping",
                },
                {
                    text: this.props.translate("LfMobile.ChooseYourGoal.Steps"),
                    value: "Steps",
                },
                {
                    text: this.props.translate("LfMobile.ChooseYourGoal.Workouts"),
                    value: "Workouts",
                },
            ];
        };
        this.getImproveMuscleList = () => {
            return [
                {
                    text: this.props.translate("LfMobile.ChooseYourGoal.NotSure"),
                    value: "NotSure",
                },
                {
                    text: this.props.translate("LfMobile.ChooseYourGoal.WholeBody"),
                    value: "WholeBody",
                },
                {
                    text: this.props.translate("LfMobile.ChooseYourGoal.Chest"),
                    value: "Chest",
                },
                {
                    text: this.props.translate("LfMobile.ChooseYourGoal.Shoulders"),
                    value: "Shoulders",
                },
                {
                    text: this.props.translate("LfMobile.ChooseYourGoal.Legs"),
                    value: "Legs",
                },
                {
                    text: this.props.translate("LfMobile.ChooseYourGoal.Back"),
                    value: "Back",
                },
                {
                    text: this.props.translate("LfMobile.ChooseYourGoal.Arms"),
                    value: "Arms",
                },
                {
                    text: this.props.translate("LfMobile.ChooseYourGoal.Squat"),
                    value: "Squat",
                },
                {
                    text: this.props.translate("LfMobile.ChooseYourGoal.Deadlift"),
                    value: "Deadlift",
                },
                {
                    text: this.props.translate("LfMobile.ChooseYourGoal.MilitaryPress"),
                    value: "MilitaryPress",
                },
                {
                    text: this.props.translate("LfMobile.ChooseYourGoal.BenchPress"),
                    value: "BenchPress",
                },
                {
                    text: this.props.translate("LfMobile.ChooseYourGoal.Cleans"),
                    value: "Cleans",
                },
                {
                    text: this.props.translate("LfMobile.ChooseYourGoal.BarbellRow"),
                    value: "BarbellRow",
                },
                {
                    text: this.props.translate("LfMobile.ChooseYourGoal.PullUps"),
                    value: "PullUps",
                },
                {
                    text: this.props.translate("LfMobile.ChooseYourGoal.Dips"),
                    value: "Dips",
                },
            ];
        };
        this.getMuscleNeedsHelpList = () => {
            return [
                {
                    text: this.props.translate("LfMobile.ChooseYourGoal.GettingAProgram"),
                    value: "GettingAProgram",
                },
                {
                    text: this.props.translate("LfMobile.ChooseYourGoal.StickingToTheProgram"),
                    value: "StickingToTheProgram",
                },
                {
                    text: this.props.translate("LfMobile.ChooseYourGoal.Diet"),
                    value: "Diet",
                },
                {
                    text: this.props.translate("LfMobile.ChooseYourGoal.StayingMotivated"),
                    value: "StayingMotivated",
                },
                {
                    text: this.props.translate("LfMobile.ChooseYourGoal.IDontNeedHelp"),
                    value: "IDontNeedHelp",
                },
            ];
        };
        this.getEventsList = () => {
            return [
                {
                    text: this.props.translate("LfMobile.ChooseYourGoal.CyclingRace"),
                    value: "CyclingRace",
                },
                {
                    text: this.props.translate("LfMobile.ChooseYourGoal.CrossCountryCycling"),
                    value: "CrossCountryCycling",
                },
                {
                    text: this.props.translate("LfMobile.ChooseYourGoal.Ironman"),
                    value: "Ironman",
                },
                {
                    text: this.props.translate("LfMobile.ChooseYourGoal.MudRun"),
                    value: "MudRun",
                },
                {
                    text: this.props.translate("LfMobile.ChooseYourGoal.ObstacleCourse"),
                    value: "ObstacleCourse",
                },
                {
                    text: this.props.translate("LfMobile.ChooseYourGoal.ToughMudder"),
                    value: "ToughMudder",
                },
                {
                    text: this.props.translate("LfMobile.ChooseYourGoal.Other"),
                    value: "Other",
                },
                {
                    text: this.props.translate("LfMobile.ChooseYourGoal.10kmRun"),
                    value: "10kmRun",
                },
                {
                    text: this.props.translate("LfMobile.ChooseYourGoal.CouchTo5k"),
                    value: "CouchTo5k",
                },
                {
                    text: this.props.translate("LfMobile.ChooseYourGoal.RunningRace"),
                    value: "RunningRace",
                },
                {
                    text: this.props.translate("LfMobile.ChooseYourGoal.CrossCountryCycle"),
                    value: "CrossCountryCycle",
                },
                {
                    text: this.props.translate("LfMobile.ChooseYourGoal.HalfMarathon"),
                    value: "HalfMarathon",
                },
                {
                    text: this.props.translate("LfMobile.ChooseYourGoal.Marathon"),
                    value: "Marathon",
                },
                {
                    text: this.props.translate("LfMobile.ChooseYourGoal.UltraMarathon"),
                    value: "UltraMarathon",
                },
                {
                    text: this.props.translate("LfMobile.ChooseYourGoal.TriathlonSprint"),
                    value: "TriathlonSprint",
                },
                {
                    text: this.props.translate("LfMobile.ChooseYourGoal.TriathlonOlympic"),
                    value: "TriathlonOlympic",
                },
                {
                    text: this.props.translate("LfMobile.ChooseYourGoal.WalkingRace"),
                    value: "WalkingRace",
                },
            ];
        };
        this.getNeedsHelpList = () => {
            return [
                {
                    text: this.props.translate("LfMobile.ChooseYourGoal.StickingToDiet"),
                    value: "StickingToDiet",
                },
                {
                    text: this.props.translate("LfMobile.ChooseYourGoal.KnowingWhatToEat"),
                    value: "KnowingWhatToEat",
                },
                {
                    text: this.props.translate("LfMobile.ChooseYourGoal.HowToExercise"),
                    value: "HowToExercise",
                },
                {
                    text: this.props.translate("LfMobile.ChooseYourGoal.StayingMotivated"),
                    value: "StayingMotivated",
                },
                {
                    text: this.props.translate("LfMobile.ChooseYourGoal.NotGainingBack"),
                    value: "NotGainingBack",
                },
            ];
        };
        this.getDietsList = () => {
            return [
                {
                    text: this.props.translate("LfMobile.ChooseYourGoal.INeedHelpWithThis"),
                    value: "NeedsHelp",
                },
                {
                    text: this.props.translate("LfMobile.ChooseYourGoal.Keto"),
                    value: "Keto",
                },
                {
                    text: this.props.translate("LfMobile.ChooseYourGoal.Paleo"),
                    value: "Paleo",
                },
                {
                    text: this.props.translate("LfMobile.ChooseYourGoal.LowSodium"),
                    value: "LowSodium",
                },
                {
                    text: this.props.translate("LfMobile.ChooseYourGoal.IntermittentFasting"),
                    value: "IntermittentFasting",
                },
                {
                    text: this.props.translate("LfMobile.ChooseYourGoal.HighProtein"),
                    value: "HighProtein",
                },
                {
                    text: this.props.translate("LfMobile.ChooseYourGoal.CleanEating"),
                    value: "CleanEating",
                },
                {
                    text: this.props.translate("LfMobile.ChooseYourGoal.Mediterranean"),
                    value: "Mediterranean",
                },
                {
                    text: this.props.translate("LfMobile.ChooseYourGoal.LowCarb"),
                    value: "LowCarb",
                },
                {
                    text: this.props.translate("LfMobile.ChooseYourGoal.GlutenFree"),
                    value: "GlutenFree",
                },
                {
                    text: this.props.translate("LfMobile.ChooseYourGoal.Vegan"),
                    value: "Vegan",
                },
                {
                    text: this.props.translate("LfMobile.ChooseYourGoal.Other"),
                    value: "Other",
                },
            ];
        };
    }
    componentDidCatch(error, info) {
        console.error(error, info);
    }
    render() {
        return (React.createElement(react_native_1.View, null,
            React.createElement(NavHeader_1.NavHeader, { title: this.props.translate("LfMobile.ChooseYourGoal.ChooseYourGoal"), buttonText: this.props.translate("LfMobile.ChooseYourGoal.Next"), onButtonPress: () => null, buttonDisabled: true }),
            React.createElement(ColouredBox_1.ColouredBox, { backgroundColor: "grayscale3" },
                React.createElement(TextBoldTitle_1.TextBoldTitle, null, this.props.translate("LfMobile.ChooseYourGoal.Step2")),
                React.createElement(react_native_1.Text, null, this.props.translate("LfMobile.ChooseYourGoal.SettingClearlyDefined"))),
            React.createElement(InputGroupTitle_1.InputGroupTitle, { title: this.props.translate("LfMobile.ChooseYourGoal.WhatsYouGoal") }),
            React.createElement(react_native_1.TouchableOpacity, { onPress: () => this.props.selectGoal("BeHealthier"), style: ChooseYourGoalContainerStyles_1.ChooseYourGoalContainerStyles.goalRow },
                React.createElement(react_native_1.View, { style: ChooseYourGoalContainerStyles_1.ChooseYourGoalContainerStyles.goalRowIcon },
                    React.createElement(react_native_1.Image, { style: ChooseYourGoalContainerStyles_1.ChooseYourGoalContainerStyles.icon, source: require("./../../images/heartrate.png") })),
                React.createElement(react_native_1.View, { style: ChooseYourGoalContainerStyles_1.ChooseYourGoalContainerStyles.goalRowTextContainer },
                    React.createElement(react_native_1.Text, { style: ChooseYourGoalContainerStyles_1.ChooseYourGoalContainerStyles.goalRowText }, this.props.translate("LfMobile.ChooseYourGoal.BeHealthier"))),
                React.createElement(react_native_1.View, { style: ChooseYourGoalContainerStyles_1.ChooseYourGoalContainerStyles.goalRowCaret },
                    React.createElement(react_native_1.Image, { style: ChooseYourGoalContainerStyles_1.ChooseYourGoalContainerStyles.goalRowCaretImage, source: require("./../../images/chevron-right.png") }))),
            React.createElement(react_native_1.TouchableOpacity, { onPress: () => this.props.selectGoal("GetInShape"), style: ChooseYourGoalContainerStyles_1.ChooseYourGoalContainerStyles.goalRow },
                React.createElement(react_native_1.View, { style: ChooseYourGoalContainerStyles_1.ChooseYourGoalContainerStyles.goalRowIcon },
                    React.createElement(react_native_1.Image, { style: ChooseYourGoalContainerStyles_1.ChooseYourGoalContainerStyles.icon, source: require("./../../images/speed.png") })),
                React.createElement(react_native_1.View, { style: ChooseYourGoalContainerStyles_1.ChooseYourGoalContainerStyles.goalRowTextContainer },
                    React.createElement(react_native_1.Text, { style: ChooseYourGoalContainerStyles_1.ChooseYourGoalContainerStyles.goalRowText }, this.props.translate("LfMobile.ChooseYourGoal.GetInShape"))),
                React.createElement(react_native_1.View, { style: ChooseYourGoalContainerStyles_1.ChooseYourGoalContainerStyles.goalRowCaret },
                    React.createElement(react_native_1.Image, { style: ChooseYourGoalContainerStyles_1.ChooseYourGoalContainerStyles.goalRowCaretImage, source: require("./../../images/chevron-right.png") }))),
            React.createElement(react_native_1.TouchableOpacity, { onPress: () => this.props.selectGoal("TrainForAnEvent"), style: ChooseYourGoalContainerStyles_1.ChooseYourGoalContainerStyles.goalRow },
                React.createElement(react_native_1.View, { style: ChooseYourGoalContainerStyles_1.ChooseYourGoalContainerStyles.goalRowIcon },
                    React.createElement(react_native_1.Image, { style: ChooseYourGoalContainerStyles_1.ChooseYourGoalContainerStyles.icon, source: require("./../../images/calendar.png") })),
                React.createElement(react_native_1.View, { style: ChooseYourGoalContainerStyles_1.ChooseYourGoalContainerStyles.goalRowTextContainer },
                    React.createElement(react_native_1.Text, { style: ChooseYourGoalContainerStyles_1.ChooseYourGoalContainerStyles.goalRowText }, this.props.translate("LfMobile.ChooseYourGoal.TrainForAnEvent"))),
                React.createElement(react_native_1.View, { style: ChooseYourGoalContainerStyles_1.ChooseYourGoalContainerStyles.goalRowCaret },
                    React.createElement(react_native_1.Image, { style: ChooseYourGoalContainerStyles_1.ChooseYourGoalContainerStyles.goalRowCaretImage, source: require("./../../images/chevron-right.png") }))),
            React.createElement(react_native_1.TouchableOpacity, { onPress: () => this.props.selectGoal("BuildMuscle"), style: ChooseYourGoalContainerStyles_1.ChooseYourGoalContainerStyles.goalRow },
                React.createElement(react_native_1.View, { style: ChooseYourGoalContainerStyles_1.ChooseYourGoalContainerStyles.goalRowIcon },
                    React.createElement(react_native_1.Image, { style: ChooseYourGoalContainerStyles_1.ChooseYourGoalContainerStyles.icon, source: require("./../../images/strength.png") })),
                React.createElement(react_native_1.View, { style: ChooseYourGoalContainerStyles_1.ChooseYourGoalContainerStyles.goalRowTextContainer },
                    React.createElement(react_native_1.Text, { style: ChooseYourGoalContainerStyles_1.ChooseYourGoalContainerStyles.goalRowText }, this.props.translate("LfMobile.ChooseYourGoal.BuildMuscle"))),
                React.createElement(react_native_1.View, { style: ChooseYourGoalContainerStyles_1.ChooseYourGoalContainerStyles.goalRowCaret },
                    React.createElement(react_native_1.Image, { style: ChooseYourGoalContainerStyles_1.ChooseYourGoalContainerStyles.goalRowCaretImage, source: require("./../../images/chevron-right.png") }))),
            React.createElement(react_native_1.TouchableOpacity, { onPress: () => this.props.selectGoal("LoseWeight"), style: ChooseYourGoalContainerStyles_1.ChooseYourGoalContainerStyles.goalRow },
                React.createElement(react_native_1.View, { style: ChooseYourGoalContainerStyles_1.ChooseYourGoalContainerStyles.goalRowIcon },
                    React.createElement(react_native_1.Image, { style: ChooseYourGoalContainerStyles_1.ChooseYourGoalContainerStyles.icon, source: require("./../../images/weight.png") })),
                React.createElement(react_native_1.View, { style: ChooseYourGoalContainerStyles_1.ChooseYourGoalContainerStyles.goalRowTextContainer },
                    React.createElement(react_native_1.Text, { style: ChooseYourGoalContainerStyles_1.ChooseYourGoalContainerStyles.goalRowText }, this.props.translate("LfMobile.ChooseYourGoal.LoseWeight"))),
                React.createElement(react_native_1.View, { style: ChooseYourGoalContainerStyles_1.ChooseYourGoalContainerStyles.goalRowCaret },
                    React.createElement(react_native_1.Image, { style: ChooseYourGoalContainerStyles_1.ChooseYourGoalContainerStyles.goalRowCaretImage, source: require("./../../images/chevron-right.png") }))),
            React.createElement(react_native_1.View, null,
                React.createElement(InputGroupTitle_1.InputGroupTitle, { title: this.props.translate("LfMobile.ChooseYourGoal.WieghtLossCaps") }),
                React.createElement(WeightInput_1.default, { title: this.props.translate("LfMobile.ChooseYourGoal.CurrentWeight"), value: "130", unitSelected: "lbs", onChangeValue: () => null }),
                React.createElement(WeightInput_1.default, { title: this.props.translate("LfMobile.ChooseYourGoal.DesiredWeightLoss"), value: "130", unitSelected: "kg", onChangeValue: () => null }),
                React.createElement(react_native_1.Text, null, this.props.translate("LfMobile.ChooseYourGoal.ThatsGreat", {
                    targetWeight: "120",
                })),
                React.createElement(InputGroupTitle_1.InputGroupTitle, { title: this.props.translate("LfMobile.ChooseYourGoal.AreYouCurrently") }),
                React.createElement(DropdownInput_1.DropdownInput, { placeholder: "", options: this.getDietsList(), value: "sepecfic", onSelect: () => null }),
                React.createElement(InputGroupTitle_1.InputGroupTitle, { title: this.props.translate("LfMobile.ChooseYourGoal.WouldYouLikeAdvice") }),
                React.createElement(DropdownInput_1.DropdownInput, { placeholder: this.props.translate("LfMobile.ChooseYourGoal.SelectAdvice"), options: this.getNeedsHelpList(), value: "sepecfic", onSelect: () => null })),
            React.createElement(react_native_1.View, null,
                React.createElement(InputGroupTitle_1.InputGroupTitle, { title: this.props.translate("LfMobile.ChooseYourGoal.HowMuchBodyFat") }),
                React.createElement(TextInputWithLabel_1.TextInputWithLabel, { label: this.props.translate("LfMobile.ChooseYourGoal.BodyFat"), placeholder: this.props.translate("LfMobile.ChooseYourGoal.Eg4"), onChangeText: () => null, keyboardType: "numeric" }),
                React.createElement(InputGroupTitle_1.InputGroupTitle, { title: this.props.translate("LfMobile.ChooseYourGoal.IsThereAParticularDiet") }),
                React.createElement(DropdownInput_1.DropdownInput, { placeholder: "", options: this.getDietsList(), value: "sepecfic", onSelect: () => null }),
                React.createElement(InputGroupTitle_1.InputGroupTitle, { title: this.props.translate("LfMobile.ChooseYourGoal.WouldYouLikeAdvice") }),
                React.createElement(DropdownInput_1.DropdownInput, { placeholder: this.props.translate("LfMobile.ChooseYourGoal.SelectAdvice"), options: this.getNeedsHelpList(), value: "sepecfic", onSelect: () => null })),
            React.createElement(react_native_1.View, null,
                React.createElement(InputGroupTitle_1.InputGroupTitle, { title: this.props.translate("LfMobile.ChooseYourGoal.WhatEvent") }),
                React.createElement(DropdownInput_1.DropdownInput, { placeholder: this.props.translate("LfMobile.ChooseYourGoal.SelectAnEvent"), options: this.getEventsList(), value: "", onSelect: () => null }),
                React.createElement(InputGroupTitle_1.InputGroupTitle, { title: this.props.translate("LfMobile.ChooseYourGoal.WhatIsTheDate") }),
                React.createElement(DateInput_1.default, { placeholder: this.props.translate("LfMobile.ChooseYourGoal.SelectDate"), title: this.props.translate("LfMobile.ChooseYourGoal.Date"), date: "", onDateChange: () => null }),
                React.createElement(InputGroupTitle_1.InputGroupTitle, { title: this.props.translate("LfMobile.ChooseYourGoal.WouldYouLikeAdvice") }),
                React.createElement(DropdownInput_1.DropdownInput, { placeholder: this.props.translate("LfMobile.ChooseYourGoal.SelectAdvice"), options: this.getNeedsHelpList(), value: "sepecfic", onSelect: () => null })),
            React.createElement(react_native_1.View, null,
                React.createElement(InputGroupTitle_1.InputGroupTitle, { title: this.props.translate("LfMobile.ChooseYourGoal.StrengthGains") }),
                React.createElement(DropdownInput_1.DropdownInput, { placeholder: this.props.translate("LfMobile.ChooseYourGoal.Select"), options: this.getImproveMuscleList(), value: "", onSelect: () => null }),
                React.createElement(InputGroupTitle_1.InputGroupTitle, { title: this.props.translate("LfMobile.ChooseYourGoal.WouldYouLikeAdvice") }),
                React.createElement(DropdownInput_1.DropdownInput, { placeholder: this.props.translate("LfMobile.ChooseYourGoal.SelectAdvice"), options: this.getMuscleNeedsHelpList(), value: "", onSelect: () => null })),
            React.createElement(react_native_1.View, null,
                React.createElement(InputGroupTitle_1.InputGroupTitle, { title: this.props.translate("LfMobile.ChooseYourGoal.PrimaryFocus") }),
                React.createElement(DropdownInput_1.DropdownInput, { placeholder: this.props.translate("LfMobile.ChooseYourGoal.Select"), options: this.getBeHealthierPrimaryFocusList(), value: "", onSelect: () => null }),
                React.createElement(react_native_1.View, null,
                    React.createElement(InputGroupTitle_1.InputGroupTitle, { title: this.props.translate("LfMobile.ChooseYourGoal.WhatIsYourCalorieGoal") }),
                    React.createElement(TextInputWithLabel_1.TextInputWithLabel, { label: this.props.translate("LfMobile.ChooseYourGoal.CalorieGoal"), placeholder: this.props.translate("LfMobile.ChooseYourGoal.Eg2000"), onChangeText: () => null, keyboardType: "numeric" }),
                    React.createElement(InputGroupTitle_1.InputGroupTitle, { title: this.props.translate("LfMobile.ChooseYourGoal.AreYouCurrently") }),
                    React.createElement(DropdownInput_1.DropdownInput, { placeholder: "", options: this.getDietsList(), value: "sepecfic", onSelect: () => null }),
                    React.createElement(InputGroupTitle_1.InputGroupTitle, { title: this.props.translate("LfMobile.ChooseYourGoal.WouldYouLikeAdvice") }),
                    React.createElement(DropdownInput_1.DropdownInput, { placeholder: this.props.translate("LfMobile.ChooseYourGoal.SelectAdvice"), options: this.getNeedsHelpList(), value: "sepecfic", onSelect: () => null })),
                React.createElement(react_native_1.View, null,
                    React.createElement(InputGroupTitle_1.InputGroupTitle, { title: this.props.translate("LfMobile.ChooseYourGoal.HowManyHours") }),
                    React.createElement(TextInputWithLabel_1.TextInputWithLabel, { label: this.props.translate("LfMobile.ChooseYourGoal.Hours"), placeholder: this.props.translate("LfMobile.ChooseYourGoal.Eg8"), onChangeText: () => null, keyboardType: "numeric" })),
                React.createElement(react_native_1.View, null,
                    React.createElement(InputGroupTitle_1.InputGroupTitle, { title: this.props.translate("LfMobile.ChooseYourGoal.HowManySteps") }),
                    React.createElement(TextInputWithLabel_1.TextInputWithLabel, { label: this.props.translate("LfMobile.ChooseYourGoal.Steps"), placeholder: this.props.translate("LfMobile.ChooseYourGoal.Eg5000"), onChangeText: () => null, keyboardType: "numeric" })),
                React.createElement(react_native_1.View, null,
                    React.createElement(InputGroupTitle_1.InputGroupTitle, { title: this.props.translate("LfMobile.ChooseYourGoal.HowManyWorkouts") }),
                    React.createElement(TextInputWithLabel_1.TextInputWithLabel, { label: this.props.translate("LfMobile.ChooseYourGoal.Workouts"), placeholder: this.props.translate("LfMobile.ChooseYourGoal.Eg2"), onChangeText: () => null, keyboardType: "numeric" })))));
    }
}
exports.AboutYou = AboutYou;
exports.default = react_redux_1.connect(mapStateToProps, mapDispatchToProps)(AboutYou);

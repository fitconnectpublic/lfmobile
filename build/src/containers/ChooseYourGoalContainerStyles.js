"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const react_native_1 = require("react-native");
const VariablesColors_1 = require("./../VariablesColors");
exports.ChooseYourGoalContainerStyles = react_native_1.StyleSheet.create({
    icon: {
        tintColor: "#0079C3",
        width: 40,
        height: 40,
    },
    goalRow: {
        display: "flex",
        borderBottomColor: VariablesColors_1.colors.grayscale2,
        borderBottomWidth: 1,
        alignSelf: "stretch",
        flexDirection: "row",
        backgroundColor: VariablesColors_1.colors.grayscale0,
    },
    goalRowIcon: {
        backgroundColor: VariablesColors_1.colors.grayscale1,
        width: 80,
        height: 80,
        justifyContent: "center",
        alignItems: "center",
    },
    goalRowTextContainer: {
        flex: 1,
        paddingLeft: 25,
        paddingRight: 25,
        justifyContent: "center",
    },
    goalRowText: {
        fontWeight: "bold",
        color: VariablesColors_1.colors.grayscale5,
        fontSize: 15,
    },
    goalRowCaret: {
        width: 30,
        alignItems: "center",
        justifyContent: "center",
        padding: 20,
    },
    goalRowCaretImage: {
        width: 17,
        height: 17,
        tintColor: VariablesColors_1.colors.grayscale4,
    },
});

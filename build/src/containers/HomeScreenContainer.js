"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const React = require("react");
const react_redux_1 = require("react-redux");
const react_localize_redux_1 = require("react-localize-redux");
const react_native_1 = require("react-native");
const HomeScreenContainerStyles_1 = require("./HomeScreenContainerStyles");
// Components
const CircularProgressGradient_1 = require("../components/CircularProgressGradient");
const UserTimelineContainer_1 = require("../../src/containers/UserTimelineContainer");
const ChartIdPicker_1 = require("./../components/ChartIdPicker");
const ChartGranularityPicker_1 = require("./../components/ChartGranularityPicker");
const Chart_1 = require("./../components/Chart");
const DashboardActions_1 = require("./../Dashboard/DashboardActions");
const mapStateToProps = (state) => {
    return {
        translate: react_localize_redux_1.getTranslate(state.locale),
        selectedChart: state.Dashboard.selectedChartId,
        selectedChartGranularity: state.Dashboard.selectedChartGranularity,
        goalName: state.Dashboard.goalName,
        goalProgress: state.Dashboard.goalProgress,
        chart: state.Dashboard.chart,
    };
};
const mapDispatchToProps = (dispatch) => {
    return {
        selectChart: (chartId) => dispatch(DashboardActions_1.selectChartId(chartId)),
        selectChartGranularity: (granularity) => dispatch(DashboardActions_1.selectChartGranularity(granularity)),
        viewContainer: () => dispatch(DashboardActions_1.viewDashboardScreenAction()),
    };
};
class HomeScreen extends React.Component {
    componentDidMount() {
        this.props.viewContainer();
    }
    render() {
        return (React.createElement(react_native_1.View, { style: HomeScreenContainerStyles_1.default.root },
            React.createElement(react_native_1.Text, { style: HomeScreenContainerStyles_1.default.goalName }, this.props.goalName.toUpperCase()),
            React.createElement(CircularProgressGradient_1.CircularProgressGradient, { percent: this.props.goalProgress, startColor: "#0a7abf", endColor: "#00a9cb" }),
            React.createElement(react_native_1.Text, { style: HomeScreenContainerStyles_1.default.sectionTitle }, this.props.translate("InsightsWidget.ProfileChart.Workouts").toString().toUpperCase()),
            React.createElement(react_native_1.View, { style: HomeScreenContainerStyles_1.default.charts },
                React.createElement(react_native_1.View, { style: HomeScreenContainerStyles_1.default.chartsPickers },
                    React.createElement(ChartIdPicker_1.default, { selectedValue: this.props.selectedChart, onValueChange: this.props.selectChart }),
                    React.createElement(ChartGranularityPicker_1.default, { selectedValue: this.props.selectedChartGranularity, onValueChange: this.props.selectChartGranularity })),
                React.createElement(react_native_1.View, { style: HomeScreenContainerStyles_1.default.chart },
                    React.createElement(Chart_1.default, { values: this.props.chart.values, labels: this.props.chart.labels }))),
            React.createElement(react_native_1.View, { style: HomeScreenContainerStyles_1.default.timeline },
                React.createElement(UserTimelineContainer_1.default, null))));
    }
}
exports.HomeScreen = HomeScreen;
exports.default = react_redux_1.connect(mapStateToProps, mapDispatchToProps)(HomeScreen);

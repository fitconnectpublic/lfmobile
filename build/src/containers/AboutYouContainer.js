"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const React = require("react");
const react_redux_1 = require("react-redux");
const react_localize_redux_1 = require("react-localize-redux");
const react_native_1 = require("react-native");
const TextInputWithLabel_1 = require("../components/TextInputWithLabel");
const ColouredBox_1 = require("../components/ColouredBox");
const TextBoldTitle_1 = require("../components/TextBoldTitle");
const InputGroupTitle_1 = require("../components/InputGroupTitle");
const NavHeader_1 = require("../components/NavHeader");
const mapStateToProps = (state) => {
    return {
        translate: react_localize_redux_1.getTranslate(state.locale),
    };
};
const mapDispatchToProps = (dispatch) => {
    return {
        test: () => null,
    };
};
class AboutYou extends React.Component {
    componentDidCatch(error, info) {
        console.error(error, info);
    }
    render() {
        return (React.createElement(react_native_1.View, null,
            React.createElement(NavHeader_1.NavHeader, { title: this.props.translate("LfMobile.AboutYou.YourDetails"), buttonText: this.props.translate("LfMobile.AboutYou.Next"), onButtonPress: () => null, buttonDisabled: true }),
            React.createElement(ColouredBox_1.ColouredBox, { backgroundColor: "gray" },
                React.createElement(TextBoldTitle_1.TextBoldTitle, null, this.props.translate("LfMobile.AboutYou.Step1")),
                React.createElement(react_native_1.Text, null, this.props.translate("LfMobile.AboutYou.EnterTheFollowing"))),
            React.createElement(InputGroupTitle_1.InputGroupTitle, { title: this.props.translate("LfMobile.AboutYou.AboutYou") }),
            React.createElement(TextInputWithLabel_1.TextInputWithLabel, { label: this.props.translate("LfMobile.AboutYou.FirstName"), placeholder: this.props.translate("LfMobile.AboutYou.Enter"), onChangeText: () => null }),
            React.createElement(TextInputWithLabel_1.TextInputWithLabel, { label: this.props.translate("LfMobile.AboutYou.LastName"), placeholder: this.props.translate("LfMobile.AboutYou.Enter"), onChangeText: () => null })));
    }
}
exports.AboutYou = AboutYou;
exports.default = react_redux_1.connect(mapStateToProps, mapDispatchToProps)(AboutYou);

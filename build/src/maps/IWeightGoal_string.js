"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const round_1 = require("../lib/round");
exports.IWeightGoal_string = (goal, translate /* tslint:enable */) => {
    let displayWeight = goal.weightLoseAmount;
    if (goal.preferred_unit === "kg") {
        displayWeight = "" + round_1.round(goal.weightLoseAmount, 2);
    }
    else if (goal.preferred_unit === "lbs") {
        displayWeight = "" + round_1.round(parseInt(goal.weightLoseAmount, 10) * 2.20462, 2);
    }
    else if (goal.preferred_unit === "inches") {
        displayWeight = "" + round_1.round(parseInt(goal.weightLoseAmount, 10) / 3.85554, 2);
    }
    return translate("InsightsWidget.UserGoal.Weight.Goal", {
        weight: displayWeight,
        unit: goal.preferred_unit,
    });
};

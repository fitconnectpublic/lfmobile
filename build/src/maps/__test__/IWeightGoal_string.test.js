"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const IWeightGoal_string_1 = require("./../IWeightGoal_string");
const react_localize_redux_1 = require("react-localize-redux");
const en_1 = require("../../localisation/en");
const ramda_1 = require("ramda");
describe("IWeightGoal_string", () => {
    test("transforms weight goal", () => {
        const translate = react_localize_redux_1.getTranslate({
            languages: [{
                    code: "en_GB",
                    active: true,
                }],
            translations: ramda_1.map((x) => [x], en_1.default),
            options: {},
        });
        const TGoalJsonKgDataSet = {
            type: "weight",
            preferred_unit: "kg",
            weightLoseAmount: "50.55",
        };
        expect(IWeightGoal_string_1.IWeightGoal_string(TGoalJsonKgDataSet, translate))
            .toEqual(`Lose ${TGoalJsonKgDataSet.weightLoseAmount}${TGoalJsonKgDataSet.preferred_unit}`);
        const TGoalJsonLbsDataSet = {
            type: "weight",
            preferred_unit: "lbs",
            weightLoseAmount: "50.55",
        };
        expect(IWeightGoal_string_1.IWeightGoal_string(TGoalJsonLbsDataSet, translate))
            .toEqual(`Lose 110.23${TGoalJsonLbsDataSet.preferred_unit}`);
        const TGoalJsonInchesDataSet = {
            type: "weight",
            preferred_unit: "inches",
            weightLoseAmount: "50.55",
        };
        expect(IWeightGoal_string_1.IWeightGoal_string(TGoalJsonInchesDataSet, translate))
            .toEqual(`Lose 12.97${TGoalJsonInchesDataSet.preferred_unit}`);
    });
});

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const IHealthierGoal_string_1 = require("./../IHealthierGoal_string");
const react_localize_redux_1 = require("react-localize-redux");
const en_1 = require("../../localisation/en");
const ramda_1 = require("ramda");
describe("IHealthierGoal_string", () => {
    test("transforms healthier goal", () => {
        const translate = react_localize_redux_1.getTranslate({
            languages: [{
                    code: "en_GB",
                    active: true,
                }],
            translations: ramda_1.map((x) => [x], en_1.default),
            options: {},
        });
        const TGoalJsonStepsDataSet = {
            type: "healthier",
            subtype: "steps",
            stepsNumber: 10,
        };
        expect(IHealthierGoal_string_1.IHealthierGoal_string(TGoalJsonStepsDataSet, translate))
            .toEqual(`To walk ${TGoalJsonStepsDataSet.stepsNumber} steps a day`);
        const TGoalJsonWorkoutsDataSet = {
            type: "healthier",
            subtype: "workouts",
            workoutsNumber: 10,
        };
        expect(IHealthierGoal_string_1.IHealthierGoal_string(TGoalJsonWorkoutsDataSet, translate))
            .toEqual(`To workout ${TGoalJsonWorkoutsDataSet.workoutsNumber} times a week`);
        const TGoalJsonSleepDataSet = {
            type: "healthier",
            subtype: "sleep",
            sleepHours: 10,
        };
        expect(IHealthierGoal_string_1.IHealthierGoal_string(TGoalJsonSleepDataSet, translate))
            .toEqual(`To sleep ${TGoalJsonSleepDataSet.sleepHours} hours a night`);
        const TGoalJsonEatingDataSet = {
            type: "healthier",
            subtype: "eating",
            eatingCalories: 10,
        };
        expect(IHealthierGoal_string_1.IHealthierGoal_string(TGoalJsonEatingDataSet, translate))
            .toEqual(`To eat ${TGoalJsonEatingDataSet.eatingCalories} calories a day`);
        const TGoalJsonEmptyDataSet = {
            type: "healthier",
        };
        expect(IHealthierGoal_string_1.IHealthierGoal_string(TGoalJsonEmptyDataSet, translate))
            .toEqual(`To get healthier`);
    });
});

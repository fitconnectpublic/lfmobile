"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const TChartId_TChartTypeRequest_1 = require("./../TChartId_TChartTypeRequest");
describe("TChartId_TChartTypeRequest", () => {
    test("transforms bad string into default", () => {
        expect(TChartId_TChartTypeRequest_1.TChartId_TChartTypeRequest("whothereoverthere"))
            .toEqual("number-of-workouts");
    });
    test("transforms correct string", () => {
        expect(TChartId_TChartTypeRequest_1.TChartId_TChartTypeRequest("NumberOfWorkouts"))
            .toEqual("number-of-workouts");
        expect(TChartId_TChartTypeRequest_1.TChartId_TChartTypeRequest("CaloriesConsumed"))
            .toEqual("calories-consumed");
        expect(TChartId_TChartTypeRequest_1.TChartId_TChartTypeRequest("Steps"))
            .toEqual("steps");
        expect(TChartId_TChartTypeRequest_1.TChartId_TChartTypeRequest("Sleep"))
            .toEqual("sleep");
        expect(TChartId_TChartTypeRequest_1.TChartId_TChartTypeRequest("WorkoutTime"))
            .toEqual("workout-time");
        expect(TChartId_TChartTypeRequest_1.TChartId_TChartTypeRequest("Weight"))
            .toEqual("weight");
        expect(TChartId_TChartTypeRequest_1.TChartId_TChartTypeRequest("BodyFat"))
            .toEqual("body-fat");
    });
});

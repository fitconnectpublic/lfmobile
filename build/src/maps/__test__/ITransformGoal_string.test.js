"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const ITransformGoal_string_1 = require("./../ITransformGoal_string");
const react_localize_redux_1 = require("react-localize-redux");
const en_1 = require("../../localisation/en");
const ramda_1 = require("ramda");
describe("ITransformGoal_string", () => {
    test("transforms transform goal", () => {
        const translate = react_localize_redux_1.getTranslate({
            languages: [{
                    code: "en_GB",
                    active: true,
                }],
            translations: ramda_1.map((x) => [x], en_1.default),
            options: {},
        });
        const TGoalJsonDataSet = {
            type: "transform",
            bodFatDecrease: "10",
        };
        expect(ITransformGoal_string_1.ITransformGoal_string(TGoalJsonDataSet, translate))
            .toEqual(`Get in shape, reduce body fat by 1000%`);
        const TGoalJsonEmptyDataSet = {
            type: "transform",
        };
        expect(ITransformGoal_string_1.ITransformGoal_string(TGoalJsonEmptyDataSet, translate))
            .toEqual(`Get in shape`);
    });
});

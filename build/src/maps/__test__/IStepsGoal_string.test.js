"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const IStepsGoal_string_1 = require("./../IStepsGoal_string");
const react_localize_redux_1 = require("react-localize-redux");
const en_1 = require("../../localisation/en");
const ramda_1 = require("ramda");
describe("IStepsGoal_string", () => {
    test("transforms steps goal", () => {
        const translate = react_localize_redux_1.getTranslate({
            languages: [{
                    code: "en_GB",
                    active: true,
                }],
            translations: ramda_1.map((x) => [x], en_1.default),
            options: {},
        });
        const TGoalJsonStepsDataSet = {
            type: "steps",
            stepsgoal: 10,
        };
        expect(IStepsGoal_string_1.IStepsGoal_string(TGoalJsonStepsDataSet, translate))
            .toEqual(`To walk ${TGoalJsonStepsDataSet.stepsgoal} steps a day`);
    });
});

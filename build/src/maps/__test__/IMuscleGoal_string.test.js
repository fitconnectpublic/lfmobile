"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const IMuscleGoal_string_1 = require("./../IMuscleGoal_string");
const react_localize_redux_1 = require("react-localize-redux");
const en_1 = require("../../localisation/en");
const ramda_1 = require("ramda");
describe("IMuscleGoal_string", () => {
    test("transforms muscle goal", () => {
        const translate = react_localize_redux_1.getTranslate({
            languages: [{
                    code: "en_GB",
                    active: true,
                }],
            translations: ramda_1.map((x) => [x], en_1.default),
            options: {},
        });
        const TGoalJsonDataSet = {
            type: "muscle",
            muscleIncrease: "10",
        };
        expect(IMuscleGoal_string_1.IMuscleGoal_string(TGoalJsonDataSet, translate))
            .toEqual(`Increase muscle mass by 1000%`);
        const TGoalJsonEmptyDataSet = {
            type: "muscle",
        };
        expect(IMuscleGoal_string_1.IMuscleGoal_string(TGoalJsonEmptyDataSet, translate))
            .toEqual(`Increase muscle mass`);
    });
});

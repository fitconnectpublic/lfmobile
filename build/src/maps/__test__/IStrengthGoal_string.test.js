"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const IStrengthGoal_string_1 = require("./../IStrengthGoal_string");
const react_localize_redux_1 = require("react-localize-redux");
const en_1 = require("../../localisation/en");
const ramda_1 = require("ramda");
describe("IStrengthGoal_string", () => {
    test("transforms strength goal", () => {
        const translate = react_localize_redux_1.getTranslate({
            languages: [{
                    code: "en_GB",
                    active: true,
                }],
            translations: ramda_1.map((x) => [x], en_1.default),
            options: {},
        });
        const TGoalJsonDataSet = {
            type: "strength",
        };
        expect(IStrengthGoal_string_1.IStrengthGoal_string(TGoalJsonDataSet, translate))
            .toEqual(`Get stronger.`);
    });
});

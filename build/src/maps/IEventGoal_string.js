"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const moment = require("moment");
const round_1 = require("../lib/round");
exports.IEventGoal_string = (model, translate) => {
    let goalmessage = "";
    if (model.eventName && model.eventDistance && model.eventDate) {
        goalmessage = translate("InsightsWidget.UserGoal.Event.GoalWithEventNameEventDistanceAndEventDate", {
            event_name: model.eventName,
            event_distance: round_1.round(model.eventDistance, 1),
            event_date: moment(model.eventDate).format("ddd Do MMM"),
        });
    }
    else if (model.eventName && model.eventDistance) {
        goalmessage = translate("InsightsWidget.UserGoal.Event.GoalWithEventNameAndEventDistance", {
            event_name: model.eventName,
            event_distance: round_1.round(model.eventDistance, 1),
        });
    }
    else if (model.eventName && model.eventDate) {
        goalmessage = translate("InsightsWidget.UserGoal.Event.GoalWithEventNameAndEventDate", {
            event_name: model.eventName,
            event_date: moment(model.eventDate).format("ddd Do MMM"),
        });
    }
    else if (model.eventDistance && model.eventDate) {
        goalmessage = translate("InsightsWidget.UserGoal.Event.GoalWithEventDistanceAndEventDate", {
            event_distance: round_1.round(model.eventDistance, 1),
            event_date: moment(model.eventDate).format("ddd Do MMM"),
        });
    }
    else if (model.eventName) {
        goalmessage = translate("InsightsWidget.UserGoal.Event.GoalWithEventName", {
            event_name: model.eventName,
        });
    }
    else if (model.eventDate) {
        goalmessage = translate("InsightsWidget.UserGoal.Event.GoalWithEventDate", {
            event_date: moment(model.eventDate).format("ddd Do MMM"),
        });
    }
    else if (model.eventDistance) {
        goalmessage = translate("InsightsWidget.UserGoal.Event.GoalWithEventDistance", {
            event_distance: round_1.round(model.eventDistance, 1),
        });
    }
    else {
        goalmessage = translate("InsightsWidget.UserGoal.Event.GoalWithNoData");
    }
    return goalmessage;
};

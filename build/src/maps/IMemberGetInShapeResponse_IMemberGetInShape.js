"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mergeDefaultAndResponse_1 = require("./../lib/mergeDefaultAndResponse");
const IMemberGetInShapeResponse_1 = require("./../responses/IMemberGetInShapeResponse");
exports.IMemberGetInShapeResponse_IMemberGetInShape = (value) => {
    const validValue = mergeDefaultAndResponse_1.mergeDefaultAndResponse(new IMemberGetInShapeResponse_1.IMemberGetInShapeResponse(), value);
    return {
        id: validValue.attributes.data["_userid_"],
        firstName: validValue.attributes.data["First Name"],
        lastName: validValue.attributes.data["Last Name"],
        bodyFatDecrease: validValue.attributes.data["Body Fat Decrease"],
        needsHelpWith: validValue.attributes.data["Needs help with_filterselect_"],
        progress: validValue.attributes.data["Progress"],
    };
};

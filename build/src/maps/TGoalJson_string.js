"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const ramda_1 = require("ramda");
// Maps
const IEventGoal_string_1 = require("./IEventGoal_string");
const IWeightGoal_string_1 = require("./IWeightGoal_string");
const ITransformGoal_string_1 = require("./ITransformGoal_string");
const IStrengthGoal_string_1 = require("./IStrengthGoal_string");
const IMuscleGoal_string_1 = require("./IMuscleGoal_string");
const IHealthierGoal_string_1 = require("./IHealthierGoal_string");
const IStepsGoal_string_1 = require("./IStepsGoal_string");
exports.TGoalJson_string = (goal, translate) => {
    return ramda_1.cond([
        [ramda_1.propEq("type", "event"), IEventGoal_string_1.IEventGoal_string],
        [ramda_1.propEq("type", "weight"), IWeightGoal_string_1.IWeightGoal_string],
        [ramda_1.propEq("type", "transform"), ITransformGoal_string_1.ITransformGoal_string],
        [ramda_1.propEq("type", "strength"), IStrengthGoal_string_1.IStrengthGoal_string],
        [ramda_1.propEq("type", "muscle"), IMuscleGoal_string_1.IMuscleGoal_string],
        [ramda_1.propEq("type", "healthier"), IHealthierGoal_string_1.IHealthierGoal_string],
        [ramda_1.propEq("type", "steps"), IStepsGoal_string_1.IStepsGoal_string],
        [ramda_1.propEq("type", ""), ramda_1.always("")],
        [ramda_1.propEq("type", undefined), ramda_1.always("")],
        [ramda_1.propEq("type", null), ramda_1.always("")],
    ])(goal, translate);
};

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.IStrengthGoal_string = (goal, translate /* tslint:enable */) => {
    return translate("InsightsWidget.UserGoal.Strength.Goal");
};

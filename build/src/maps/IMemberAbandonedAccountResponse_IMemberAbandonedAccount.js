"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mergeDefaultAndResponse_1 = require("./../lib/mergeDefaultAndResponse");
const IMemberAbandonedAccountResponse_1 = require("../responses/IMemberAbandonedAccountResponse");
exports.IMemberAbandonedAccountResponse_IMemberAbandonedAccount = (value) => {
    const validValue = mergeDefaultAndResponse_1.mergeDefaultAndResponse(new IMemberAbandonedAccountResponse_1.IMemberAbandonedAccountResponse(), value);
    return {
        id: validValue.attributes.data["_userid_"],
        firstName: validValue.attributes.data["First Name"],
        lastName: validValue.attributes.data["Last Name"],
        dateAbandoned: validValue.attributes.data["Date Abandoned"],
        droppedOffAt: validValue.attributes.data["Dropped Off At"],
        email: validValue.attributes.data["Email"],
        siteName: validValue.attributes.data["Site Name"],
    };
};

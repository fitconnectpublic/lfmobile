"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const ramda_1 = require("ramda");
const ITimelineItemResponse_ITimelineItem_1 = require("./ITimelineItemResponse_ITimelineItem");
const moment = require("moment");
exports.TTimelineResponse_TTimeline = (response, translate /* tslint:enable */) => {
    const groupTimelineItemsByTimestamp = (item) => {
        switch (item.type) {
            case "Routine":
                return moment(item.timestamp).utc().format("YYYY-MM-DD");
            default:
                return moment(item.timestamp).format("YYYY-MM-DD");
        }
    };
    const ITimelineItemRecord_ITimelineItemDateGroup = (items, key) => {
        return {
            date: key,
            timelineItems: items,
        };
    };
    return ramda_1.pipe(ramda_1.ifElse(ramda_1.isNil, ramda_1.always([]), ramda_1.identity), ramda_1.map((value) => ITimelineItemResponse_ITimelineItem_1.ITimelineItemResponse_ITimelineItem(value, translate)), ramda_1.groupBy(groupTimelineItemsByTimestamp), ramda_1.mapObjIndexed(ITimelineItemRecord_ITimelineItemDateGroup), ramda_1.values)(response.data);
};

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mergeDefaultAndResponse_1 = require("./../lib/mergeDefaultAndResponse");
const IUserGoalsResponse_1 = require("./../responses/IUserGoalsResponse");
const ramda_1 = require("ramda");
exports.IUserGoalResponseArray_IMemberSummaryGoal = (value) => {
    const validValue = mergeDefaultAndResponse_1.mergeDefaultAndResponse(new IUserGoalsResponse_1.IUserGoalsResponse(), ramda_1.ifElse(ramda_1.either(ramda_1.isNil, ramda_1.isEmpty), ramda_1.always({}), ramda_1.head)(value));
    return {
        name: validValue.attributes.type,
        progress: parseInt(validValue.attributes.percentage, 10),
        summary: "",
    };
};

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mergeDefaultAndResponse_1 = require("./../lib/mergeDefaultAndResponse");
const IMemberRewardPointsResponse_1 = require("./../responses/IMemberRewardPointsResponse");
exports.IMemberRewardPointsResponse_IMemberRewardPoints = (value) => {
    const validValue = mergeDefaultAndResponse_1.mergeDefaultAndResponse(new IMemberRewardPointsResponse_1.IMemberRewardPointsResponse(), value);
    return {
        id: validValue.attributes.data["_userid_"],
        firstName: validValue.attributes.data["First Name"],
        lastName: validValue.attributes.data["Last Name"],
        points: validValue.attributes.data["Points_filterrange_"],
    };
};

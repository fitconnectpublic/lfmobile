"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mergeDefaultAndResponse_1 = require("./../lib/mergeDefaultAndResponse");
const IUserGoalsResponse_1 = require("./../responses/IUserGoalsResponse");
exports.IUserGoalResponse_IMemberSummary = (value) => {
    const validValue = mergeDefaultAndResponse_1.mergeDefaultAndResponse(new IUserGoalsResponse_1.IUserGoalsResponse(), value);
    return {
        name: validValue.attributes.type,
        progress: parseInt(validValue.attributes.percentage, 10),
        summary: ""
    };
};

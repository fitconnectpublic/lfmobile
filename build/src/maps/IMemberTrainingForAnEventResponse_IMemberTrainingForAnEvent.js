"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mergeDefaultAndResponse_1 = require("./../lib/mergeDefaultAndResponse");
const IMemberTrainingForAnEventResponse_1 = require("./../responses/IMemberTrainingForAnEventResponse");
exports.IMemberTrainingForAnEventResponse_IMemberTrainingForAnEvent = (value) => {
    const validValue = mergeDefaultAndResponse_1.mergeDefaultAndResponse(new IMemberTrainingForAnEventResponse_1.IMemberTrainingForAnEventResponse(), value);
    return {
        id: validValue.attributes.data["_userid_"],
        firstName: validValue.attributes.data["First Name"],
        lastName: validValue.attributes.data["Last Name"],
        eventType: validValue.attributes.data["_filterselect_Type"],
        eventName: validValue.attributes.data["Event Name"],
        date: validValue.attributes.data["Date"],
        distance: validValue.attributes.data["Distance"],
        goalProgress: validValue.attributes.data["Goal Progress"],
        needsHelpWith: validValue.attributes.data["Needs help with"],
    };
};

"use strict";
// export const blue = "#0079C2";
// export const darkBlue = "#415464";
// export const darkGray = "#4a4a4a";
// export const gray = "#9a9a9a";
// export const green = "#739849";
// export const lightBlue = "#4196b4";
// export const lightGray = "#e9e9e9";
// export const orange = "#FF6D41";
// export const red = "#AB1000";
// export const yellow = "#F5BD47";
Object.defineProperty(exports, "__esModule", { value: true });
// export const grayscale0 = "#fff";
// export const grayscale1 = "#f5f5f5";
// export const grayscale2 = "#E9E9E9";
// export const grayscale3 = "#DBDBDB";
// export const grayscale4 = "#A7A7A7";
// export const grayscale5 = "#4A4A4A";
// export const timelineMessageGrey = "#9DAEAB"; // Unlikely to be used except for timeline
exports.colors = {
    blue: "#0079C2",
    darkBlue: "#415464",
    darkGray: "#4a4a4a",
    gray: "#9a9a9a",
    green: "#739849",
    lightBlue: "#4196b4",
    lightGray: "#e9e9e9",
    orange: "#FF6D41",
    red: "#AB1000",
    yellow: "#F5BD47",
    grayscale0: "#fff",
    grayscale1: "#f5f5f5",
    grayscale2: "#E9E9E9",
    grayscale3: "#DBDBDB",
    grayscale4: "#A7A7A7",
    grayscale5: "#4A4A4A",
    timelineMessageGrey: "#9DAEAB",
};

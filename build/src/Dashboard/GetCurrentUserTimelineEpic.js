"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Observable_1 = require("rxjs/Observable");
require("rxjs/add/operator/switchMap");
require("rxjs/add/operator/filter");
require("rxjs/add/operator/map");
require("rxjs/add/operator/catch");
require("rxjs/add/observable/empty");
const react_localize_redux_1 = require("react-localize-redux");
const ramda_1 = require("ramda");
// Misc
const fetchFitconnectAndReturnJson_1 = require("./../lib/fetchFitconnectAndReturnJson");
const DashboardActions_1 = require("./DashboardActions");
const TTimelineResponse_TTimeline_1 = require("./../maps/TTimelineResponse_TTimeline");
exports.GetCurrentUserTimelineEpic = (action$, stateMiddleware) => {
    return action$
        .filter((action) => {
        return (action.type === "ViewDashboardScreen"
            || action.type === "AuthenticationSuccess");
    })
        .filter((action) => !ramda_1.isNil(stateMiddleware.getState().user.accessToken))
        .switchMap((action) => {
        const state = stateMiddleware.getState();
        return fetchFitconnectAndReturnJson_1.fetchFitconnectAndReturnJson("Bearer " + state.user.accessToken)(`v3/users/me/timeline?$top=25&$skip=${state.Dashboard.timelineOffset * 25}`)
            .map((response) => {
            return TTimelineResponse_TTimeline_1.TTimelineResponse_TTimeline(response.body, react_localize_redux_1.getTranslate(state.locale));
        })
            .catch(() => Observable_1.Observable.empty());
    })
        .map(DashboardActions_1.getTimelineSuccess);
};

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const ramda_1 = require("ramda");
const DashboardState_1 = require("./DashboardState");
function DashboardReducer() {
    return (state = new DashboardState_1.DashboardState(), action) => {
        switch (action.type) {
            case "SelectChartId":
                return ramda_1.assoc("selectedChartId", action.chartId, state);
            case "SelectChartGranularity":
                return ramda_1.assoc("selectedChartGranularity", action.chartGranularity, state);
            case "GetMemberSummarySuccess":
                return ramda_1.pipe(ramda_1.assoc("goalName", action.goal.name), ramda_1.assoc("goalProgress", action.goal.progress))(state);
            case "GetChartSuccess":
                return ramda_1.assoc("chart", action.chart, state);
            case "GetTimelineSuccess":
                return ramda_1.assoc("timeline", action.timeline, state);
            case "OpenTimelineEntry":
                return ramda_1.assoc("timelineEntryIndex", action.entryIndex, state);
            case "ViewMoreTimeline":
                return ramda_1.assoc("timelineOffset", state.timelineOffset + 1, state);
            default:
                return state;
        }
    };
}
exports.DashboardReducer = DashboardReducer;

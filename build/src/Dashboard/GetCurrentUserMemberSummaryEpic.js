"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Observable_1 = require("rxjs/Observable");
require("rxjs/add/operator/switchMap");
require("rxjs/add/operator/filter");
require("rxjs/add/operator/map");
require("rxjs/add/operator/catch");
require("rxjs/add/observable/empty");
const ramda_1 = require("ramda");
// Misc
const fetchFitconnectAndReturnJson_1 = require("./../lib/fetchFitconnectAndReturnJson");
const IUserGoalResponse_IMemberSummaryGoal_1 = require("./../maps/IUserGoalResponse_IMemberSummaryGoal");
// Actions
const DashboardActions_1 = require("../Dashboard/DashboardActions");
exports.GetCurrentUserMemberSummaryEpic = (action$, stateMiddleware) => {
    return action$
        .filter((action) => {
        return (action.type === "ViewDashboardScreen"
            || action.type === "AuthenticationSuccess");
    })
        .filter((action) => !ramda_1.isNil(stateMiddleware.getState().user.accessToken))
        .switchMap((action) => {
        const state = stateMiddleware.getState();
        return fetchFitconnectAndReturnJson_1.fetchFitconnectAndReturnJson("Bearer " + state.user.accessToken)(`/v3/users/me/goals`)
            .map((response) => {
            return IUserGoalResponse_IMemberSummaryGoal_1.IUserGoalResponseArray_IMemberSummaryGoal(response.body.data);
        })
            .catch(() => Observable_1.Observable.empty());
    })
        .map(DashboardActions_1.getMemberSummarySuccess);
};

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.viewDashboardScreenAction = () => {
    return {
        type: "ViewDashboardScreen",
    };
};
exports.selectChartId = (chartId) => {
    return {
        type: "SelectChartId",
        chartId,
    };
};
exports.selectChartGranularity = (chartGranularity) => {
    return {
        type: "SelectChartGranularity",
        chartGranularity,
    };
};
exports.getMemberSummarySuccess = (goal) => {
    return {
        type: "GetMemberSummarySuccess",
        goal,
    };
};
exports.getChartSuccess = (chart) => {
    return {
        type: "GetChartSuccess",
        chart,
    };
};
exports.getTimelineSuccess = (timeline) => {
    return {
        type: "GetTimelineSuccess",
        timeline,
    };
};
exports.openTimelineEntry = (entryIndex) => {
    return {
        type: "OpenTimelineEntry",
        entryIndex,
    };
};
exports.viewMoreTimeline = () => {
    return {
        type: "ViewMoreTimeline",
    };
};

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Observable_1 = require("rxjs/Observable");
require("rxjs/add/operator/switchMap");
require("rxjs/add/operator/filter");
require("rxjs/add/operator/map");
require("rxjs/add/operator/catch");
require("rxjs/add/observable/empty");
const ramda_1 = require("ramda");
// Misc
const fetchFitconnectAndReturnJson_1 = require("..//lib/fetchFitconnectAndReturnJson");
const mergeDefaultAndResponse_1 = require("..//lib/mergeDefaultAndResponse");
const IBarChartResponse_1 = require("../responses/IBarChartResponse");
const IBarChartResponse_IBarChart_1 = require("./../maps/IBarChartResponse_IBarChart");
const TChartId_TChartTypeRequest_1 = require("../maps/TChartId_TChartTypeRequest");
// Actions
const DashboardActions_1 = require("../Dashboard/DashboardActions");
exports.GetCurrentUserChartEpic = (action$, stateMiddleware) => {
    return action$
        .filter((action) => {
        return (action.type === "ViewDashboardScreen"
            || action.type === "SelectChartGranularity"
            || action.type === "SelectChartId"
            || action.type === "AuthenticationSuccess");
    })
        .filter((action) => !ramda_1.isNil(stateMiddleware.getState().user.accessToken))
        .switchMap((action) => {
        const state = stateMiddleware.getState();
        return fetchFitconnectAndReturnJson_1.fetchFitconnectAndReturnJson("Bearer " + state.user.accessToken)(`/v3/users/me/chart-data/chart/${TChartId_TChartTypeRequest_1.TChartId_TChartTypeRequest(state.Dashboard.selectedChartId)}/period/${state.Dashboard.selectedChartGranularity}`)
            .map((response) => {
            return IBarChartResponse_IBarChart_1.IBarChartResponse_IBarChart(mergeDefaultAndResponse_1.mergeDefaultAndResponse(new IBarChartResponse_1.IBarChartResponse(), response.body), state.Dashboard.selectedChartGranularity);
        })
            .catch(() => Observable_1.Observable.empty());
    })
        .map(DashboardActions_1.getChartSuccess);
};

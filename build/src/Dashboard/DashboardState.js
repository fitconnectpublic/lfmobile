"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class DashboardState {
    constructor() {
        this.goalName = "";
        this.goalProgress = 0;
        this.selectedChartId = "NumberOfWorkouts";
        this.selectedChartGranularity = "months";
        this.chart = {
            values: [],
            labels: [],
        };
        this.timeline = [];
        this.timelineOffset = 0;
        this.openTimelineEntryIndex = -1;
    }
}
exports.DashboardState = DashboardState;

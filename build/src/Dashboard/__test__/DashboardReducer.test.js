"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const DashboardReducer_1 = require("./../DashboardReducer");
const DashboardActions_1 = require("../DashboardActions");
const DashboardState_1 = require("../DashboardState");
const ramda_1 = require("ramda");
describe("Dashboard Reducer", () => {
    const reducer = DashboardReducer_1.DashboardReducer();
    test("sets selected chart id", () => {
        const selectedChartId = "NumberOfWorkouts";
        const expected = ramda_1.merge(new DashboardState_1.DashboardState(), {
            selectedChartId,
        });
        expect(reducer(new DashboardState_1.DashboardState(), DashboardActions_1.selectChartId(selectedChartId)))
            .toEqual(expected);
    });
    test("sets selected chart granularity", () => {
        const selectedChartGranularity = "weeks";
        const expected = ramda_1.merge(new DashboardState_1.DashboardState(), {
            selectedChartGranularity,
        });
        expect(reducer(new DashboardState_1.DashboardState(), DashboardActions_1.selectChartGranularity(selectedChartGranularity)))
            .toEqual(expected);
    });
    test("sets goal name and progress on get user goals success", () => {
        const goalSummary = {
            name: "Lose Weight",
            progress: 50,
            summary: "",
        };
        const expected = ramda_1.merge(new DashboardState_1.DashboardState(), {
            goalName: goalSummary.name,
            goalProgress: goalSummary.progress,
        });
        expect(reducer(new DashboardState_1.DashboardState(), DashboardActions_1.getMemberSummarySuccess(goalSummary)))
            .toEqual(expected);
    });
    test("sets chart on get chart success", () => {
        const chart = {
            values: [2, 3, 3],
            labels: ["a", "b", "c"],
        };
        const expected = ramda_1.merge(new DashboardState_1.DashboardState(), {
            chart,
        });
        expect(reducer(new DashboardState_1.DashboardState(), DashboardActions_1.getChartSuccess(chart)))
            .toEqual(expected);
    });
    test("sets timeline on get timeline success", () => {
        const timeline = [
            {
                timelineItems: [
                    {
                        appImages: [],
                        type: "Fitness",
                        summary: "who",
                        timestamp: "2018-05-05",
                        responsibleId: "whonaw",
                        payload: "",
                    },
                ],
                date: "2018-05-05",
            },
        ];
        const expected = ramda_1.merge(new DashboardState_1.DashboardState(), {
            timeline,
        });
        expect(reducer(new DashboardState_1.DashboardState(), DashboardActions_1.getTimelineSuccess(timeline)))
            .toEqual(expected);
    });
    test("sets timeline entry as open when its opened", () => {
        const timelineEntryIndex = 10;
        const expected = ramda_1.merge(new DashboardState_1.DashboardState(), {
            timelineEntryIndex,
        });
        expect(reducer(new DashboardState_1.DashboardState(), DashboardActions_1.openTimelineEntry(timelineEntryIndex)))
            .toEqual(expected);
    });
    test("increments timeline offset on view more timeline", () => {
        const expected = ramda_1.merge(new DashboardState_1.DashboardState(), {
            timelineOffset: 1,
        });
        expect(reducer(new DashboardState_1.DashboardState(), DashboardActions_1.viewMoreTimeline()))
            .toEqual(expected);
    });
});

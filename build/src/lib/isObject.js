"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const ramda_1 = require("ramda");
exports.isObject = (value /* tslint:enable */) => {
    return ramda_1.type(value) === "Object";
};
exports.default = exports.isObject;

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.round = (value, decimals = 0) => {
    return Number(Math.round(Number(value + "e" + decimals)) + "e-" + decimals);
};

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.parseJsonWithDefault = (value /* tslint:enable */, defaultValue) => {
    try {
        const parsedValue = JSON.parse(value);
        return parsedValue;
    }
    catch (_a) {
        return defaultValue;
    }
};
exports.default = exports.parseJsonWithDefault;

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const ramda_1 = require("ramda");
exports.isArray = (value /* tslint:enable */) => {
    return ramda_1.type(value) === "Array";
};
exports.default = exports.isArray;

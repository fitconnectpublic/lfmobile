"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
// import assoc from "ramda/src/assoc";
// import remove from "ramda/src/remove";
// import join from "ramda/src/join";
const ramda_1 = require("ramda");
/* tslint:disable */
exports.addEmptyObjectsIfNull = (props, object) => {
    object = object ? object : {};
    /* tslint:enable */
    props.map((item) => {
        const split = item.split(".");
        if (typeof object[split[0]] !== "object" || object[split[0]] === null) {
            object = ramda_1.assoc(split[0], {}, object);
        }
        if (split.length > 1) {
            object = ramda_1.assoc(split[0], exports.addEmptyObjectsIfNull([ramda_1.join(".", ramda_1.remove(0, 1, split))], object[split[0]]), object);
        }
    });
    return object;
};

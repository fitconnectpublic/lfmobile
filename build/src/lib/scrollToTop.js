"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.scrollToTop = () => {
    document.body.scrollTop = document.documentElement.scrollTop = 0;
    if ("parentIFrame" in window) {
        window /* tslint:enable */.parentIFrame.scrollTo(0, 0);
    }
};

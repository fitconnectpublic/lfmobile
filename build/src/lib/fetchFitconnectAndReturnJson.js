"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const fetchAndReturnJson_1 = require("./fetchAndReturnJson");
const mergeDeepLeft = require("ramda/src/mergeDeepLeft");
require("rxjs/add/operator/map");
const config_1 = require("./../../config");
exports.fetchFitconnectAndReturnJson = (apiKey) => (input, init) => {
    return fetchAndReturnJson_1.fetchAndReturnJson(`${config_1.config.baseUrl}${input}`, mergeDeepLeft({
        headers: {
            Authorization: apiKey,
        },
    }, init || {}));
};

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const redux_1 = require("redux");
const react_localize_redux_1 = require("react-localize-redux");
const UserReducer_1 = require("./../src/user/UserReducer");
const DashboardReducer_1 = require("./Dashboard/DashboardReducer");
function MakeRootReducer() {
    return redux_1.combineReducers({
        locale: react_localize_redux_1.localeReducer,
        user: UserReducer_1.UserReducer(),
        Dashboard: DashboardReducer_1.DashboardReducer(),
    });
}
exports.MakeRootReducer = MakeRootReducer;

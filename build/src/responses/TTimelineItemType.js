"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.TTimelineItemTypeToTTimelineItemTypeText = {
    FITNESS: "Fitness",
    FITNESS_STRENGTH: "Fitness Strength",
    GOAL: "Goal",
    ROUTINE: "Routine",
    WEIGHT: "Weight",
    SLEEP: "Sleep",
    NUTRITION: "Nutrition",
    HEARTRATE: "Heartrate",
    MESSAGE: "Message",
    GROUP: "Group",
};

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const IMemberBaseResponse_1 = require("./IMemberBaseResponse");
class IMemberLoseWeightResponseAttributes extends IMemberBaseResponse_1.IMemberBaseResponse {
    constructor() {
        super(...arguments);
        this["Weight Lose Amount"] = "";
        this["Goal Progress"] = "0%";
        this["_filterselect_Needs help with"] = "";
    }
}
class IMemberLoseWeightResponse {
    constructor() {
        this.attributes = {
            data: new IMemberLoseWeightResponseAttributes()
        };
    }
}
exports.IMemberLoseWeightResponse = IMemberLoseWeightResponse;

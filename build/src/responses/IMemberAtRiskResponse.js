"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const IMemberBaseResponse_1 = require("./IMemberBaseResponse");
class IMemberAtRiskResponseAttributes extends IMemberBaseResponse_1.IMemberBaseResponse {
    constructor() {
        super(...arguments);
        this["Last 30 days_chartx_"] = "";
        this["30 - 60 Days"] = "";
        this["Reduction In Workouts_charty_"] = "";
        this["Best Month"] = "";
        this["Last Activity"] = "";
    }
}
class IMemberAtRiskResponse {
    constructor() {
        this.attributes = {
            data: new IMemberAtRiskResponseAttributes()
        };
    }
}
exports.IMemberAtRiskResponse = IMemberAtRiskResponse;

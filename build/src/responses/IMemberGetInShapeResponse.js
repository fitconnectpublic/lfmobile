"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const IMemberBaseResponse_1 = require("./IMemberBaseResponse");
class IMemberGetInShapeResponseAttributes extends IMemberBaseResponse_1.IMemberBaseResponse {
    constructor() {
        super(...arguments);
        this["Body Fat Decrease"] = "";
        this["Needs help with_filterselect_"] = "";
        this["Progress"] = "";
    }
}
class IMemberGetInShapeResponse {
    constructor() {
        this.attributes = {
            data: new IMemberGetInShapeResponseAttributes()
        };
    }
}
exports.IMemberGetInShapeResponse = IMemberGetInShapeResponse;

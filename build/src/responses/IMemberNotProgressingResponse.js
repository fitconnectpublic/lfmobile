"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const IMemberBaseResponse_1 = require("./IMemberBaseResponse");
class IMemberNotProgressingResponseAttributes extends IMemberBaseResponse_1.IMemberBaseResponse {
    constructor() {
        super(...arguments);
        this["Goal Progress"] = "0%";
        this["Days Since Progress"] = "";
        this["Last Related Activity"] = "";
        this.goal = {
            type: "",
            intensity: "",
        };
    }
}
class IMemberNotProgressingResponse {
    constructor() {
        this.attributes = {
            data: new IMemberNotProgressingResponseAttributes()
        };
    }
}
exports.IMemberNotProgressingResponse = IMemberNotProgressingResponse;

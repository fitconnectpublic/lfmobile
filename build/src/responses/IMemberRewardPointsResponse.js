"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const IMemberBaseResponse_1 = require("./IMemberBaseResponse");
class IMemberRewardPointsResponseAttributes extends IMemberBaseResponse_1.IMemberBaseResponse {
    constructor() {
        super(...arguments);
        this["Points_filterrange_"] = "";
    }
}
class IMemberRewardPointsResponse {
    constructor() {
        this.attributes = {
            data: new IMemberRewardPointsResponseAttributes()
        };
    }
}
exports.IMemberRewardPointsResponse = IMemberRewardPointsResponse;

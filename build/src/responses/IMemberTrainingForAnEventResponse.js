"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const IMemberBaseResponse_1 = require("./IMemberBaseResponse");
class IMemberTrainingForAnEventResponseAtttributes extends IMemberBaseResponse_1.IMemberBaseResponse {
    constructor() {
        super(...arguments);
        this["_filterselect_Type"] = "";
        this["Event Name"] = "";
        this["Date"] = "";
        this["Distance"] = "";
        this["Goal Progress"] = "0%";
        this["Needs help with"] = "";
    }
}
class IMemberTrainingForAnEventResponse {
    constructor() {
        this.attributes = {
            data: new IMemberTrainingForAnEventResponseAtttributes()
        };
    }
}
exports.IMemberTrainingForAnEventResponse = IMemberTrainingForAnEventResponse;

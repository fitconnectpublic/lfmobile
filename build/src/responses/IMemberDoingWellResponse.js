"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const IMemberBaseResponse_1 = require("./IMemberBaseResponse");
class IMemberDoingWellResponseAttributes extends IMemberBaseResponse_1.IMemberBaseResponse {
    constructor() {
        super(...arguments);
        this["Goal_goaltext_"] = "";
        this["Goal Progress (%)"] = "0%";
        this["Last Workout"] = "";
        this.goal = {
            type: "",
        };
    }
}
class IMemberDoingWellResponse {
    constructor() {
        this.attributes = {
            data: new IMemberDoingWellResponseAttributes()
        };
    }
}
exports.IMemberDoingWellResponse = IMemberDoingWellResponse;

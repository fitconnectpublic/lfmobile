"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class IBarChartResponseData {
    constructor() {
        this.attributes = new IBarChartResponseDataAttributes();
    }
}
class IBarChartResponseDataAttributes {
    constructor() {
        this.yAxis = [];
        this.xAxis = [];
    }
}
class IBarChartResponse {
    constructor() {
        this.data = new IBarChartResponseData();
    }
}
exports.IBarChartResponse = IBarChartResponse;

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const IMemberBaseResponse_1 = require("./IMemberBaseResponse");
class IMemberAbandonedAccountResponseAttributes extends IMemberBaseResponse_1.IMemberBaseResponse {
    constructor() {
        super(...arguments);
        this["Date Abandoned"] = "";
        this["Dropped Off At"] = "";
        this["Email"] = "";
        this["Site Name"] = "";
    }
}
class IMemberAbandonedAccountResponse {
    constructor() {
        this.attributes = {
            data: new IMemberAbandonedAccountResponseAttributes()
        };
    }
}
exports.IMemberAbandonedAccountResponse = IMemberAbandonedAccountResponse;

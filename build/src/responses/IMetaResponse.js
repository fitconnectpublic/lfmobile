"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class IMetaPaginationResponse {
    constructor() {
        this.count = 0;
        this.currentPage = 0;
        this.perPage = 0;
        this.total = 0;
        this.totalPages = 0;
        this.notHere = 2;
    }
}
class IMetaResponse {
    constructor() {
        this.pagination = new IMetaPaginationResponse();
    }
}
exports.IMetaResponse = IMetaResponse;

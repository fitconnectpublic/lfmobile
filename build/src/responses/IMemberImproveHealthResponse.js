"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const IMemberBaseResponse_1 = require("./IMemberBaseResponse");
class IMemberImproveHealthResponseAttributes extends IMemberBaseResponse_1.IMemberBaseResponse {
    constructor() {
        super(...arguments);
        this["_filterselect_Wants To Improve"] = "";
        this["Target"] = "";
        this["Goal Progress"] = "0%";
    }
}
class IMemberImproveHealthResponse {
    constructor() {
        this.attributes = {
            data: new IMemberImproveHealthResponseAttributes()
        };
    }
}
exports.IMemberImproveHealthResponse = IMemberImproveHealthResponse;

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const IMemberBaseResponse_1 = require("./IMemberBaseResponse");
class IMemberNewSignupResponseAttributes extends IMemberBaseResponse_1.IMemberBaseResponse {
    constructor() {
        super(...arguments);
        this["Sign Up Day"] = "";
        this["Site"] = "";
    }
}
class IMemberNewSignupResponse {
    constructor() {
        this.attributes = {
            data: new IMemberNewSignupResponseAttributes()
        };
    }
}
exports.IMemberNewSignupResponse = IMemberNewSignupResponse;

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const IMetaResponse_1 = require("./IMetaResponse");
class IReportResponse {
    constructor() {
        this.data = [];
        this.meta = new IMetaResponse_1.IMetaResponse();
        this.reportId = "All";
    }
}
exports.IReportResponse = IReportResponse;

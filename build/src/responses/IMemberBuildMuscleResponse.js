"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const IMemberBaseResponse_1 = require("./IMemberBaseResponse");
class IMemberBuildMuscleResponseAttributes extends IMemberBaseResponse_1.IMemberBaseResponse {
    constructor() {
        super(...arguments);
        this["Muscle Mass Increase"] = "";
        this["Needs help with"] = "";
        this["Progress"] = "";
    }
}
class IMemberBuildMuscleResponse {
    constructor() {
        this.attributes = {
            data: new IMemberBuildMuscleResponseAttributes()
        };
    }
}
exports.IMemberBuildMuscleResponse = IMemberBuildMuscleResponse;

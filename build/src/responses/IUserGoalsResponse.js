"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class IUserGoalsResponse {
    constructor() {
        this.type = "";
        this.id = "";
        this.attributes = {
            // this shouldent be the default
            // should ther ever be a default for goals its not localised either
            type: "Lose Weight",
            createdOn: "",
            completedOn: "",
            percentage: "0",
            payload: {
                targetLoseAmount: 0,
                preferredUnit: "kg",
                helpSubject: "",
            }
        };
    }
}
exports.IUserGoalsResponse = IUserGoalsResponse;

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class ITimelineItemFitnessRunPayload {
    constructor() {
        this.distance = 0;
        this.duration = 0;
        this.calories = 0;
        this.description = "";
    }
}
exports.ITimelineItemFitnessRunPayload = ITimelineItemFitnessRunPayload;
class ITimelineItemFitnessCyclePayload {
    constructor() {
        this.distance = 0;
        this.duration = 0;
        this.calories = 0;
        this.description = "";
        this.rpm = 0;
    }
}
exports.ITimelineItemFitnessCyclePayload = ITimelineItemFitnessCyclePayload;
class ITimelineItemFitnessSwimPayload {
    constructor() {
        this.distance = 0;
        this.duration = 0;
        this.poolLengthDistance = 0;
        this.lengthsSwam = 0;
        this.calories = 0;
        this.averageLengthTime = 0;
        this.description = "";
    }
}
exports.ITimelineItemFitnessSwimPayload = ITimelineItemFitnessSwimPayload;
class ITimelineItemFitnessStrengthPayload {
    constructor() {
        this.duration = 0;
        this.calories = 0;
        this.description = "";
    }
}
exports.ITimelineItemFitnessStrengthPayload = ITimelineItemFitnessStrengthPayload;
class ITimelineItemFitnessStrengthSetPayload {
    constructor() {
        this.description = "";
        this.weightExpectedKg = 0;
        this.weightActualKg = 0;
        this.repsExpected = 0;
        this.repsActual = 0;
    }
}
exports.ITimelineItemFitnessStrengthSetPayload = ITimelineItemFitnessStrengthSetPayload;
class ITimelineItemFitnessWalkPayload {
    constructor() {
        this.distance = 0;
        this.duration = 0;
        this.calories = 0;
        this.steps = 0;
        this.description = "";
    }
}
exports.ITimelineItemFitnessWalkPayload = ITimelineItemFitnessWalkPayload;
class ITimelineItemFitnessPayload {
    constructor() {
        this.distance = 0;
        this.duration = 0;
        this.calories = 0;
        this.steps = 0;
        this.intensity = "";
        this.swimPoolLengthDistance = 0;
        this.swimLengthSwam = 0;
        this.swimAvgLengthTime = 0;
        this.description = "";
    }
}
exports.ITimelineItemFitnessPayload = ITimelineItemFitnessPayload;
class ITimelineItemRoutinePayload {
    constructor() {
        this.distance = 0;
        this.floors = 0;
        this.elevation = 0;
        this.calories = 0;
        this.steps = 0;
        this.description = "";
    }
}
exports.ITimelineItemRoutinePayload = ITimelineItemRoutinePayload;
class ITimelineItemWeightPayload {
    constructor() {
        this.weight = 0;
        this.fatPercent = 0;
        this.height = 0;
        this.massWeight = 0;
        this.freeMass = 0;
        this.bmi = 0;
        this.description = "";
    }
}
exports.ITimelineItemWeightPayload = ITimelineItemWeightPayload;
class ITimelineItemSleepPayload {
    constructor() {
        this.sleepLength = 0;
        this.awake = 0;
        this.deep = 0;
        this.light = 0;
        this.rem = 0;
        this.timesWoken = 0;
    }
}
exports.ITimelineItemSleepPayload = ITimelineItemSleepPayload;
class ITimelineItemNutritionPayload {
    constructor() {
        this.calories = 0;
        this.carbohydrates = 0;
        this.fat = 0;
        this.fiber = 0;
        this.protein = 0;
        this.sodium = 0;
        this.water = 0;
        this.meal = "";
        this.saturatedFat = 0;
        this.polyunsaturatedFat = 0;
        this.monounsaturatedFat = 0;
        this.transFat = 0;
        this.cholesterol = 0;
        this.potassium = 0;
        this.sugar = 0;
        this.vitamin_a = 0;
        this.vitamin_c = 0;
        this.calcium = 0;
        this.iron = 0;
        this.foodName = "";
        this.foodDescription = "";
    }
}
exports.ITimelineItemNutritionPayload = ITimelineItemNutritionPayload;
class ITimelineItemHeartRatePayload {
    constructor() {
        this.average = 0;
        this.resting = 0;
        this.low = 0;
        this.peak = 0;
        this.description = "";
        this.duration = 0;
    }
}
exports.ITimelineItemHeartRatePayload = ITimelineItemHeartRatePayload;
class ITimelineItemMessagePayload {
    constructor() {
        this.message = "";
    }
}
exports.ITimelineItemMessagePayload = ITimelineItemMessagePayload;
// endregion payloads
class ITimelineItemAttributes {
    constructor() {
        this.timestamp = "";
        this.logType = "FITNESS";
        this.logSubType = "";
        this.data = {};
        this.app = "";
        this.children = [];
    }
}
exports.ITimelineItemAttributes = ITimelineItemAttributes;
class ITimelineItemResponse {
    constructor() {
        this.id = 0;
        this.type = "LOG";
        this.attributes = {
            timestamp: "",
            logType: "FITNESS",
            logSubType: "",
            data: {},
            app: "",
            children: [],
        };
    }
}
exports.ITimelineItemResponse = ITimelineItemResponse;

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const redux_observable_1 = require("redux-observable");
const AuthenticateUserEpic_1 = require("./auth/AuthenticateUserEpic");
const setLanguageEpic_1 = require("./epics/setLanguageEpic");
const GetCurrentUserMemberSummaryEpic_1 = require("./Dashboard/GetCurrentUserMemberSummaryEpic");
const GetCurrentUserChartEpic_1 = require("./Dashboard/GetCurrentUserChartEpic");
const GetCurrentUserTimelineEpic_1 = require("./Dashboard/GetCurrentUserTimelineEpic");
exports.RootEpic = redux_observable_1.combineEpics(AuthenticateUserEpic_1.AuthenticateUserEpic, setLanguageEpic_1.setLanguageEpic, GetCurrentUserMemberSummaryEpic_1.GetCurrentUserMemberSummaryEpic, GetCurrentUserChartEpic_1.GetCurrentUserChartEpic, GetCurrentUserTimelineEpic_1.GetCurrentUserTimelineEpic);

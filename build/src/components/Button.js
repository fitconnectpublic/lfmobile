"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const React = require("react");
const react_1 = require("react");
const react_native_1 = require("react-native");
const ButtonStyles_1 = require("./ButtonStyles");
class Button extends react_1.Component {
    render() {
        return (React.createElement(react_native_1.TouchableOpacity, { style: react_native_1.StyleSheet.flatten([
                ButtonStyles_1.ButtonStyles.button,
                this.props.color === "primary" || !this.props.color ? ButtonStyles_1.ButtonStyles.buttonPrimary : null,
                this.props.color === "transparent" ? ButtonStyles_1.ButtonStyles.buttonTransparent : null,
                this.props.disabled && this.props.color === "transparent" ? ButtonStyles_1.ButtonStyles.buttonDisabledTransparent : null,
                this.props.disabled && this.props.color === "primary" ? ButtonStyles_1.ButtonStyles.buttonDisabledPrimary : null,
                this.props.fullWidth ? ButtonStyles_1.ButtonStyles.buttonFullWidth : null,
            ]), disabled: this.props.disabled, onPress: this.props.onPress },
            React.createElement(react_native_1.Text, { style: react_native_1.StyleSheet.flatten([
                    ButtonStyles_1.ButtonStyles.text,
                    this.props.color === "transparent" ? ButtonStyles_1.ButtonStyles.textTransparent : null,
                ]) }, this.props.title)));
    }
}
exports.Button = Button;

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const React = require("react");
const react_1 = require("react");
const react_native_1 = require("react-native");
const DropdownInputStyles_1 = require("./DropdownInputStyles");
class DropdownInput extends react_1.Component {
    render() {
        return (React.createElement(react_native_1.View, { style: DropdownInputStyles_1.DropdownInputStyles.container },
            React.createElement(react_native_1.Picker, { style: DropdownInputStyles_1.DropdownInputStyles.picker, selectedValue: this.props.value, onValueChange: (itemValue) => this.props.onSelect(itemValue) }, this.props.options.map((dropdownItem) => {
                return React.createElement(react_native_1.Picker.Item, { label: dropdownItem.text, value: dropdownItem.value });
            }))));
    }
}
exports.DropdownInput = DropdownInput;

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const React = require("react");
const react_1 = require("react");
const react_native_1 = require("react-native");
const react_native_datepicker_1 = require("react-native-datepicker");
const merge = require("ramda/src/merge");
const DateInputStyles_1 = require("./DateInputStyles");
const react_localize_redux_1 = require("react-localize-redux");
const react_redux_1 = require("react-redux");
const mapStateToProps = (state, props) => {
    return merge({
        translate: react_localize_redux_1.getTranslate(state.locale),
    }, props);
};
class DateInput extends react_1.Component {
    render() {
        return (React.createElement(react_native_1.View, { style: DateInputStyles_1.DateInputStyles.container },
            React.createElement(react_native_1.Text, { style: DateInputStyles_1.DateInputStyles.label }, this.props.title),
            React.createElement(react_native_1.View, { style: DateInputStyles_1.DateInputStyles.datePickerView },
                React.createElement(react_native_datepicker_1.default, { date: this.props.date, mode: "date", placeholder: this.props.placeholder, format: "ddd, D MMM YYYY", confirmBtnText: this.props.translate("LfMobile.DateInput.Confirm"), cancelBtnText: this.props.translate("LfMobile.DateInput.Cancel"), customStyles: {
                        dateInput: DateInputStyles_1.DateInputStyles.dateInput,
                        placeholderText: DateInputStyles_1.DateInputStyles.placeholderText,
                        dateText: DateInputStyles_1.DateInputStyles.dateText,
                    }, onDateChange: (date) => this.props.onDateChange(date), showIcon: false }))));
    }
}
exports.default = react_redux_1.connect(mapStateToProps)(DateInput);

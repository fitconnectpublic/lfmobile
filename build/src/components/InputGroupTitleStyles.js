"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const react_native_1 = require("react-native");
const VariablesColors_1 = require("./../VariablesColors");
exports.InputGroupTitleStyles = react_native_1.StyleSheet.create({
    container: {
        paddingTop: 50,
        paddingLeft: 25,
        paddingRight: 25,
        paddingBottom: 15,
    },
    title: {
        color: VariablesColors_1.colors.grayscale4,
    },
});

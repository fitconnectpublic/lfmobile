"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const react_native_1 = require("react-native");
exports.TimelineLogStatStyles = react_native_1.StyleSheet.create({
    bold: {
        fontWeight: "bold",
    },
    text: {
        fontSize: 12,
    },
});

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const React = require("react");
const react_1 = require("react");
const react_native_1 = require("react-native");
const TimelineLogStatStyles_1 = require("./TimelineLogStatStyles");
class TimelineLogStat extends react_1.Component {
    render() {
        return (React.createElement(react_native_1.Text, { style: TimelineLogStatStyles_1.TimelineLogStatStyles.text },
            React.createElement(react_native_1.Text, { style: TimelineLogStatStyles_1.TimelineLogStatStyles.bold },
                this.props.title,
                " "),
            this.props.text));
    }
}
exports.TimelineLogStat = TimelineLogStat;

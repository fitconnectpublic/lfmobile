"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const react_native_1 = require("react-native");
const VariablesColors_1 = require("./../VariablesColors");
exports.DropdownInputStyles = react_native_1.StyleSheet.create({
    picker: {
        color: VariablesColors_1.colors.grayscale5,
    },
    container: {
        backgroundColor: VariablesColors_1.colors.grayscale0,
        paddingLeft: 18,
        paddingRight: 18,
        paddingTop: 6,
        paddingBottom: 6,
        borderTopWidth: 1,
        borderTopColor: VariablesColors_1.colors.grayscale3,
        borderBottomWidth: 1,
        borderBottomColor: VariablesColors_1.colors.grayscale3,
    },
});

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const react_native_1 = require("react-native");
exports.ChartGranularityPickerStyles = react_native_1.StyleSheet.create({
    root: {
        flex: 1,
        alignSelf: "stretch",
    },
});
exports.default = exports.ChartGranularityPickerStyles;

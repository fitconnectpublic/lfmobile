"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const React = require("react");
const react_1 = require("react");
const react_native_1 = require("react-native");
const react_redux_1 = require("react-redux");
const react_localize_redux_1 = require("react-localize-redux");
const ChartIdPickerStyles_1 = require("./ChartIdPickerStyles");
const mapStateToProps = (state) => {
    return {
        translate: react_localize_redux_1.getTranslate(state.locale),
    };
};
const mapDispatchToProps = (dispatch) => {
    return {};
};
class ChartIdPicker extends react_1.Component {
    render() {
        return (React.createElement(react_native_1.Picker, { style: ChartIdPickerStyles_1.default.root, selectedValue: this.props.selectedValue, onValueChange: this.props.onValueChange },
            React.createElement(react_native_1.Picker.Item, { label: this.props.translate("InsightsWidget.ProfileChart.CaloriesConsumed"), value: "CaloriesConsumed" }),
            React.createElement(react_native_1.Picker.Item, { label: this.props.translate("InsightsWidget.ProfileChart.Steps"), value: "Steps" }),
            React.createElement(react_native_1.Picker.Item, { label: this.props.translate("InsightsWidget.ProfileChart.Sleep"), value: "Sleep" }),
            React.createElement(react_native_1.Picker.Item, { label: this.props.translate("InsightsWidget.ProfileChart.WorkoutTime"), value: "WorkoutTime" }),
            React.createElement(react_native_1.Picker.Item, { label: this.props.translate("InsightsWidget.ProfileChart.NumberOfWorkouts"), value: "NumberOfWorkouts" }),
            React.createElement(react_native_1.Picker.Item, { label: this.props.translate("InsightsWidget.ProfileChart.Weight"), value: "Weight" }),
            React.createElement(react_native_1.Picker.Item, { label: this.props.translate("InsightsWidget.ProfileChart.BodyFat"), value: "BodyFat" })));
    }
}
exports.ChartIdPicker = ChartIdPicker;
exports.default = react_redux_1.connect(mapStateToProps, mapDispatchToProps)(ChartIdPicker);

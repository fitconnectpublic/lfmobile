"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const react_native_1 = require("react-native");
const VariablesColors_1 = require("./../VariablesColors");
exports.TimelineLogDetailsStyles = react_native_1.StyleSheet.create({
    container: {
        flexDirection: "row",
        backgroundColor: VariablesColors_1.colors.grayscale0,
        minHeight: 100,
    },
    bold: {
        fontWeight: "bold",
    },
    text: {
        fontSize: 10,
    },
    header: {
        fontSize: 12,
        fontWeight: "bold",
        marginTop: 10,
    },
});

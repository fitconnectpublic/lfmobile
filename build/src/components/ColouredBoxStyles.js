"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const react_native_1 = require("react-native");
exports.ColouredBoxStyles = react_native_1.StyleSheet.create({
    box: {
        backgroundColor: "#E9E9E9",
        padding: 25,
    },
});

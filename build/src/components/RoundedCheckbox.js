"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const React = require("react");
const react_1 = require("react");
const react_native_1 = require("react-native");
const RoundedCheckboxStyles_1 = require("./RoundedCheckboxStyles");
class RoundedCheckbox extends react_1.Component {
    render() {
        const containerStyles = [RoundedCheckboxStyles_1.RoundedCheckboxStyles.container];
        const checkStyles = [RoundedCheckboxStyles_1.RoundedCheckboxStyles.check];
        if (this.props.checked) {
            containerStyles.push(RoundedCheckboxStyles_1.RoundedCheckboxStyles.containerChecked);
            checkStyles.push(RoundedCheckboxStyles_1.RoundedCheckboxStyles.checkChecked);
        }
        return (React.createElement(react_native_1.View, { style: containerStyles },
            React.createElement(react_native_1.Image, { style: checkStyles, source: require("./../../images/check.png") })));
    }
}
exports.RoundedCheckbox = RoundedCheckbox;

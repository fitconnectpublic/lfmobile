"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const React = require("react");
const react_native_1 = require("react-native");
const react_native_svg_charts_1 = require("react-native-svg-charts");
class Chart extends React.Component {
    render() {
        return (React.createElement(react_native_1.View, null,
            React.createElement(react_native_svg_charts_1.BarChart, { style: { height: 200 }, data: [
                    {
                        values: this.props.values,
                        positive: {
                            fill: "#0a7abf",
                        },
                    },
                ] }),
            React.createElement(react_native_svg_charts_1.XAxis, { style: { paddingVertical: 16 }, values: this.props.labels, formatLabel: (value) => value, chartType: react_native_svg_charts_1.XAxis.Type.BAR, labelStyle: { color: "#4a4a4a" } })));
    }
}
exports.default = Chart;

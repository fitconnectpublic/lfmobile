"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const react_native_1 = require("react-native");
exports.TextBoldTitleStyles = react_native_1.StyleSheet.create({
    title: {
        color: "#4A4A4A",
        fontWeight: "bold",
    },
});

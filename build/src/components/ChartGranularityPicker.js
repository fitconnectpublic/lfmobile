"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const React = require("react");
const react_1 = require("react");
const react_native_1 = require("react-native");
const react_redux_1 = require("react-redux");
const react_localize_redux_1 = require("react-localize-redux");
const ChartGranularityPickerStyles_1 = require("./ChartGranularityPickerStyles");
const mapStateToProps = (state) => {
    return {
        translate: react_localize_redux_1.getTranslate(state.locale),
    };
};
const mapDispatchToProps = (dispatch) => {
    return {};
};
class DayWeekMonthPicker extends react_1.Component {
    render() {
        return (React.createElement(react_native_1.Picker, { style: ChartGranularityPickerStyles_1.default.root, selectedValue: this.props.selectedValue, onValueChange: this.props.onValueChange },
            React.createElement(react_native_1.Picker.Item, { label: this.props.translate("InsightsWidget.ProfileChart.Days"), value: "days" }),
            React.createElement(react_native_1.Picker.Item, { label: this.props.translate("InsightsWidget.ProfileChart.Weeks"), value: "weeks" }),
            React.createElement(react_native_1.Picker.Item, { label: this.props.translate("InsightsWidget.ProfileChart.Months"), value: "months" })));
    }
}
exports.DayWeekMonthPicker = DayWeekMonthPicker;
exports.default = react_redux_1.connect(mapStateToProps, mapDispatchToProps)(DayWeekMonthPicker);

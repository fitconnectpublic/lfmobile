"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const react_native_1 = require("react-native");
const VariablesColors_1 = require("./../VariablesColors");
exports.TimelineRowStyles = react_native_1.StyleSheet.create({
    container: {
        flexDirection: "row",
        backgroundColor: VariablesColors_1.colors.grayscale0,
        minHeight: 100,
    },
    background: {
        justifyContent: "center",
        alignItems: "center",
        flexDirection: "row",
    },
    image: {
        height: 30,
        width: 30,
        resizeMode: "contain",
    },
    innerContainer: {
        flex: 1,
        flexDirection: "row",
        justifyContent: "space-between",
        alignItems: "center",
        padding: 10,
        borderLeftWidth: 2,
        borderColor: VariablesColors_1.colors.lightGray,
        paddingLeft: 39.5,
        marginLeft: 39.5,
        paddingTop: 20,
        paddingBottom: 20,
    },
    imageContainer: {
        position: "absolute",
        zIndex: 100,
        marginTop: 20,
        marginLeft: 10,
        marginRight: 10,
        marginBottom: 20,
        padding: 15,
        borderRadius: 50,
    },
    textContainer: {
        flex: 9,
    },
    appIconContainer: {
        flexDirection: "row",
        justifyContent: "flex-end",
        alignContent: "center",
        alignItems: "center",
        alignSelf: "flex-start",
        marginTop: 5,
    },
    appIcon: {
        width: 20,
        height: 20,
        marginRight: 5,
    },
    time: {
        fontSize: 12,
        color: VariablesColors_1.colors.grayscale4,
    },
    chevron: {
        height: 10,
        width: 10,
        resizeMode: "contain",
        tintColor: VariablesColors_1.colors.grayscale4,
    },
});

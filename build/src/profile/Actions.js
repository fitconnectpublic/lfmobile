"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.apiGetUserGoalSuccess = (goal) => {
    return {
        type: "ApiGetUserGoalSuccess",
        goal,
    };
};
exports.apiGetMemberSummarySuccess = (memberSummary) => {
    return {
        type: "ApiGetMemberSummarySuccess",
        memberSummary,
    };
};
exports.apiPostMessageSuccess = () => {
    return {
        type: "ApiPostMessageSuccess",
    };
};
exports.userSendsMessage = () => {
    return {
        type: "UserSendsMessage",
    };
};
exports.userClosesMessageModal = () => {
    return {
        type: "UserClosesMessageModal",
    };
};
exports.userOpensMessageModal = () => {
    return {
        type: "UserOpensMessageModal",
    };
};
exports.userTypesInMessageInput = (message) => {
    return {
        type: "UserTypesInMessageInput",
        message,
    };
};
exports.UserViewsInlineProfile = (memberId) => {
    return {
        type: "UserViewsInlineProfile",
        memberId,
    };
};

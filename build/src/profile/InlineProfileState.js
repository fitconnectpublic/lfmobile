"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class InlineProfileState {
    constructor() {
        this.memberSummary = {
            firstName: "",
            lastName: "",
            id: 0,
            email: "",
            timesVisitedThisWeek: 0,
            timesVisitedDifferenceThanLastWeek: 0,
            lessOrMoreThanLastWeek: "less",
            isAtRiskOfLeaving: false,
            isExercisingButNotProgressing: false,
            goal: {
                name: "",
                progress: 0,
                summary: "",
            },
            connectedApps: [],
            dateAccountStarted: "",
        };
        this.isLoading = true;
        this.activeMemberId = 0;
        this.messageModal = {
            message: "",
            isModalVisible: false,
            isSendingMessage: false,
            messageSent: false,
        };
    }
}
exports.InlineProfileState = InlineProfileState;

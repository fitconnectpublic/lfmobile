"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */
const React = require("react");
const react_native_1 = require("react-native");
const react_redux_1 = require("react-redux");
const Store_1 = require("./Store");
const config_1 = require("./../config");
const react_localize_redux_1 = require("react-localize-redux");
const en_1 = require("./localisation/en");
const Actions_1 = require("./auth/Actions");
const TestNativeReact = react_native_1.NativeModules.TestNativeReact;
TestNativeReact.get();
const HomeScreenContainer_1 = require("./containers/HomeScreenContainer");
const instructions = react_native_1.Platform.select({
    ios: "Press Cmd+R to reload,\n" +
        "Cmd+D or shake for dev menu",
    android: "Double tap R on your keyboard to reload,\n" +
        "Shake or press menu button for dev menu",
});
const store = Store_1.makeStore();
const supportedLanguages = [
    "en_GB",
    "en_US",
];
store.dispatch(react_localize_redux_1.initialize(supportedLanguages, { defaultLanguage: "en_US" }));
store.dispatch(react_localize_redux_1.addTranslationForLanguage(en_1.default, "en_GB"));
store.dispatch(react_localize_redux_1.addTranslationForLanguage(en_1.default, "en_US"));
store.dispatch(react_localize_redux_1.setActiveLanguage("en_US"));
class App extends React.Component {
    constructor() {
        super(...arguments);
        this.state = {
            test: true,
        };
        this.run = [
            { latitude: 51.468330, longitude: -2.613448 },
            { latitude: 51.465247, longitude: -2.610351 },
            { latitude: 51.464792, longitude: -2.612754 },
            { latitude: 51.464966, longitude: -2.613569 },
            { latitude: 51.467466, longitude: -2.614867 },
            { latitude: 51.468328, longitude: -2.613494 },
            { latitude: 51.467992, longitude: -2.613316 },
        ];
    }
    fitPadding() {
        let maxLat = null;
        let maxLng = null;
        let minLat = null;
        let minLng = null;
        this.run.forEach((LatLng) => {
            if (maxLat == null || LatLng.latitude > maxLat) {
                maxLat = LatLng.latitude;
            }
            if (maxLng == null || LatLng.longitude > maxLng) {
                maxLng = LatLng.longitude;
            }
            if (minLat == null || LatLng.latitude < minLat) {
                minLat = LatLng.latitude;
            }
            if (minLng == null || LatLng.longitude < minLng) {
                minLng = LatLng.longitude;
            }
        });
        this.map.fitToCoordinates([
            { latitude: maxLat, longitude: maxLng },
            { latitude: minLat, longitude: minLng },
        ], {
            edgePadding: { top: 100, right: 100, bottom: 100, left: 100 },
            animated: false,
        });
    }
    componentDidMount() {
        store.dispatch(Actions_1.AuthenticateAction(config_1.config.token, config_1.config.secret));
    }
    render() {
        return (React.createElement(react_redux_1.Provider, { store: store },
            React.createElement(react_native_1.ScrollView, { style: styles.container, contentContainerStyle: styles.contentContainer },
                React.createElement(HomeScreenContainer_1.default, null))));
    }
}
exports.default = App;
const styles = react_native_1.StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#e9e9e9",
    },
    contentContainer: {
        justifyContent: "center",
        alignItems: "center",
    },
    welcome: {
        fontSize: 20,
        textAlign: "center",
        margin: 10,
    },
    instructions: {
        textAlign: "center",
        color: "#333333",
        marginBottom: 5,
    },
});

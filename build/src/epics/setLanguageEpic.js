"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
require("rxjs/add/operator/filter");
require("rxjs/add/operator/map");
const react_localize_redux_1 = require("react-localize-redux");
exports.setLanguageEpic = (action$, stateMiddleware) => {
    return action$
        .filter((action) => {
        return action.type === "LoadLFMobile";
    })
        .map(() => {
        // let locale = getQueryParameterByName("locale");
        // if (! locale || ! contains(locale, supportedLanguages)) {
        //     locale = "en_US";
        // }
        return react_localize_redux_1.setActiveLanguage("en_US");
    });
};

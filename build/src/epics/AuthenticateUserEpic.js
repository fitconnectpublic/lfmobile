"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Observable_1 = require("rxjs/Observable");
require("rxjs/add/operator/switchMap");
require("rxjs/add/operator/filter");
require("rxjs/add/operator/map");
require("rxjs/add/operator/catch");
require("rxjs/add/observable/empty");
// Misc
const config_1 = require("./../config");
const ObservableFetch_1 = require("./../functions/ObservableFetch");
const AuthenticationSuccessAction_1 = require("./../actions/AuthenticationSuccessAction");
const AuthenticationErrorAction_1 = require("./../actions/AuthenticationErrorAction");
const AuthenticationUserNotFoundAction_1 = require("./../actions/AuthenticationUserNotFoundAction");
exports.AuthenticateUserEpic = (action$, stateMiddleware) => {
    return action$
        .filter((action) => {
        return action.type === "Authenticate";
    })
        .switchMap(() => {
        const state = stateMiddleware.getState();
        const formData = new FormData();
        formData.append("access_token", config_1.config.token);
        formData.append("access_secret", config_1.config.secret);
        return ObservableFetch_1.ObservableFetch(`${config_1.config.baseUrl}oauth/life-fitness-mobile`, {
            method: "POST",
            body: formData,
        })
            .catch(() => Observable_1.Observable.empty());
    })
        .map((response) => {
        let authResponse = response.body;
        let errorResponse = response.body;
        // console.log(response);
        if (response.status === 200) {
            return AuthenticationSuccessAction_1.AuthenticationSuccessAction(authResponse.accessToken);
        }
        if (response.status === 500 && errorResponse.error.code === "UserNotFoundException") {
            return AuthenticationUserNotFoundAction_1.AuthenticationUserNotFoundAction(errorResponse);
        }
        return AuthenticationErrorAction_1.AuthenticationErrorAction(errorResponse);
    });
};

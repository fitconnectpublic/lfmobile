"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
// Maps id's of connected apps returned from the database to urls of images
exports.ConnectedAppIcons = {
    0: "",
    // Argus
    11: "https://www.fitconnect.io/assets/images/app-icons/35px/argus.jpg",
    // Azumio
    36: "https://www.fitconnect.io/assets/images/app-icons/35px/azumio.jpg",
    // Exist
    29: "https://www.fitconnect.io/assets/images/app-icons/35px/exist.jpg",
    // FatSecret
    9: "https://www.fitconnect.io/assets/images/app-icons/35px/fatsecret.jpg",
    // Fitbit
    10: "https://www.fitconnect.io/assets/images/app-icons/35px/fitbit.jpg",
    // Fitness Buddy
    14: "https://www.fitconnect.io/assets/images/app-icons/35px/fitnessbuddy.jpg",
    // Garmin Connect
    38: "https://www.fitconnect.io/assets/images/app-icons/35px/garmin.jpg",
    // Glucose Buddy
    15: "https://www.fitconnect.io/assets/images/app-icons/35px/glucosebuddy.jpg",
    // Google Fit
    30: "https://www.fitconnect.io/assets/images/app-icons/35px/googlefit.jpg",
    // Instant Heart Rate
    12: "https://www.fitconnect.io/assets/images/app-icons/35px/instantheartrate.jpg",
    // Jawbone UP
    17: "https://www.fitconnect.io/assets/images/app-icons/35px/jawboneup.jpg",
    // LFconnect
    20: "https://www.fitconnect.io/assets/images/app-icons/35px/lfconnect.jpg",
    // MapMyFit
    5: "https://www.fitconnect.io/assets/images/app-icons/35px/mapmyfit.jpg",
    // MapMyHike
    6: "https://www.fitconnect.io/assets/images/app-icons/35px/mapmyhike.jpg",
    // MapMyRide
    4: "https://www.fitconnect.io/assets/images/app-icons/35px/mapmyride.jpg",
    // MapMyRun
    3: "https://www.fitconnect.io/assets/images/app-icons/35px/mapmyrun.jpg",
    // MapMyWalk
    7: "https://www.fitconnect.io/assets/images/app-icons/35px/mapmywalk.jpg",
    // Microsoft Health
    27: "https://www.fitconnect.io/assets/images/app-icons/35px/mshealth.jpg",
    // My Fitness Pal
    37: "https://www.fitconnect.io/assets/images/app-icons/35px/myfitnesspal.jpg",
    // Myzone
    28: "https://www.fitconnect.io/assets/images/app-icons/35px/myzone.jpg",
    // Runkeeper:
    8: "https://www.fitconnect.io/assets/images/app-icons/35px/runkeeper.jpg",
    // Sleep Time
    13: "https://www.fitconnect.io/assets/images/app-icons/35px/sleeptime.jpg",
    // Strava
    22: "https://www.fitconnect.io/assets/images/app-icons/35px/strava.jpg",
    // Swimtag
    35: "https://www.fitconnect.io/assets/images/app-icons/35px/swimtag.jpg",
    // Withings
    26: "https://www.fitconnect.io/assets/images/app-icons/35px/withings.jpg",
    // iHealth
    31: "https://www.fitconnect.io/assets/images/app-icons/35px/ihealth.jpg",
    // UA Record:
    2: "https://www.fitconnect.io/assets/images/app-icons/35px/ua-record.jpg",
    // Bodytrax
    41: "https://www.fitconnect.io/assets/images/app-icons/35px/boditrax.jpg",
    // Forsky
    40: "https://www.fitconnect.io/assets/images/app-icons/35px/forksy.jpg",
    // Gympact
    16: "https://www.fitconnect.io/assets/images/app-icons/35px/gympact.jpg",
    // Loseit
    18: "https://www.fitconnect.io/assets/images/app-icons/35px/loseit.jpg",
    // Lumo
    32: "https://www.fitconnect.io/assets/images/app-icons/35px/lumo.jpg",
    // Misfit
    24: "https://www.fitconnect.io/assets/images/app-icons/35px/misfit.jpg",
    // Moves
    21: "https://www.fitconnect.io/assets/images/app-icons/35px/moves.jpg",
    // Polar
    34: "https://www.fitconnect.io/assets/images/app-icons/35px/polar.jpg",
    // Vita-dock
    33: "https://www.fitconnect.io/assets/images/app-icons/35px/vitadock.jpg",
    // Wego
    19: "https://www.fitconnect.io/assets/images/app-icons/35px/wego.jpg",
};
exports.ConnectedStringAppIcons = {
    "0": "",
    // Argus
    "argus": "https://www.fitconnect.io/assets/images/app-icons/35px/argus.jpg",
    // Azumio
    "azumio": "https://www.fitconnect.io/assets/images/app-icons/35px/azumio.jpg",
    // Exist
    "exist": "https://www.fitconnect.io/assets/images/app-icons/35px/exist.jpg",
    // FatSecret
    "fatsecret": "https://www.fitconnect.io/assets/images/app-icons/35px/fatsecret.jpg",
    // Fitbit
    "fitbit": "https://www.fitconnect.io/assets/images/app-icons/35px/fitbit.jpg",
    // Fitness Buddy
    "fitnessbuddy": "https://www.fitconnect.io/assets/images/app-icons/35px/fitnessbuddy.jpg",
    // Garmin Connect
    "garmin": "https://www.fitconnect.io/assets/images/app-icons/35px/garmin.jpg",
    // Glucose Buddy
    "glucosebuddy": "https://www.fitconnect.io/assets/images/app-icons/35px/glucosebuddy.jpg",
    // Google Fit
    "googlefit": "https://www.fitconnect.io/assets/images/app-icons/35px/googlefit.jpg",
    // Instant Heart Rate
    "instantheartrate": "https://www.fitconnect.io/assets/images/app-icons/35px/instantheartrate.jpg",
    // Jawbone UP
    "jawboneup": "https://www.fitconnect.io/assets/images/app-icons/35px/jawboneup.jpg",
    // LFconnect
    "lfconnect": "https://www.fitconnect.io/assets/images/app-icons/35px/lfconnect.jpg",
    // MapMyFit
    "mapmyfit": "https://www.fitconnect.io/assets/images/app-icons/35px/mapmyfit.jpg",
    // MapMyHike
    "mapmyhike": "https://www.fitconnect.io/assets/images/app-icons/35px/mapmyhike.jpg",
    // MapMyRide
    "mapmyride": "https://www.fitconnect.io/assets/images/app-icons/35px/mapmyride.jpg",
    // MapMyRun
    "mapmyrun": "https://www.fitconnect.io/assets/images/app-icons/35px/mapmyrun.jpg",
    // MapMyWalk
    "mapmywalk": "https://www.fitconnect.io/assets/images/app-icons/35px/mapmywalk.jpg",
    // Microsoft Health
    "mshealth": "https://www.fitconnect.io/assets/images/app-icons/35px/mshealth.jpg",
    // My Fitness Pal
    "myfitnesspal": "https://www.fitconnect.io/assets/images/app-icons/35px/myfitnesspal.jpg",
    // Myzone
    "myzone": "https://www.fitconnect.io/assets/images/app-icons/35px/myzone.jpg",
    // Runkeeper:
    "runkeeper": "https://www.fitconnect.io/assets/images/app-icons/35px/runkeeper.jpg",
    // Sleep Time
    "sleeptime": "https://www.fitconnect.io/assets/images/app-icons/35px/sleeptime.jpg",
    // Strava
    "strava": "https://www.fitconnect.io/assets/images/app-icons/35px/strava.jpg",
    // Swimtag
    "swimtag": "https://www.fitconnect.io/assets/images/app-icons/35px/swimtag.jpg",
    // Withings
    "withings": "https://www.fitconnect.io/assets/images/app-icons/35px/withings.jpg",
    // iHealth
    "ihealth": "https://www.fitconnect.io/assets/images/app-icons/35px/ihealth.jpg",
    // UA Record:
    "ua-record": "https://www.fitconnect.io/assets/images/app-icons/35px/ua-record.jpg",
    // Bodytrax
    "boditrax": "https://www.fitconnect.io/assets/images/app-icons/35px/boditrax.jpg",
    // Forsky
    "forksy": "https://www.fitconnect.io/assets/images/app-icons/35px/forksy.jpg",
    // Gympact
    "gympact": "https://www.fitconnect.io/assets/images/app-icons/35px/gympact.jpg",
    // Loseit
    "loseit": "https://www.fitconnect.io/assets/images/app-icons/35px/loseit.jpg",
    // Lumo
    "lumo": "https://www.fitconnect.io/assets/images/app-icons/35px/lumo.jpg",
    // Misfit
    "misfit": "https://www.fitconnect.io/assets/images/app-icons/35px/misfit.jpg",
    // Moves
    "moves": "https://www.fitconnect.io/assets/images/app-icons/35px/moves.jpg",
    // Polar
    "polar": "https://www.fitconnect.io/assets/images/app-icons/35px/polar.jpg",
    // Vita-dock
    "vitadock": "https://www.fitconnect.io/assets/images/app-icons/35px/vitadock.jpg",
    // Wego
    "wego": "https://www.fitconnect.io/assets/images/app-icons/35px/wego.jpg",
};

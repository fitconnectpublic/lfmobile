"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.AuthenticateAction = (token, secret) => {
    return {
        type: "Authenticate",
        token,
        secret,
    };
};
exports.AuthenticationErrorAction = (error) => {
    return {
        type: "AuthenticationError",
        error,
    };
};
exports.AuthenticationSuccessAction = (token) => {
    return {
        type: "AuthenticationSuccess",
        token,
    };
};
exports.AuthenticationUserNotFoundAction = (error) => {
    return {
        type: "AuthenticationUserNotFound",
        error,
    };
};

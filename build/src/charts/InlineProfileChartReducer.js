"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
// State & Mutators
const InlineProfileChartState_1 = require("./InlineProfileChartState");
// import pipe from "ramda/src/pipe";
const ramda_1 = require("ramda");
function MakeInlineProfileChartReducer() {
    return (state = new InlineProfileChartState_1.InlineProfileChartState(), action) => {
        switch (action.type) {
            case "UserSelectsChart":
                return ramda_1.pipe(ramda_1.assoc("selectedChart", action.chartId), ramda_1.assoc("isLoading", true))(state);
            case "UserSelectsChartGranularity":
                return ramda_1.pipe(ramda_1.assoc("selectedGranularity", action.chartGranularity), ramda_1.assoc("isLoading", true))(state);
            case "ApiGetChartSuccess":
                return ramda_1.pipe(ramda_1.assoc("barChart", action.chart), ramda_1.assoc("isLoading", false))(state);
            case "UserViewsFullProfile":
                return ramda_1.pipe(ramda_1.assoc("isLoading", true))(state);
            default:
                return state;
        }
    };
}
exports.MakeInlineProfileChartReducer = MakeInlineProfileChartReducer;

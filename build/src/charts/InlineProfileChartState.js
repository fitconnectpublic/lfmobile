"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class InlineProfileChartState {
    constructor() {
        this.barChart = {
            values: [],
            labels: [],
        };
        this.selectedGranularity = "weeks";
        this.selectedChart = "NumberOfWorkouts";
        this.yAxisLabel = "";
        this.isLoading = true;
    }
}
exports.InlineProfileChartState = InlineProfileChartState;

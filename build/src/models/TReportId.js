"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.TReportIdToTReportNumericId = {
    All: -1,
    TrainingForAnEvent: 7,
    ImproveHealth: 9,
    AtRisk: 4,
    LoseWeight: 8,
    RewardPoints: 19,
    GetInShape: 11,
    NotProgressing: 32,
    DoingWell: 14,
    BuildMuscle: 12,
    NewSignup: 17,
    AbandonedAccounts: 16,
};

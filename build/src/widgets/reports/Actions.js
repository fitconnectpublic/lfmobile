"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.apiGetAllExercisersSuccess = (reportSet) => {
    return {
        type: "ApiGetAllExercisersSuccess",
        reportSet,
    };
};
exports.apiGetCountsSuccess = (tabs) => {
    return {
        type: "ApiGetCountsSuccess",
        tabs,
    };
};
exports.apiGetHeatmapDataSuccess = (heatmapDataSet) => {
    return {
        type: "ApiGetHeatmapDataSuccess",
        heatmapDataSet,
    };
};
exports.apiGetReportsSuccess = (reportSet) => {
    return {
        type: "ApiGetReportsSuccess",
        reportSet,
    };
};
exports.goToFirstExerciseReportPageAction = () => {
    return {
        type: "GoToFirstExerciseReportPage",
    };
};
exports.goToLastExerciseReportPageAction = () => {
    return {
        type: "GoToLastExerciseReportPage",
    };
};
exports.goToNextExerciseReportPageAction = () => {
    return {
        type: "GoToNextExerciseReportPage",
    };
};
exports.goToPreviousExerciseReportPageAction = () => {
    return {
        type: "GoToPreviousExerciseReportPageAction",
    };
};
exports.goToSpecificExerciseReportPageAction = (pageIndex) => {
    return {
        type: "GoToSpecificExerciseReportPageAction",
        pageIndex,
    };
};
exports.userChangeHeatmapPeriod = (period) => {
    return {
        type: "UserChangeHeatmapPeriod",
        period,
    };
};
exports.userSelectsReport = (reportId) => {
    return {
        type: "UserSelectsReport",
        reportId,
    };
};
exports.userSelectsReportCategory = (reportCategoryId) => {
    return {
        type: "UserSelectsReportCategory",
        reportCategoryId,
    };
};
exports.userTogglesInGym = () => {
    return {
        type: "UserTogglesInGym",
    };
};
exports.userViewsHeatmap = () => {
    return {
        type: "UserViewsHeatmap",
    };
};
exports.userViewsInlineProfile = (memberId) => {
    return {
        type: "UserViewsInlineProfile",
        memberId,
    };
};
exports.userViewsExerciserReports = () => {
    return {
        type: "UserViewsExerciserReports",
    };
};

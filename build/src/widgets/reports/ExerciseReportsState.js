"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const TReportId_1 = require("../../models/TReportId");
class ExerciseReportsState {
    constructor() {
        this.inGym = false;
        this.isReportLoading = true;
        this.reportControls = {
            isLoadingReportCounts: false,
            pageIndex: 0,
            pageSize: 8,
            totalPageCount: 1,
            selectedReportCategory: "AllExercisers",
            selectedReport: "All",
            selectedReportNumericId: TReportId_1.TReportIdToTReportNumericId.All,
            visibleTabs: [
                {
                    count: 0,
                    reportId: "All",
                    reportCategoryId: ["AllExercisers"],
                },
            ],
            allTabs: [
                {
                    count: 0,
                    reportId: "All",
                    reportCategoryId: ["AllExercisers"],
                },
                {
                    count: 0,
                    reportId: "AtRisk",
                    reportCategoryId: ["ExerciserInsights"],
                },
                {
                    count: 0,
                    reportId: "NotProgressing",
                    reportCategoryId: ["ExerciserInsights"],
                },
                {
                    count: 0,
                    reportId: "DoingWell",
                    reportCategoryId: ["ExerciserInsights"],
                },
                {
                    count: 0,
                    reportId: "TrainingForAnEvent",
                    reportCategoryId: ["GoalReports"],
                },
                {
                    count: 0,
                    reportId: "LoseWeight",
                    reportCategoryId: ["GoalReports"],
                },
                {
                    count: 0,
                    reportId: "ImproveHealth",
                    reportCategoryId: ["GoalReports"],
                },
                {
                    count: 0,
                    reportId: "GetInShape",
                    reportCategoryId: ["GoalReports"],
                },
                {
                    count: 0,
                    reportId: "BuildMuscle",
                    reportCategoryId: ["GoalReports"],
                },
                {
                    count: 0,
                    reportId: "NewSignup",
                    reportCategoryId: ["AccountReports"],
                },
                {
                    count: 0,
                    reportId: "AbandonedAccounts",
                    reportCategoryId: ["AccountReports"],
                },
            ],
        };
        this.reports = [
            {
                members: [],
                reportId: "All",
            },
            {
                members: [],
                reportId: "AtRisk",
            },
            {
                members: [],
                reportId: "BuildMuscle",
            },
            {
                members: [],
                reportId: "DoingWell",
            },
            {
                members: [],
                reportId: "GetInShape",
            },
            {
                members: [],
                reportId: "ImproveHealth",
            },
            {
                members: [],
                reportId: "LoseWeight",
            },
            {
                members: [],
                reportId: "NewSignup",
            },
            {
                members: [],
                reportId: "NotProgressing",
            },
            {
                members: [],
                reportId: "RewardPoints",
            },
            {
                members: [],
                reportId: "TrainingForAnEvent",
            },
        ];
        this.heatmapControls = {
            isLoading: true,
            selectedPeriod: "1month",
        };
        this.heatmapData = [];
    }
}
exports.ExerciseReportsState = ExerciseReportsState;

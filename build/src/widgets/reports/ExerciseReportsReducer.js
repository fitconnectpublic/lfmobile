"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
// import assoc from "ramda/src/assoc";
// import assocPath from "ramda/src/assocPath";
// import filter from "ramda/src/filter";
// import contains from "ramda/src/contains";
// import pipe from "ramda/src/pipe";
// import concat from "ramda/src/concat";
// import reject from "ramda/src/reject";
// import propEq from "ramda/src/propEq";
// import find from "ramda/src/find";
const ramda_1 = require("ramda");
// State & Mutators
const ExerciseReportsState_1 = require("./ExerciseReportsState");
const TReportId_1 = require("../../models/TReportId");
function MakeExerciseReportsReducer() {
    return (state = new ExerciseReportsState_1.ExerciseReportsState(), action) => {
        switch (action.type) {
            case "UserSelectsReportCategory":
                return selectCategoryReport(state, action);
            case "UserSelectsReport":
                return ramda_1.pipe(ramda_1.assoc("isReportLoading", true), ramda_1.assocPath(["reportControls", "pageIndex"], 0), ramda_1.assocPath(["reportControls", "selectedReport"], action.reportId), ramda_1.assocPath(["reportControls", "selectedReportNumericId"], TReportId_1.TReportIdToTReportNumericId[action.reportId]))(state);
            case "ApiGetReportsSuccess":
                return apiGetReportsSuccess(state, action);
            case "UserTogglesInGym":
                return ramda_1.assoc("inGym", !state.inGym, state);
            case "UserViewsHeatmap":
                return ramda_1.assocPath(["heatmapControls", "isLoading"], true, state);
            case "ApiGetAllExercisersSuccess":
                return exports.apiGetAllExercisersSuccess(state, action);
            case "ApiGetHeatmapDataSuccess":
                return ramda_1.pipe(ramda_1.assoc("heatmapData", action.heatmapDataSet), ramda_1.assocPath(["heatmapControls", "isLoading"], false))(state);
            case "UserChangeHeatmapPeriod":
                return ramda_1.pipe(ramda_1.assocPath(["heatmapControls", "selectedPeriod"], action.period), ramda_1.assocPath(["heatmapControls", "isLoading"], true))(state);
            case "GoToNextExerciseReportPage":
                return ramda_1.pipe(ramda_1.assoc("isReportLoading", true), ramda_1.assocPath(["reportControls", "pageIndex"], (state.reportControls.pageIndex === state.reportControls.totalPageCount - 1 ?
                    state.reportControls.totalPageCount - 1 :
                    state.reportControls.pageIndex + 1)))(state);
            case "GoToPreviousExerciseReportPageAction":
                return ramda_1.pipe(ramda_1.assoc("isReportLoading", true), ramda_1.assocPath(["reportControls", "pageIndex"], (state.reportControls.pageIndex === 0 ?
                    state.reportControls.pageIndex :
                    state.reportControls.pageIndex - 1)))(state);
            case "GoToSpecificExerciseReportPageAction":
                return ramda_1.pipe(ramda_1.assoc("isReportLoading", true), ramda_1.assocPath(["reportControls", "pageIndex"], action.pageIndex))(state);
            case "GoToFirstExerciseReportPage":
                return ramda_1.pipe(ramda_1.assoc("isReportLoading", true), ramda_1.assocPath(["reportControls", "pageIndex"], 0))(state);
            case "GoToLastExerciseReportPage":
                return ramda_1.pipe(ramda_1.assoc("isReportLoading", true), ramda_1.assocPath(["reportControls", "pageIndex"], state.reportControls.totalPageCount - 1))(state);
            case "UserSelectsSite":
                const defaultState = new ExerciseReportsState_1.ExerciseReportsState();
                return ramda_1.pipe(ramda_1.assoc("isReportLoading", true), ramda_1.assoc("reportControls", defaultState.reportControls), ramda_1.assoc("reports", defaultState.reports), ramda_1.assocPath(["reportControls", "isLoadingReportCounts"], true))(state);
            case "ApiGetCountsSuccess":
                return ramda_1.pipe(ramda_1.assocPath(["reportControls", "allTabs"], action.tabs), ramda_1.assocPath(["reportControls", "isLoadingReportCounts"], false))(state);
            case "ApiGetSitesSuccess":
                return ramda_1.assocPath(["reportControls", "isLoadingReportCounts"], true, state);
            default:
                return state;
        }
    };
}
exports.MakeExerciseReportsReducer = MakeExerciseReportsReducer;
const selectCategoryReport = (state, action) => {
    const reportCategoryTabs = ramda_1.filter((tab) => ramda_1.contains(action.reportCategoryId, tab.reportCategoryId), state.reportControls.allTabs);
    return ramda_1.pipe(ramda_1.assoc("isReportLoading", true), ramda_1.assocPath(["reportControls", "pageIndex"], 0), ramda_1.assocPath(["reportControls", "selectedReportCategory"], action.reportCategoryId), ramda_1.assocPath(["reportControls", "visibleTabs"], reportCategoryTabs), ramda_1.assocPath(["reportControls", "selectedReport"], reportCategoryTabs[0] ? reportCategoryTabs[0].reportId : "All"), ramda_1.assocPath(["reportControls", "selectedReportNumericId"], reportCategoryTabs[0] ? TReportId_1.TReportIdToTReportNumericId[reportCategoryTabs[0].reportId] : -1))(state);
};
const apiGetReportsSuccess = (state, action) => {
    const totalPageCount = Math.ceil((action.reportSet.totalCount || 1) / state.reportControls.pageSize);
    const allExercisersReportSet = ramda_1.find(ramda_1.propEq("reportId", "All"))(state.reports);
    const report = {
        members: action.reportSet.members,
        reportId: action.reportSet.reportId,
    };
    const reports = ramda_1.pipe(ramda_1.reject(ramda_1.propEq("reportId", report.reportId)), ramda_1.concat([report]))(state.reports);
    return ramda_1.pipe(ramda_1.assocPath(["reportControls", "totalPageCount"], totalPageCount), ramda_1.assoc("isReportLoading", false), ramda_1.assoc("reports", reports))(state);
};
exports.apiGetAllExercisersSuccess = (state, action) => {
    const totalPageCount = Math.ceil((action.reportSet.totalCount || 1) / state.reportControls.pageSize);
    const reports = ramda_1.pipe(ramda_1.reject(ramda_1.propEq("reportId", "All")), ramda_1.concat([{
            members: action.reportSet.members,
            reportId: action.reportSet.reportId,
        }]))(state.reports);
    return ramda_1.pipe(ramda_1.assocPath(["reportControls", "totalPageCount"], totalPageCount), ramda_1.assoc("isReportLoading", false), ramda_1.assoc("reports", reports))(state);
};

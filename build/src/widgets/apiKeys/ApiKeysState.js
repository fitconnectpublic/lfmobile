"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class ApiKeysState {
    constructor() {
        this.apiKey = "";
        this.lfFirebaseToken = "";
    }
}
exports.ApiKeysState = ApiKeysState;

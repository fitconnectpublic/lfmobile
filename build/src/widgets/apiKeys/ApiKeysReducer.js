"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
// import assoc from "ramda/src/assoc";
// import pipe from "ramda/src/pipe";
const ramda_1 = require("ramda");
// State
const ApiKeysState_1 = require("./ApiKeysState");
function MakeApiKeysReducer() {
    return (state = new ApiKeysState_1.ApiKeysState(), action) => {
        switch (action.type) {
            case "ApiAuthWithFirebaseSuccess":
                return ramda_1.pipe(ramda_1.assoc("apiKey", action.apiKey), ramda_1.assoc("lfFirebaseToken", action.firebaseToken))(state);
            default:
                return state;
        }
    };
}
exports.MakeApiKeysReducer = MakeApiKeysReducer;

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const ramda_1 = require("ramda");
// import pipe from "ramda/src/pipe";
// import concat from "ramda/src/concat";
// import ifElse from "ramda/src/ifElse";
// State & Mutators
const InlineProfileTimelineState_1 = require("./InlineProfileTimelineState");
function MakeInlineProfileTimelineReducer() {
    return (state = new InlineProfileTimelineState_1.InlineProfileTimelineState(), action) => {
        switch (action.type) {
            case "UserViewsMoreTimeline":
                return ramda_1.pipe(ramda_1.assoc("offset", state.offset + 1), ramda_1.assoc("isLoading", true))(state);
            case "UserViewsFullProfile":
                return ramda_1.pipe(ramda_1.assoc("isLoading", true), ramda_1.assoc("timeline", []), ramda_1.assoc("selectedFilter", "AllActivity"), ramda_1.assoc("offset", 0), ramda_1.assoc("showMoreButtonIsVisble", true))(state);
            case "UserSelectsTimelineFilter":
                return ramda_1.pipe(ramda_1.assoc("selectedFilter", action.timelineFilter))(state);
            case "ApiGetTimelineSuccess":
                return ramda_1.pipe(ramda_1.assoc("isLoading", false), ramda_1.assoc("timeline", ramda_1.concat(state.timeline, action.timeline)), ramda_1.ifElse(() => action.timeline.length === 0, ramda_1.assoc("showMoreButtonIsVisble", false), (s) => s))(state);
            default:
                return state;
        }
    };
}
exports.MakeInlineProfileTimelineReducer = MakeInlineProfileTimelineReducer;

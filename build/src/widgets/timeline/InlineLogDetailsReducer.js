"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
// import assoc from "ramda/src/assoc";
// import pipe from "ramda/src/pipe";
const ramda_1 = require("ramda");
const InlineLogDetailsState_1 = require("./InlineLogDetailsState");
function MakeInlineLogDetailsReducer() {
    return (state = new InlineLogDetailsState_1.InlineLogDetailsState(), action) => {
        switch (action.type) {
            case "UserViewsInlineLogDetails":
                return ramda_1.pipe(ramda_1.assoc("activeLogIndex", action.logIndex))(new InlineLogDetailsState_1.InlineLogDetailsState());
            // case "ApiGetLogDetailsSuccess":
            //     return pipe(
            //     )(state) as InlineLogDetailsState;
            default:
                return state;
        }
    };
}
exports.MakeInlineLogDetailsReducer = MakeInlineLogDetailsReducer;

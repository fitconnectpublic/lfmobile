"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.apiSendPromotionEmailsError = () => {
    return {
        type: "ApiSendPromotionEmailsError",
    };
};
exports.apiSendPromotionEmailsSuccess = () => {
    return {
        type: "ApiSendPromotionEmailsSuccess",
    };
};
exports.userSelectsPromotionType = (promotionType) => {
    return {
        type: "UserSelectsPromotionType",
        promotionType,
    };
};
exports.userSendsPromotionEmails = () => {
    return {
        type: "UserSendsPromotionEmails",
    };
};
exports.userTypesInPromotionEmailsInput = (emails) => {
    return {
        type: "UserTypesInPromotionEmailsInput",
        emails,
    };
};

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
// import assoc from "ramda/src/assoc";
// import pipe from "ramda/src/pipe";
const ramda_1 = require("ramda");
const PromotionState_1 = require("./PromotionState");
function MakePromotionReducer() {
    return (state = new PromotionState_1.PromotionState(), action) => {
        switch (action.type) {
            case "UserSelectsPromotionType":
                return ramda_1.assoc("selectedPromotionType", action.promotionType, state);
            case "UserSendsPromotionEmails":
                return ramda_1.pipe(ramda_1.assoc("isSendingEmails", true))(state);
            case "UserTypesInPromotionEmailsInput":
                return ramda_1.assoc("promotionEmails", action.emails, state);
            case "ApiSendPromotionEmailsSuccess":
                return ramda_1.pipe(ramda_1.assoc("isSendingEmails", false), ramda_1.assoc("hasSentEmails", true), ramda_1.assoc("promotionEmails", ""))(state);
            default:
                return state;
        }
    };
}
exports.MakePromotionReducer = MakePromotionReducer;

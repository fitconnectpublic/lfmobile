"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class PromotionState {
    constructor() {
        this.selectedPromotionType = "PromotionMaterial";
        this.promotionEmails = "";
        this.isSendingEmails = false;
        this.hasSentEmails = false;
    }
}
exports.PromotionState = PromotionState;

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const assoc_1 = require("ramda/src/assoc");
// State & Mutators
const InlineProfileChartState_1 = require("./InlineProfileChartState");
// import pipe from "ramda/src/pipe";
const ramda_1 = require("ramda");
function MakeInlineProfileChartReducer() {
    return (state = new InlineProfileChartState_1.InlineProfileChartState(), action) => {
        switch (action.type) {
            case "UserSelectsChart":
                return ramda_1.pipe(assoc_1.default("selectedChart", action.chartId), assoc_1.default("isLoading", true))(state);
            case "UserSelectsChartGranularity":
                return ramda_1.pipe(assoc_1.default("selectedGranularity", action.chartGranularity), assoc_1.default("isLoading", true))(state);
            case "ApiGetChartSuccess":
                return ramda_1.pipe(assoc_1.default("barChart", action.chart), assoc_1.default("isLoading", false))(state);
            case "UserViewsFullProfile":
                return ramda_1.pipe(assoc_1.default("isLoading", true))(state);
            default:
                return state;
        }
    };
}
exports.MakeInlineProfileChartReducer = MakeInlineProfileChartReducer;

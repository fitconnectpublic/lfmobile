"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class SelectSiteState {
    constructor() {
        this.sites = [];
        this.selectedSiteId = 0;
        this.selectedSite = {
            id: 0,
            name: "",
            linkName: "",
            fitconnectEnabled: true,
            expiryDate: "",
            promotionResourceUrl: "",
        };
        this.isLoading = true;
    }
}
exports.SelectSiteState = SelectSiteState;

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
// import pipe from "ramda/src/pipe";
// import assoc from "ramda/src/assoc";
// import find from "ramda/src/find";
// import propEq from "ramda/src/propEq";
const ramda_1 = require("ramda");
const SelectSiteState_1 = require("./SelectSiteState");
function MakeSelectSiteReducer() {
    return (state = new SelectSiteState_1.SelectSiteState(), action) => {
        switch (action.type) {
            case "ApiGetSitesSuccess":
                return ramda_1.pipe(ramda_1.assoc("sites", action.sites), ramda_1.assoc("selectedSiteId", (action.sites.length > 0 ? action.sites[0].id : 0)), ramda_1.assoc("selectedSite", action.sites.length > 0 ? ramda_1.find(ramda_1.propEq("id", action.sites[0].id), action.sites) : state.selectedSite), ramda_1.assoc("isLoading", false))(state);
            case "UserSelectsSite":
                return ramda_1.pipe(ramda_1.assoc("selectedSiteId", action.siteId), ramda_1.assoc("sites", state.sites), ramda_1.assoc("selectedSite", state.sites.length > 0 ? ramda_1.find(ramda_1.propEq("id", action.siteId), state.sites) : state.selectedSite))(state);
            default:
                return state;
        }
    };
}
exports.MakeSelectSiteReducer = MakeSelectSiteReducer;

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
// import assoc from "ramda/src/assoc";
const ramda_1 = require("ramda");
const scrollToTop_1 = require("../../lib/scrollToTop");
// State & Mutators
const PageState_1 = require("./PageState");
function MakePageReducer() {
    return (state = new PageState_1.PageState(), action) => {
        switch (action.type) {
            case "UserChangesPage":
                scrollToTop_1.scrollToTop();
                return ramda_1.assoc("activePage", action.pageId, state);
            case "UserViewsFullProfile":
                scrollToTop_1.scrollToTop();
                return ramda_1.assoc("activePage", "Profile", state);
            default:
                return state;
        }
    };
}
exports.MakePageReducer = MakePageReducer;

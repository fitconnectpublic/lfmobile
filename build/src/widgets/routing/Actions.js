"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.userChangesPage = (pageId) => {
    return {
        type: "UserChangesPage",
        pageId,
    };
};
exports.userViewsFullProfile = () => {
    return {
        type: "UserViewsFullProfile",
    };
};

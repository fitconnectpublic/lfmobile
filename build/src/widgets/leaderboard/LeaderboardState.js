"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class LeaderboardState {
    constructor() {
        this.members = [];
        this.selectedLeaderboard = 1;
        this.isLoading = true;
        this.pageIndex = 0;
        this.pageSize = 8;
        this.totalPageCount = 1;
    }
}
exports.LeaderboardState = LeaderboardState;

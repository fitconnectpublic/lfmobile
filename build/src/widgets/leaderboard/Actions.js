"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.apiGetLeaderboardSuccess = (leaderboard) => {
    return {
        type: "ApiGetLeaderboardSuccess",
        leaderboard,
    };
};
exports.goToFirstLeaderboardPageAction = () => {
    return {
        type: "GoToFirstLeaderboardPage",
    };
};
exports.goToLastLeaderboardPageAction = () => {
    return {
        type: "GoToLastLeaderboardPage",
    };
};
exports.goToNextLeaderboardPageAction = () => {
    return {
        type: "GoToNextLeaderboardPage",
    };
};
exports.goToPreviousLeaderboardPageAction = () => {
    return {
        type: "GoToPreviousLeaderboardPageAction",
    };
};
exports.goToSpecificLeaderboardPageAction = (pageIndex) => {
    return {
        type: "GoToSpecificLeaderboardPageAction",
        pageIndex,
    };
};
exports.userSelectsLeaderboard = (leaderboardId) => {
    return {
        type: "UserSelectsLeaderboard",
        leaderboardId,
    };
};
exports.userViewsLeaderboardsAction = () => {
    return {
        type: "UserViewsLeaderboards",
    };
};

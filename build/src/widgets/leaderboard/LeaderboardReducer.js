"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
// import assoc from "ramda/src/assoc";
const ramda_1 = require("ramda");
// State & Mutators
const LeaderboardState_1 = require("./LeaderboardState");
// import pipe from "ramda/src/pipe";
const ramda_2 = require("ramda");
function MakeLeaderboardReducer() {
    return (state = new LeaderboardState_1.LeaderboardState(), action) => {
        switch (action.type) {
            case "ApiGetLeaderboardSuccess":
                return apiGetLeaderboardSuccess(state, action);
            case "UserSelectsLeaderboard":
                return ramda_2.pipe(ramda_1.assoc("selectedLeaderboard", action.leaderboardId), ramda_1.assoc("isLoading", true))(state);
            case "GoToNextLeaderboardPage":
                return goToNextLeaderboardPage(state, action);
            case "GoToPreviousLeaderboardPageAction":
                return goToPreviousLeaderboardPage(state, action);
            case "GoToSpecificLeaderboardPageAction":
                return ramda_2.pipe(ramda_1.assoc("isLoading", true), ramda_1.assoc("pageIndex", action.pageIndex))(state);
            case "GoToFirstLeaderboardPage":
                return ramda_2.pipe(ramda_1.assoc("isLoading", true), ramda_1.assoc("pageIndex", 0))(state);
            case "GoToLastLeaderboardPage":
                return ramda_2.pipe(ramda_1.assoc("isLoading", true), ramda_1.assoc("pageIndex", state.totalPageCount - 1))(state);
            case "UserSelectsSite":
                return exports.userSelectsSiteLeaderboard(state, action);
            default:
                return state;
        }
    };
}
exports.MakeLeaderboardReducer = MakeLeaderboardReducer;
const apiGetLeaderboardSuccess = (state, action) => {
    const totalPageCount = Math.ceil((action.leaderboard.totalCount || 1) / state.pageSize);
    return ramda_2.pipe(ramda_1.assoc("totalPageCount", totalPageCount), ramda_1.assoc("members", action.leaderboard.members), ramda_1.assoc("isLoading", false))(state);
};
const goToNextLeaderboardPage = (state, action) => {
    return ramda_2.pipe(ramda_1.assoc("isLoading", true), ramda_1.assoc("pageIndex", (state.pageIndex === state.totalPageCount - 1 ?
        state.totalPageCount - 1 :
        state.pageIndex + 1)))(state);
};
const goToPreviousLeaderboardPage = (state, action) => {
    return ramda_2.pipe(ramda_1.assoc("isLoading", true), ramda_1.assoc("pageIndex", (state.pageIndex === 0 ?
        state.pageIndex :
        state.pageIndex - 1)))(state);
};
exports.userSelectsSiteLeaderboard = (state, action) => {
    const defaultState = new LeaderboardState_1.LeaderboardState();
    return ramda_2.pipe(ramda_1.assoc("isLoading", true), ramda_1.assoc("selectedLeaderboard", defaultState.selectedLeaderboard), ramda_1.assoc("pageIndex", defaultState.pageIndex), ramda_1.assoc("members", defaultState.members))(state);
};

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class InlineProfileTimelineState {
    constructor() {
        this.selectedFilter = "AllActivity";
        this.timeline = [];
        this.isLoading = true;
        this.offset = 0;
        this.showMoreButtonIsVisble = true;
    }
}
exports.InlineProfileTimelineState = InlineProfileTimelineState;

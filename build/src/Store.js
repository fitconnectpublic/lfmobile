"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const redux_1 = require("redux");
const redux_observable_1 = require("redux-observable");
const redux_devtools_extension_1 = require("redux-devtools-extension");
const RootReducer_1 = require("./RootReducer");
const RootEpic_1 = require("./RootEpic");
function makeStore() {
    return redux_1.createStore(RootReducer_1.MakeRootReducer(), redux_devtools_extension_1.composeWithDevTools(redux_1.applyMiddleware(redux_observable_1.createEpicMiddleware(RootEpic_1.RootEpic))));
}
exports.makeStore = makeStore;

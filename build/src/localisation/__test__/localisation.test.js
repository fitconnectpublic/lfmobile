"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const sinon_1 = require("sinon");
const localisation_1 = require("./../localisation");
;
test("localisation gets initialised", () => {
    const dispatchSpy = sinon_1.default.spy();
    const store = { dispatch: dispatchSpy };
    localisation_1.initializeLocalisation(store);
    expect(dispatchSpy.firstCall.args[0].type).toBe("@@localize/INITIALIZE");
    expect(dispatchSpy.secondCall.args[0].type).toBe("@@localize/ADD_TRANSLATION_FOR_LANGUGE");
});

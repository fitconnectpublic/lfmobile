"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const React = require("react");
const react_redux_1 = require("react-redux");
const react_localize_redux_1 = require("react-localize-redux");
const react_native_1 = require("react-native");
const HomeScreenContainerStyles_1 = require("./HomeScreenContainerStyles");
// Components
const CircularProgressGradient_1 = require("../components/CircularProgressGradient");
const UserTimelineContainer_1 = require("../../src/containers/UserTimelineContainer");
const ChartIdPicker_1 = require("./../components/ChartIdPicker");
const ChartGranularityPicker_1 = require("./../components/ChartGranularityPicker");
const react_native_svg_charts_1 = require("react-native-svg-charts");
const Actions_1 = require("./../../../LfWeb/src/charts/Actions");
const ViewDashboardScreenAction_1 = require("./../actions/ViewDashboardScreenAction");
const mapStateToProps = (state) => {
    return {
        translate: react_localize_redux_1.getTranslate(state.locale) /* tslint:enable */,
        selectedChart: state.charts.selectedChart,
        selectedChartGranularity: state.charts.selectedGranularity,
        goalName: state.profile.memberSummary.goal.name,
        goalProgress: state.profile.memberSummary.goal.progress,
        chart: state.charts.barChart || {}
    };
};
const mapDispatchToProps = (dispatch) => {
    return {
        selectChart: (chartId) => dispatch(Actions_1.userSelectsChart(chartId)),
        selectChartGranularity: (granularity) => dispatch(Actions_1.userSelectsChartGranularity(granularity)),
        viewContainer: () => dispatch(ViewDashboardScreenAction_1.viewDashboardScreenAction())
    };
};
class HomeScreen extends React.Component {
    componentDidMount() {
        this.props.viewContainer();
    }
    render() {
        return (React.createElement(react_native_1.View, { style: HomeScreenContainerStyles_1.default.root },
            React.createElement(react_native_1.Text, { style: HomeScreenContainerStyles_1.default.goalName }, this.props.goalName.toUpperCase()),
            React.createElement(CircularProgressGradient_1.CircularProgressGradient, { percent: this.props.goalProgress, startColor: "#0a7abf", endColor: "#00a9cb" }),
            React.createElement(react_native_1.Text, { style: HomeScreenContainerStyles_1.default.sectionTitle }, this.props.translate("InsightsWidget.ProfileChart.Workouts").toString().toUpperCase()),
            React.createElement(react_native_1.View, { style: HomeScreenContainerStyles_1.default.charts },
                React.createElement(react_native_1.View, { style: HomeScreenContainerStyles_1.default.chartsPickers },
                    React.createElement(ChartIdPicker_1.default, { selectedValue: this.props.selectedChart, onValueChange: this.props.selectChart }),
                    React.createElement(ChartGranularityPicker_1.default, { selectedValue: this.props.selectedChartGranularity, onValueChange: this.props.selectChartGranularity })),
                React.createElement(react_native_1.View, null,
                    React.createElement(react_native_svg_charts_1.BarChart, { style: { height: 200 }, data: [
                            {
                                values: this.props.chart.values,
                                positive: {
                                    fill: "#0a7abf",
                                },
                            },
                        ] }),
                    React.createElement(react_native_svg_charts_1.XAxis, { style: { paddingVertical: 16 }, values: this.props.chart.labels, formatLabel: (value) => value, chartType: react_native_svg_charts_1.XAxis.Type.BAR, labelStyle: { color: 'grey' } }))),
            React.createElement(react_native_1.View, { style: HomeScreenContainerStyles_1.default.timeline },
                React.createElement(UserTimelineContainer_1.default, null))));
    }
}
exports.HomeScreen = HomeScreen;
exports.default = react_redux_1.connect(mapStateToProps, mapDispatchToProps)(HomeScreen);

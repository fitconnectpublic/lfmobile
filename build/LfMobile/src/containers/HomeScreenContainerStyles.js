"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const react_native_1 = require("react-native");
exports.HomeScreenContainerStyles = react_native_1.StyleSheet.create({
    root: {
        flex: 1,
        alignSelf: "stretch",
        alignItems: "center",
        paddingTop: 20,
        backgroundColor: "#fff",
    },
    goalName: {
        fontWeight: "bold",
        fontSize: 20,
        color: "#0a7abf",
    },
    sectionTitle: {
        flex: 1,
        alignSelf: "stretch",
        fontSize: 20,
        textAlign: "left",
        paddingTop: 20,
        paddingBottom: 20,
        paddingLeft: 10,
        paddingRight: 10,
        backgroundColor: "#e9e9e9",
    },
    charts: {
        flex: 1,
        alignSelf: "stretch",
        paddingLeft: 10,
        paddingRight: 10,
    },
    chartsPickers: {
        alignSelf: "stretch",
        display: "flex",
        flexWrap: "nowrap",
        flexDirection: "row"
    },
    chartsPicker: {
        flex: 1,
    },
    timeline: {
        backgroundColor: "#e9e9e9",
    },
});
exports.default = exports.HomeScreenContainerStyles;

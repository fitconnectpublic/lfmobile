"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const React = require("react");
const react_redux_1 = require("react-redux");
const react_localize_redux_1 = require("react-localize-redux");
const react_native_1 = require("react-native");
const ConnectAppsContainerStyles_1 = require("./ConnectAppsContainerStyles");
const AppRow_1 = require("../components/AppRow");
const mapStateToProps = (state) => {
    return {
        translate: react_localize_redux_1.getTranslate(state.locale) /* tslint:enable */,
    };
};
const mapDispatchToProps = (dispatch) => {
    return {
        test: () => null,
    };
};
class ConnectApps extends React.Component {
    componentDidCatch(error, info) {
        console.error(error, info);
    }
    render() {
        return (React.createElement(react_native_1.View, { style: ConnectAppsContainerStyles_1.ConnectAppsContainerStyles.container }, this.getApps().map((app) => {
            return (React.createElement(AppRow_1.AppRow, { imgSource: app.icon, title: app.name, recommended: false, switched: true, onPress: () => null }));
        })));
    }
    getApps() {
        return [
            {
                name: this.props.translate("LfMobile.ConnectAppsContainer.Fitbit"),
                icon: require("./../../images/apps/fitbit-full.jpg"),
            },
        ];
    }
}
exports.ConnectApps = ConnectApps;
exports.default = react_redux_1.connect(mapStateToProps, mapDispatchToProps)(ConnectApps);

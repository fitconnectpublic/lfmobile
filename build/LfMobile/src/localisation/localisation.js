"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const react_localize_redux_1 = require("react-localize-redux");
const en_1 = require("./en");
exports.supportedLanguages = [
    "en_GB",
    "en_US",
];
function initializeLocalisation(store) {
    store.dispatch(react_localize_redux_1.initialize(exports.supportedLanguages, { defaultLanguage: "en_US" }));
    store.dispatch(react_localize_redux_1.addTranslationForLanguage(en_1.default, "en_GB"));
    store.dispatch(react_localize_redux_1.addTranslationForLanguage(en_1.default, "en_US"));
}
exports.initializeLocalisation = initializeLocalisation;

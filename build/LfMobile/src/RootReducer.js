"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const redux_1 = require("redux");
const react_localize_redux_1 = require("react-localize-redux");
const InlineProfileTimelineReducer_1 = require("../../LfWeb/src/timeline/InlineProfileTimelineReducer");
const InlineLogDetailsReducer_1 = require("../../LfWeb/src/timeline/InlineLogDetailsReducer");
const UserReducer_1 = require("./user/UserReducer");
const InlineProfileReducer_1 = require("./../../LfWeb/src/profile/InlineProfileReducer");
const InlineProfileChartReducer_1 = require("./../../LfWeb/src/charts/InlineProfileChartReducer");
function MakeRootReducer() {
    return redux_1.combineReducers({
        locale: react_localize_redux_1.localeReducer,
        Timeline: InlineProfileTimelineReducer_1.MakeInlineProfileTimelineReducer(),
        InlineLogDetails: InlineLogDetailsReducer_1.MakeInlineLogDetailsReducer(),
        user: UserReducer_1.UserReducer(),
        profile: InlineProfileReducer_1.MakeInlineProfileReducer(),
        charts: InlineProfileChartReducer_1.MakeInlineProfileChartReducer(),
    });
}
exports.MakeRootReducer = MakeRootReducer;

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const R = require("ramda");
const UserState_1 = require("./UserState");
function UserReducer() {
    return (state = new UserState_1.UserState(), action) => {
        switch (action.type) {
            case "Authenticate":
                console.log("AUTH USER");
                let st = R.pipe(R.assoc("secret", action.secret), R.assoc("token", action.token))(state);
                console.log(st);
                return state;
            case "AuthenticationUserNotFound":
                console.log("AUTH USER NOT FOUND");
                return state;
            case "AuthenticationError":
                console.log("AUTH ERROR");
                return state;
            case "AuthenticationSuccess":
                console.log("AUTH SUCCESS");
                return R.assoc("accessToken", action.token, state);
            default:
                return state;
        }
    };
}
exports.UserReducer = UserReducer;

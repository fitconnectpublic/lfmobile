"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Observable_1 = require("rxjs/Observable");
require("rxjs/add/observable/fromPromise");
const R = require("ramda");
exports.ObservableFetch = (input, init) => {
    return Observable_1.Observable.fromPromise(fetch(input, R.mergeDeepLeft({
        headers: {
            Accept: "application/json",
        },
    }, init || {}))
        .then((response) => {
        return response
            .json()
            .then((body) => {
            return {
                status: response.status,
                statusText: response.statusText,
                body,
            };
        });
    })
        .catch((error) => {
        return {
            status: 400,
            statusText: error,
            body: {},
        };
    }));
};

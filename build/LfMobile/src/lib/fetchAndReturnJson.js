"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Observable_1 = require("rxjs/Observable");
require("rxjs/add/observable/fromPromise");
const ramda_1 = require("ramda");
exports.fetchAndReturnJson = (input, init) => {
    return Observable_1.Observable.fromPromise(fetch(input, ramda_1.mergeDeepLeft({
        headers: {
            Accept: "application/json",
        },
    }, init || {}))
        .then((response) => {
        return response
            .json()
            .then((body) => {
            return {
                status: response.status,
                statusText: response.statusText,
                body,
            };
        });
    })
        .catch((error) => {
        return {
            status: 400,
            statusText: error.message,
            body: {},
        };
    }));
};

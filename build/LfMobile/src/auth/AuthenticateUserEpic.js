"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Observable_1 = require("rxjs/Observable");
require("rxjs/add/operator/switchMap");
require("rxjs/add/operator/filter");
require("rxjs/add/operator/map");
require("rxjs/add/operator/catch");
require("rxjs/add/observable/empty");
// Misc
const config_1 = require("./../../../src/config");
const ObservableFetch_1 = require("./../lib/ObservableFetch");
// Actions
const Actions_1 = require("./../auth/Actions");
exports.AuthenticateUserEpic = (action$, stateMiddleware) => {
    return action$
        .filter((action) => {
        return action.type === "Authenticate";
    })
        .switchMap(() => {
        const state = stateMiddleware.getState();
        const formData = new FormData();
        formData.append("access_token", config_1.config.token);
        formData.append("access_secret", config_1.config.secret);
        return ObservableFetch_1.ObservableFetch(`${config_1.config.baseUrl}oauth/life-fitness-mobile`, {
            method: "POST",
            body: formData,
        })
            .catch(() => Observable_1.Observable.empty());
    })
        .map((response) => {
        const authResponse = response.body;
        const errorResponse = response.body;
        // console.log(response);
        if (response.status === 200) {
            return Actions_1.AuthenticationSuccessAction(authResponse.accessToken);
        }
        if (response.status === 500 && errorResponse.error.code === "UserNotFoundException") {
            return Actions_1.AuthenticationUserNotFoundAction(errorResponse);
        }
        return Actions_1.AuthenticationErrorAction(errorResponse);
    });
};

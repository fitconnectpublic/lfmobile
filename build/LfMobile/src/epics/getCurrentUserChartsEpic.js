"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Observable_1 = require("rxjs/Observable");
require("rxjs/add/operator/switchMap");
require("rxjs/add/operator/filter");
require("rxjs/add/operator/map");
require("rxjs/add/operator/catch");
require("rxjs/add/observable/empty");
// Misc
const fetchFitconnectAndReturnJson_1 = require("..//lib/fetchFitconnectAndReturnJson");
const mergeDefaultAndResponse_1 = require("..//lib/mergeDefaultAndResponse");
// Actions
const Actions_1 = require("./../../../LfWeb/src/charts/Actions");
const IBarChartResponse_1 = require("../../../src/responses/IBarChartResponse");
const IBarChartResponse_IBarChart_1 = require("../../../LfWeb/src/maps/IBarChartResponse_IBarChart");
const TChartId_TChartTypeRequest_1 = require("../../../LfWeb/src/maps/TChartId_TChartTypeRequest");
exports.getCurrentUserChartsEpic = (action$, stateMiddleware) => {
    let state = stateMiddleware.getState();
    return action$
        .filter((action) => {
        return action.type === "UserSelectsChart"
            || action.type === "UserSelectsChartGranularity"
            || action.type === "UserViewsFullProfile"
            || action.type === "UserViewsDashboardScreen";
    })
        .switchMap((action) => {
        state = stateMiddleware.getState();
        return fetchFitconnectAndReturnJson_1.fetchFitconnectAndReturnJson("Bearer " + state.user.accessToken)(`/v3/users/me/chart-data/chart/${TChartId_TChartTypeRequest_1.TChartId_TChartTypeRequest(state.charts.selectedChart)}/period/${state.charts.selectedGranularity}`)
            .map((response) => {
            return IBarChartResponse_IBarChart_1.IBarChartResponse_IBarChart(mergeDefaultAndResponse_1.mergeDefaultAndResponse(new IBarChartResponse_1.IBarChartResponse(), response.body), state.charts.selectedGranularity);
        })
            .catch(() => Observable_1.Observable.empty());
    })
        .map(Actions_1.apiGetChartSuccess);
};

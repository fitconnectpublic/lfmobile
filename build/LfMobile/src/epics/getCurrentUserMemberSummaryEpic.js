"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Observable_1 = require("rxjs/Observable");
require("rxjs/add/operator/switchMap");
require("rxjs/add/operator/filter");
require("rxjs/add/operator/map");
require("rxjs/add/operator/catch");
require("rxjs/add/observable/empty");
// Misc
const fetchFitconnectAndReturnJson_1 = require("./../lib/fetchFitconnectAndReturnJson");
const Actions_1 = require("./../../../LfWeb/src/profile/Actions");
const IUserGoalResponse_IMemberSummary_1 = require("./../../../LfWeb/src/maps/IUserGoalResponse_IMemberSummary");
exports.getCurrentUserMemberSummaryEpic = (action$, stateMiddleware) => {
    return action$
        .filter((action) => {
        return (action.type === "UserViewsInlineProfile"
            || action.type === "AuthenticationSuccess"
            || action.type === "UserViewsDashboardScreen");
    })
        .switchMap((action) => {
        const state = stateMiddleware.getState();
        return fetchFitconnectAndReturnJson_1.fetchFitconnectAndReturnJson("Bearer " + state.user.accessToken)(`/v3/users/me/goals`)
            .map((response) => {
            console.log("user goals", response.body.data);
            let data = response.body.data || [];
            return IUserGoalResponse_IMemberSummary_1.IUserGoalResponse_IMemberSummary(data[0]);
        })
            .catch(() => Observable_1.Observable.empty());
    })
        .map(Actions_1.apiGetUserGoalSuccess);
};

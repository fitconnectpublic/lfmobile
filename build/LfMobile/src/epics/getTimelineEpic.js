"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Observable_1 = require("rxjs/Observable");
require("rxjs/add/operator/switchMap");
require("rxjs/add/operator/filter");
require("rxjs/add/operator/map");
require("rxjs/add/operator/catch");
require("rxjs/add/observable/empty");
const react_localize_redux_1 = require("react-localize-redux");
// Misc
const fetchFitconnectAndReturnJson_1 = require("./../lib/fetchFitconnectAndReturnJson");
const TTimelineResponse_TTimeline_1 = require("./../../../LfWeb/src/maps/TTimelineResponse_TTimeline");
const Actions_1 = require("./../../../LfWeb/src/timeline/Actions");
exports.getTimelineEpic = (action$, stateMiddleware) => {
    let state = stateMiddleware.getState();
    return action$
        .filter((action) => {
        return action.type === "UserSelectsTimelineFilter"
            || action.type === "UserViewsFullProfile"
            || action.type === "UserViewsMoreTimeline"
            || action.type === "AuthenticationSuccess"
            || action.type === "UserViewsDashboardScreen";
    })
        .switchMap((action) => {
        state = stateMiddleware.getState();
        return fetchFitconnectAndReturnJson_1.fetchFitconnectAndReturnJson("Bearer " + state.user.accessToken)(`v3/users/me/timeline?$top=25&$skip=${state.Timeline.offset * 25}`)
            .map((response) => TTimelineResponse_TTimeline_1.TTimelineResponse_TTimeline(response.body, react_localize_redux_1.getTranslate(state.locale)))
            .catch(() => Observable_1.Observable.empty());
    })
        .map((timeline) => Actions_1.apiGetTimelineSuccess(timeline));
};

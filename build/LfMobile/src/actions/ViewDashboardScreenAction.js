"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.viewDashboardScreenAction = () => {
    return {
        type: "UserViewsDashboardScreen",
    };
};

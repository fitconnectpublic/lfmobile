"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const redux_observable_1 = require("redux-observable");
const AuthenticateUserEpic_1 = require("./auth/AuthenticateUserEpic");
const getTimelineEpic_1 = require("./epics/getTimelineEpic");
const setLanguageEpic_1 = require("./epics/setLanguageEpic");
const getCurrentUserMemberSummaryEpic_1 = require("./epics/getCurrentUserMemberSummaryEpic");
const getCurrentUserChartsEpic_1 = require("./epics/getCurrentUserChartsEpic");
exports.RootEpic = redux_observable_1.combineEpics(AuthenticateUserEpic_1.AuthenticateUserEpic, getTimelineEpic_1.getTimelineEpic, setLanguageEpic_1.setLanguageEpic, getCurrentUserMemberSummaryEpic_1.getCurrentUserMemberSummaryEpic, getCurrentUserChartsEpic_1.getCurrentUserChartsEpic);

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const React = require("react");
const react_1 = require("react");
const react_native_1 = require("react-native");
const VariablesColors_1 = require("./../VariablesColors");
const TimelineRowStyles_1 = require("./TimelineRowStyles");
class TimelineRow extends react_1.Component {
    constructor() {
        super(...arguments);
        this.state = {
            rotateAnim: new react_native_1.Animated.Value(0),
        };
    }
    render() {
        let spin = this.props.opened ? "180deg" : "0deg";
        // First set up animation
        react_native_1.Animated.timing(this.state.rotateAnim, {
            toValue: this.props.opened ? 1 : 0,
            duration: 200,
            easing: react_native_1.Easing.linear,
        }).start();
        // Second interpolate beginning and end values (in this case 0 and 1)
        spin = this.state.rotateAnim.interpolate({
            inputRange: [0, 1],
            outputRange: ["0deg", "180deg"],
        });
        const imageContainerStyle = [TimelineRowStyles_1.TimelineRowStyles.imageContainer];
        let icon;
        switch (this.props.type.toLowerCase()) {
            case "fitness":
                imageContainerStyle.push({ backgroundColor: VariablesColors_1.colors.blue });
                icon = require("./../../images/fitness.png");
                break;
            case "message":
                imageContainerStyle.push({ backgroundColor: VariablesColors_1.colors.timelineMessageGrey });
                icon = require("./../../images/send.png");
                break;
            case "goals":
                imageContainerStyle.push({ backgroundColor: VariablesColors_1.colors.orange });
                icon = require("./../../images/goal.png");
                break;
            case "nutrition":
                imageContainerStyle.push({ backgroundColor: VariablesColors_1.colors.green });
                icon = require("./../../images/nutrition.png");
                break;
            case "weight":
                imageContainerStyle.push({ backgroundColor: VariablesColors_1.colors.yellow });
                icon = require("./../../images/weight.png");
                break;
            case "sleep":
                imageContainerStyle.push({ backgroundColor: VariablesColors_1.colors.darkBlue });
                icon = require("./../../images/sleep.png");
                break;
            case "routine":
                imageContainerStyle.push({ backgroundColor: VariablesColors_1.colors.lightBlue });
                icon = require("./../../images/routine.png");
                break;
            default:
                icon = require("./../../images/routine.png");
                break;
        }
        return (React.createElement(react_native_1.TouchableHighlight, { style: TimelineRowStyles_1.TimelineRowStyles.background, onPress: this.props.onPress },
            React.createElement(react_native_1.View, { style: TimelineRowStyles_1.TimelineRowStyles.container },
                React.createElement(react_native_1.View, { style: imageContainerStyle },
                    React.createElement(react_native_1.Image, { style: TimelineRowStyles_1.TimelineRowStyles.image, source: icon })),
                React.createElement(react_native_1.View, { style: TimelineRowStyles_1.TimelineRowStyles.innerContainer },
                    React.createElement(react_native_1.View, { style: TimelineRowStyles_1.TimelineRowStyles.textContainer },
                        React.createElement(react_native_1.Text, null, this.props.children),
                        React.createElement(react_native_1.Text, { style: TimelineRowStyles_1.TimelineRowStyles.time }, this.props.time)),
                    React.createElement(react_native_1.View, { style: TimelineRowStyles_1.TimelineRowStyles.appIconContainer },
                        this.props.appIcons.map((appIcon, index) => {
                            return React.createElement(react_native_1.Image, { key: index, style: TimelineRowStyles_1.TimelineRowStyles.appIcon, source: appIcon });
                        }),
                        this.props.type.toLowerCase() === "message" ? null :
                            React.createElement(react_native_1.Animated.Image, { style: [TimelineRowStyles_1.TimelineRowStyles.chevron, { transform: [{ rotate: spin }] }], source: require("./../../images/chevron-down.png") }))))));
    }
}
exports.TimelineRow = TimelineRow;

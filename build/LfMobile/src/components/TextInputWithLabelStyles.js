"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const react_native_1 = require("react-native");
const VariablesColors_1 = require("./../VariablesColors");
exports.TextInputWithLabelStyles = react_native_1.StyleSheet.create({
    container: {
        flexDirection: "row",
        justifyContent: "flex-start",
        alignItems: "center",
        alignSelf: "stretch",
        backgroundColor: VariablesColors_1.colors.grayscale0,
        paddingLeft: 25,
        paddingRight: 25,
        paddingTop: 6,
        paddingBottom: 6,
        borderTopWidth: 1,
        borderTopColor: VariablesColors_1.colors.grayscale3,
        borderBottomWidth: 1,
        borderBottomColor: VariablesColors_1.colors.grayscale3,
    },
    label: {
        fontSize: 16,
        color: VariablesColors_1.colors.grayscale5,
    },
    textInput: {
        alignSelf: "stretch",
        flex: 1,
        textAlign: "right",
    },
});

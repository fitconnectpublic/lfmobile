"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const React = require("react");
const react_1 = require("react");
const react_native_1 = require("react-native");
const ColouredBoxStyles_1 = require("./ColouredBoxStyles");
const VariablesColors_1 = require("./../VariablesColors");
class ColouredBox extends react_1.Component {
    render() {
        return (React.createElement(react_native_1.View, { style: react_native_1.StyleSheet.flatten([
                ColouredBoxStyles_1.ColouredBoxStyles.box,
                { backgroundColor: VariablesColors_1.colors[this.props.backgroundColor] },
            ]) }, this.props.children));
    }
}
exports.ColouredBox = ColouredBox;

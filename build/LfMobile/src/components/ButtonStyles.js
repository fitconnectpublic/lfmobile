"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const react_native_1 = require("react-native");
const VariablesColors_1 = require("./../VariablesColors");
exports.ButtonStyles = react_native_1.StyleSheet.create({
    button: {
        flexDirection: "row",
        justifyContent: "center",
        alignItems: "center",
        backgroundColor: "#DDDDDD",
        padding: 15,
        margin: 10,
    },
    buttonFullWidth: {
        alignSelf: "stretch",
    },
    buttonPrimary: {
        backgroundColor: VariablesColors_1.colors.blue,
    },
    buttonTransparent: {
        backgroundColor: "transparent",
    },
    buttonDisabledPrimary: {
        backgroundColor: VariablesColors_1.colors.grayscale4,
    },
    buttonDisabledTransparent: {
        // color: "#fff",
        opacity: 0.35,
    },
    text: {
        color: VariablesColors_1.colors.grayscale0,
    },
    textTransparent: {
        color: VariablesColors_1.colors.blue,
    },
});

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const React = require("react");
const react_1 = require("react");
const react_native_1 = require("react-native");
const NavHeaderStyles_1 = require("./NavHeaderStyles");
const Button_1 = require("../components/Button");
class NavHeader extends react_1.Component {
    render() {
        return (React.createElement(react_native_1.View, { style: NavHeaderStyles_1.NavHeaderStyles.container },
            React.createElement(react_native_1.Image, { style: NavHeaderStyles_1.NavHeaderStyles.chevron, source: require("./../../images/chevron-left.png") }),
            React.createElement(react_native_1.View, { style: NavHeaderStyles_1.NavHeaderStyles.button },
                React.createElement(Button_1.Button, { color: "transparent", title: this.props.buttonText, onPress: this.props.onButtonPress, disabled: this.props.buttonDisabled })),
            React.createElement(react_native_1.Text, { style: NavHeaderStyles_1.NavHeaderStyles.heading }, this.props.title),
            React.createElement(react_native_1.View, { style: NavHeaderStyles_1.NavHeaderStyles.button },
                React.createElement(Button_1.Button, { color: "transparent", title: this.props.buttonText, onPress: this.props.onButtonPress, disabled: this.props.buttonDisabled }))));
    }
}
exports.NavHeader = NavHeader;

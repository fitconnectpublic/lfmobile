"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const React = require("react");
const react_1 = require("react");
const react_native_1 = require("react-native");
const AppRowStyles_1 = require("./AppRowStyles");
class AppRow extends react_1.Component {
    render() {
        return (React.createElement(react_native_1.View, { style: AppRowStyles_1.AppRowStyles.container },
            React.createElement(react_native_1.Image, { style: AppRowStyles_1.AppRowStyles.image, source: this.props.imgSource }),
            React.createElement(react_native_1.View, { style: AppRowStyles_1.AppRowStyles.innerContainer },
                React.createElement(react_native_1.View, null,
                    React.createElement(react_native_1.Text, { style: AppRowStyles_1.AppRowStyles.title }, this.props.title),
                    !this.props.recommended ? null :
                        React.createElement(react_native_1.Text, { style: AppRowStyles_1.AppRowStyles.recommended }, "Recommended")),
                React.createElement(react_native_1.Switch, { value: this.props.switched, onValueChange: this.props.onPress }))));
    }
}
exports.AppRow = AppRow;

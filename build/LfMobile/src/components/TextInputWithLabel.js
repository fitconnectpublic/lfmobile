"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const React = require("react");
const react_1 = require("react");
const react_native_1 = require("react-native");
const VariablesColors_1 = require("./../VariablesColors");
const TextInputWithLabelStyles_1 = require("./TextInputWithLabelStyles");
class TextInputWithLabel extends react_1.Component {
    render() {
        return (React.createElement(react_native_1.View, { style: TextInputWithLabelStyles_1.TextInputWithLabelStyles.container },
            React.createElement(react_native_1.Text, { style: TextInputWithLabelStyles_1.TextInputWithLabelStyles.label }, this.props.label),
            React.createElement(react_native_1.TextInput, { style: TextInputWithLabelStyles_1.TextInputWithLabelStyles.textInput, placeholder: this.props.placeholder, placeholderTextColor: VariablesColors_1.colors.grayscale4, underlineColorAndroid: "transparent", keyboardType: this.props.keyboardType || "default", secureTextEntry: this.props.secureTextEntry || false, autoCapitalize: this.props.autoCapitalize || "words" })));
    }
}
exports.TextInputWithLabel = TextInputWithLabel;

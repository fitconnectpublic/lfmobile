"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const react_native_1 = require("react-native");
const VariablesColors_1 = require("./../VariablesColors");
exports.AppRowStyles = react_native_1.StyleSheet.create({
    container: {
        flexDirection: "row",
        borderBottomWidth: 0.5,
        borderBottomColor: VariablesColors_1.colors.grayscale4,
    },
    image: {
        height: 75,
        width: 75,
    },
    innerContainer: {
        flex: 5,
        flexDirection: "row",
        justifyContent: "space-between",
        alignItems: "center",
        padding: 10,
        backgroundColor: VariablesColors_1.colors.grayscale0,
    },
    title: {
        fontWeight: "bold",
    },
    recommended: {
        color: VariablesColors_1.colors.grayscale0,
        backgroundColor: VariablesColors_1.colors.yellow,
        borderRadius: 15,
        paddingTop: 1,
        paddingBottom: 1,
        paddingLeft: 5,
        paddingRight: 5,
        fontSize: 10,
    },
});

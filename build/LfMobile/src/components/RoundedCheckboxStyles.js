"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const react_native_1 = require("react-native");
const VariablesColors_1 = require("./../VariablesColors");
exports.RoundedCheckboxStyles = react_native_1.StyleSheet.create({
    container: {
        backgroundColor: VariablesColors_1.colors.grayscale1,
        borderColor: VariablesColors_1.colors.grayscale4,
        borderWidth: 1,
        width: 25,
        height: 25,
        borderRadius: 50,
        justifyContent: "center",
        alignItems: "center",
    },
    containerChecked: {
        backgroundColor: VariablesColors_1.colors.blue,
        borderColor: VariablesColors_1.colors.blue,
    },
    check: {
        height: 20,
        width: 20,
        resizeMode: "contain",
        tintColor: VariablesColors_1.colors.grayscale4,
    },
    checkChecked: {
        tintColor: VariablesColors_1.colors.grayscale0,
    },
});

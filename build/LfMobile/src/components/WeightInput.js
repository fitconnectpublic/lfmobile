"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const React = require("react");
const react_redux_1 = require("react-redux");
const react_localize_redux_1 = require("react-localize-redux");
const react_1 = require("react");
const react_native_1 = require("react-native");
const merge = require("ramda/src/merge");
const VariablesColors_1 = require("../VariablesColors");
const WeightInputStyles_1 = require("./WeightInputStyles");
const mapStateToProps = (state, props) => {
    return merge({
        translate: react_localize_redux_1.getTranslate(state.locale) /* tslint:enable */,
    }, props);
};
class WeightInput extends react_1.Component {
    render() {
        return (React.createElement(react_native_1.View, { style: WeightInputStyles_1.WeightInputStyles.container },
            React.createElement(react_native_1.Text, { style: WeightInputStyles_1.WeightInputStyles.label }, this.props.title),
            React.createElement(react_native_1.TextInput, { style: WeightInputStyles_1.WeightInputStyles.textInput, placeholder: this.props.translate("LfMobile.WeightInput.Enter"), placeholderTextColor: VariablesColors_1.colors.grayscale4, underlineColorAndroid: "transparent", keyboardType: "numeric", secureTextEntry: false, autoCapitalize: "characters", value: this.props.value }),
            React.createElement(react_native_1.Text, { style: WeightInputStyles_1.WeightInputStyles.unit },
                this.props.unitSelected === "lbs" && this.props.translate("LfMobile.WeightInput.lbs"),
                this.props.unitSelected === "kg" && this.props.translate("LfMobile.WeightInput.kg"))));
    }
}
exports.default = react_redux_1.connect(mapStateToProps)(WeightInput);

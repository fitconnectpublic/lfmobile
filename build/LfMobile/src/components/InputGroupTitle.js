"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const React = require("react");
const react_1 = require("react");
const react_native_1 = require("react-native");
const InputGroupTitleStyles_1 = require("./InputGroupTitleStyles");
class InputGroupTitle extends react_1.Component {
    render() {
        return (React.createElement(react_native_1.View, { style: InputGroupTitleStyles_1.InputGroupTitleStyles.container },
            React.createElement(react_native_1.Text, { style: InputGroupTitleStyles_1.InputGroupTitleStyles.title }, this.props.title)));
    }
}
exports.InputGroupTitle = InputGroupTitle;

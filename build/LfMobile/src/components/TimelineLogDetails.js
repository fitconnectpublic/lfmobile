"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const React = require("react");
const react_native_1 = require("react-native");
const react_redux_1 = require("react-redux");
const react_localize_redux_1 = require("react-localize-redux");
const ramda_1 = require("ramda");
const round_1 = require("../lib/round");
const TimelineLogStat_1 = require("../components/TimelineLogStat");
const TimelineLogDetailsStyles_1 = require("./TimelineLogDetailsStyles");
const ColouredBox_1 = require("./ColouredBox");
const { UIManager } = react_native_1.NativeModules;
/* tslint:disable */
UIManager.setLayoutAnimationEnabledExperimental &&
    UIManager.setLayoutAnimationEnabledExperimental(true);
exports.mapStateToProps = (state) => {
    return {
        translate: react_localize_redux_1.getTranslate(state.locale) /* tslint:enable */,
    };
};
// export const mapDispatchToProps = (dispatch: Function) => {
//     return {};
// };
class TimelineLogDetailsContainer extends React.Component {
    componentDidCatch(error, info) {
        console.error(error, info);
    }
    render() {
        react_native_1.LayoutAnimation.easeInEaseOut();
        if (this.props.open) {
            return (React.createElement(react_native_1.View, null, (() => {
                switch (this.props.payload.logType) {
                    case "GROUP":
                        switch (this.props.payload.logSubType) {
                            case "NUTRITION":
                                const nutritionLogs = this.props.payload.children;
                                return (React.createElement(ColouredBox_1.ColouredBox, { backgroundColor: "grayscale1" },
                                    React.createElement(react_native_1.Text, { style: [TimelineLogDetailsStyles_1.TimelineLogDetailsStyles.header] }, "Nutrition Breakdown"),
                                    React.createElement(TimelineLogStat_1.TimelineLogStat, { title: "Calories", text: Math.floor(ramda_1.sum(nutritionLogs.map((v) => v["attributes"]["data"]["calories"]))) + " cal" }),
                                    React.createElement(TimelineLogStat_1.TimelineLogStat, { title: "Carbohydrates", text: Math.floor(ramda_1.sum(nutritionLogs.map((v) => v["attributes"]["data"]["carbohydrates"]))) + "g" }),
                                    React.createElement(TimelineLogStat_1.TimelineLogStat, { title: "Fat", text: Math.floor(ramda_1.sum(nutritionLogs.map((v) => v["attributes"]["data"]["fat"]))) + "g" }),
                                    React.createElement(TimelineLogStat_1.TimelineLogStat, { title: "Protein", text: Math.floor(ramda_1.sum(nutritionLogs.map((v) => v["attributes"]["data"]["protein"]))) + "g" }),
                                    React.createElement(TimelineLogStat_1.TimelineLogStat, { title: "Fiber", text: Math.floor(ramda_1.sum(nutritionLogs.map((v) => v["attributes"]["data"]["fiber"]))) + "g" }),
                                    React.createElement(TimelineLogStat_1.TimelineLogStat, { title: "Sugar", text: Math.floor(ramda_1.sum(nutritionLogs.map((v) => v["attributes"]["data"]["sugar"]))) + "g" }),
                                    React.createElement(TimelineLogStat_1.TimelineLogStat, { title: "Water", text: Math.floor(ramda_1.sum(nutritionLogs.map((v) => v["attributes"]["data"]["water"]))) + "ml" }),
                                    React.createElement(TimelineLogStat_1.TimelineLogStat, { title: "Sodium", text: Math.floor(ramda_1.sum(nutritionLogs.map((v) => v["attributes"]["data"]["sodium"]))) + "mg" }),
                                    React.createElement(TimelineLogStat_1.TimelineLogStat, { title: "Polyunstaturated Fat", text: Math.floor(ramda_1.sum(nutritionLogs.map((v) => v["attributes"]["data"]["polyunsaturatedFat"]))) + "g" }),
                                    React.createElement(TimelineLogStat_1.TimelineLogStat, { title: "Monounsaturated Fat", text: Math.floor(ramda_1.sum(nutritionLogs.map((v) => v["attributes"]["data"]["monounsaturatedFat"]))) + "g" }),
                                    React.createElement(TimelineLogStat_1.TimelineLogStat, { title: "Trans Fat", text: Math.floor(ramda_1.sum(nutritionLogs.map((v) => v["attributes"]["data"]["transFat"]))) + "g" }),
                                    React.createElement(TimelineLogStat_1.TimelineLogStat, { title: "Cholesterol", text: Math.floor(ramda_1.sum(nutritionLogs.map((v) => v["attributes"]["data"]["cholesterol"]))) + "mg" }),
                                    React.createElement(TimelineLogStat_1.TimelineLogStat, { title: "Vitamin A", text: Math.floor(ramda_1.sum(nutritionLogs.map((v) => v["attributes"]["data"]["vitamin_a"]))) + "%" }),
                                    React.createElement(TimelineLogStat_1.TimelineLogStat, { title: "Vitamin C", text: Math.floor(ramda_1.sum(nutritionLogs.map((v) => v["attributes"]["data"]["vitamin_c"]))) + "%" }),
                                    React.createElement(TimelineLogStat_1.TimelineLogStat, { title: "Calcium", text: Math.floor(ramda_1.sum(nutritionLogs.map((v) => v["attributes"]["data"]["calcium"]))) + "%" }),
                                    React.createElement(TimelineLogStat_1.TimelineLogStat, { title: "Iron", text: Math.floor(ramda_1.sum(nutritionLogs.map((v) => v["attributes"]["data"]["iron"]))) + "%" }),
                                    (() => {
                                        const elementsArray = [];
                                        ramda_1.forEachObjIndexed((value, key) => {
                                            elementsArray.push(React.createElement(react_native_1.View, { key: key },
                                                React.createElement(react_native_1.Text, { style: [TimelineLogDetailsStyles_1.TimelineLogDetailsStyles.header] }, value.map((v) => v["attributes"]["data"]["foodName"]).join(", ") + " - " + key),
                                                React.createElement(TimelineLogStat_1.TimelineLogStat, { title: "Calories", text: Math.floor(ramda_1.sum(value.map((v) => v["attributes"]["data"]["calories"]))) + " Cal" }),
                                                React.createElement(TimelineLogStat_1.TimelineLogStat, { title: "Carbohydrates", text: Math.floor(ramda_1.sum(value.map((v) => v["attributes"]["data"]["carbohydrates"]))) + "g" }),
                                                React.createElement(TimelineLogStat_1.TimelineLogStat, { title: "Fat", text: Math.floor(ramda_1.sum(value.map((v) => v["attributes"]["data"]["fat"]))) + "g" }),
                                                React.createElement(TimelineLogStat_1.TimelineLogStat, { title: "Protein", text: Math.floor(ramda_1.sum(value.map((v) => v["attributes"]["data"]["protein"]))) + "g" }),
                                                React.createElement(TimelineLogStat_1.TimelineLogStat, { title: "Fiber", text: Math.floor(ramda_1.sum(value.map((v) => v["attributes"]["data"]["fiber"]))) + "g" }),
                                                React.createElement(TimelineLogStat_1.TimelineLogStat, { title: "Sugar", text: Math.floor(ramda_1.sum(value.map((v) => v["attributes"]["data"]["sugar"]))) + "g" }),
                                                React.createElement(TimelineLogStat_1.TimelineLogStat, { title: "Water", text: Math.floor(ramda_1.sum(value.map((v) => v["attributes"]["data"]["water"]))) + "ml" }),
                                                React.createElement(TimelineLogStat_1.TimelineLogStat, { title: "Sodium", text: Math.floor(ramda_1.sum(value.map((v) => v["attributes"]["data"]["sodium"]))) + "mg" }),
                                                React.createElement(TimelineLogStat_1.TimelineLogStat, { title: "Water", text: Math.floor(ramda_1.sum(value.map((v) => v["attributes"]["data"]["water"]))) + "g" }),
                                                React.createElement(TimelineLogStat_1.TimelineLogStat, { title: "Polyunstaturated Fat", text: Math.floor(ramda_1.sum(value.map((v) => v["attributes"]["data"]["polyunsaturatedFat"]))) + "g" }),
                                                React.createElement(TimelineLogStat_1.TimelineLogStat, { title: "Monounsaturated Fat", text: Math.floor(ramda_1.sum(value.map((v) => v["attributes"]["data"]["monounsaturatedFat"]))) + "g" }),
                                                React.createElement(TimelineLogStat_1.TimelineLogStat, { title: "Trans Fat", text: Math.floor(ramda_1.sum(value.map((v) => v["attributes"]["data"]["transFat"]))) + "g" }),
                                                React.createElement(TimelineLogStat_1.TimelineLogStat, { title: "Cholesterol", text: Math.floor(ramda_1.sum(value.map((v) => v["attributes"]["data"]["cholesterol"]))) + "mg" }),
                                                React.createElement(TimelineLogStat_1.TimelineLogStat, { title: "Vitamin A", text: Math.floor(ramda_1.sum(value.map((v) => v["attributes"]["data"]["vitamin_a"]))) + "%" }),
                                                React.createElement(TimelineLogStat_1.TimelineLogStat, { title: "Vitamin C", text: Math.floor(ramda_1.sum(value.map((v) => v["attributes"]["data"]["vitamin_c"]))) + "%" }),
                                                React.createElement(TimelineLogStat_1.TimelineLogStat, { title: "Calcium", text: Math.floor(ramda_1.sum(value.map((v) => v["attributes"]["data"]["calcium"]))) + "%" }),
                                                React.createElement(TimelineLogStat_1.TimelineLogStat, { title: "Iron", text: Math.floor(ramda_1.sum(value.map((v) => v["attributes"]["data"]["iron"]))) + "%" })));
                                        }, ramda_1.groupBy((value) => value["attributes"]["data"]["meal"])(nutritionLogs));
                                        return elementsArray;
                                    })()));
                            case "WORKOUT":
                                const fitnessLogs = this.props.payload.children;
                                return (React.createElement(ColouredBox_1.ColouredBox, { backgroundColor: "grayscale1" },
                                    React.createElement(TimelineLogStat_1.TimelineLogStat, { title: "Exercises", text: fitnessLogs.length + " Exercises" }),
                                    ramda_1.sum(fitnessLogs.map((v) => v["attributes"]["data"]["distance"])) > 0 ?
                                        React.createElement(TimelineLogStat_1.TimelineLogStat, { title: "Distance", text: round_1.round(ramda_1.sum(fitnessLogs.map((v, index) => v["attributes"]["data"]["distance"])) / 1000, 2) + " km" })
                                        : null,
                                    ramda_1.sum(fitnessLogs.map((v) => v["attributes"]["data"]["duration"])) > 0 ?
                                        React.createElement(TimelineLogStat_1.TimelineLogStat, { title: "Duration", text: Math.floor(ramda_1.sum(fitnessLogs.map((v, index) => v["attributes"]["data"]["duration"])) / 60)
                                                + " mins " + ramda_1.sum(fitnessLogs.map((v) => v["attributes"]["data"]["duration"])) % 60 + " secs" })
                                        : null,
                                    round_1.round(ramda_1.sum(fitnessLogs.map((v) => v["attributes"]["data"]["calories"]))) > 0 ?
                                        React.createElement(TimelineLogStat_1.TimelineLogStat, { title: "Burned", text: round_1.round(ramda_1.sum(fitnessLogs.map((v) => v["attributes"]["data"]["calories"]))) + " Cal" })
                                        : null,
                                    /* tslint:disable */
                                    fitnessLogs.map((fitnessLog, index) => {
                                        React.createElement(TimelineLogStat_1.TimelineLogStat, { key: index, title: "Type", text: fitnessLog.type });
                                    })
                                /* tslint:enable */
                                ));
                            case "STRENGTH":
                                const strengthLogs = this.props.payload.children;
                                return (React.createElement(ColouredBox_1.ColouredBox, { backgroundColor: "grayscale1" },
                                    React.createElement(TimelineLogStat_1.TimelineLogStat, { title: "Exercises", text: strengthLogs.length.toString() }),
                                    ramda_1.sum(strengthLogs.map((v) => v["attributes"]["data"]["duration"])) > 0 ?
                                        React.createElement(TimelineLogStat_1.TimelineLogStat, { title: "Duration", text: Math.floor(ramda_1.sum(strengthLogs.map((v) => v["attributes"]["data"]["duration"])) / 60) + " mins " +
                                                ramda_1.sum(strengthLogs.map((v) => v["attributes"]["data"]["duration"])) % 60 + " secs" })
                                        : null,
                                    ramda_1.sum(strengthLogs.map((v) => v["attributes"]["data"]["calories"])) > 0 ?
                                        React.createElement(TimelineLogStat_1.TimelineLogStat, { title: "Burned", text: round_1.round(ramda_1.sum(strengthLogs.map((v) => v["attributes"]["data"]["calories"])))
                                                + " Cal" })
                                        : null,
                                    React.createElement(react_native_1.Text, { style: [TimelineLogDetailsStyles_1.TimelineLogDetailsStyles.header] }, "Workout Summary"),
                                    strengthLogs.map((log, index) => {
                                        /* tslint:disable */
                                        let sets = log.attributes.children;
                                        /* tslint:enable */
                                        if (!sets) {
                                            return React.createElement(TimelineLogStat_1.TimelineLogStat, { key: index, title: "Description", text: log.attributes.data.description });
                                        }
                                        return (React.createElement(react_native_1.View, null,
                                            React.createElement(TimelineLogStat_1.TimelineLogStat, { key: index, title: "Description", text: log.attributes.data.description }),
                                            sets.map((setResponse, i) => {
                                                const set = setResponse.attributes.data;
                                                if (set.repsActual > 0 && set.weightActualKg > 0) {
                                                    return (React.createElement(TimelineLogStat_1.TimelineLogStat, { key: i, title: "Set " + (i + 1), text: set.weightActualKg + "kg x " + set.repsActual + " reps" }));
                                                }
                                                else if (set.repsActual > 0) {
                                                    return (React.createElement(TimelineLogStat_1.TimelineLogStat, { key: i, title: "Set " + (i + 1), text: set.repsActual + " reps" }));
                                                }
                                                else if (set.weightActualKg > 0) {
                                                    return (React.createElement(TimelineLogStat_1.TimelineLogStat, { key: i, title: "Set " + (i + 1), text: set.weightActualKg + "kg" }));
                                                }
                                                else {
                                                    return null;
                                                }
                                            })));
                                    })));
                        }
                        break;
                    case "FITNESS":
                        switch (this.props.payload.logSubType) {
                            case "CYCLE":
                                const cycleLog = this.props.payload.data;
                                return (React.createElement(ColouredBox_1.ColouredBox, { backgroundColor: "grayscale1" },
                                    React.createElement(TimelineLogStat_1.TimelineLogStat, { title: "Description", text: cycleLog.description }),
                                    round_1.round(cycleLog.distance / 1000, 2) > 0 ?
                                        React.createElement(TimelineLogStat_1.TimelineLogStat, { title: "Distance", text: round_1.round(cycleLog.distance / 1000, 2) + " km" })
                                        : null,
                                    cycleLog.duration > 0 ?
                                        React.createElement(TimelineLogStat_1.TimelineLogStat, { title: "Duration", text: Math.floor(cycleLog.duration / 60)
                                                + " mins " + cycleLog.duration % 60 + " secs" })
                                        : null,
                                    round_1.round(cycleLog.calories) > 0 ?
                                        React.createElement(TimelineLogStat_1.TimelineLogStat, { title: "Burned", text: round_1.round(cycleLog.calories) + " Cal" })
                                        : null,
                                    cycleLog.rpm > 0 ?
                                        React.createElement(TimelineLogStat_1.TimelineLogStat, { title: "RPM", text: cycleLog.rpm.toString() })
                                        : null));
                            case "RUN":
                                const runLog = this.props.payload.data;
                                return (React.createElement(ColouredBox_1.ColouredBox, { backgroundColor: "grayscale1" },
                                    React.createElement(TimelineLogStat_1.TimelineLogStat, { title: "Description", text: runLog.description }),
                                    round_1.round(runLog.distance / 1000, 2) > 0 ?
                                        React.createElement(TimelineLogStat_1.TimelineLogStat, { title: "Distance", text: round_1.round(runLog.distance / 1000, 2) + " km" })
                                        : null,
                                    runLog.duration > 0 ?
                                        React.createElement(TimelineLogStat_1.TimelineLogStat, { title: "Duration", text: Math.floor(runLog.duration / 60)
                                                + " mins " + runLog.duration % 60 + " secs" })
                                        : null,
                                    round_1.round(runLog.calories) > 0 ?
                                        React.createElement(TimelineLogStat_1.TimelineLogStat, { title: "Burned", text: round_1.round(runLog.calories) + " Cal" })
                                        : null));
                            case "WALK":
                                const walkLog = this.props.payload.data;
                                return (React.createElement(react_native_1.View, null,
                                    React.createElement(TimelineLogStat_1.TimelineLogStat, { title: "Description", text: walkLog.description }),
                                    round_1.round(walkLog.distance / 1000, 2) > 0 ?
                                        React.createElement(TimelineLogStat_1.TimelineLogStat, { title: "Distance", text: round_1.round(walkLog.distance / 1000, 2) + " km" })
                                        : null,
                                    walkLog.duration > 0 ?
                                        React.createElement(TimelineLogStat_1.TimelineLogStat, { title: "Duration", text: Math.floor(walkLog.duration / 60)
                                                + " mins " + walkLog.duration % 60 + " secs" })
                                        : null,
                                    round_1.round(walkLog.calories) > 0 ?
                                        React.createElement(TimelineLogStat_1.TimelineLogStat, { title: "Burned", text: round_1.round(walkLog.calories) + " Cal" })
                                        : null,
                                    walkLog.steps > 0 ?
                                        React.createElement(TimelineLogStat_1.TimelineLogStat, { title: "Walked", text: walkLog.steps + " steps" })
                                        : null));
                            case "SWIM":
                                const swimLog = this.props.payload.data;
                                return (React.createElement(ColouredBox_1.ColouredBox, { backgroundColor: "grayscale1" },
                                    React.createElement(TimelineLogStat_1.TimelineLogStat, { title: "Description", text: swimLog.description }),
                                    round_1.round(swimLog.distance / 1000, 2) > 0 ?
                                        React.createElement(TimelineLogStat_1.TimelineLogStat, { title: "Distance", text: round_1.round(swimLog.distance / 1000, 2) + " km" })
                                        : null,
                                    swimLog.duration > 0 ?
                                        React.createElement(TimelineLogStat_1.TimelineLogStat, { title: "Duration", text: Math.floor(swimLog.duration / 60)
                                                + " mins " + swimLog.duration % 60 + " secs" })
                                        : null,
                                    round_1.round(swimLog.calories) > 0 ?
                                        React.createElement(TimelineLogStat_1.TimelineLogStat, { title: "Burned", text: round_1.round(swimLog.calories) + " Cal" })
                                        : null,
                                    round_1.round(swimLog.poolLengthDistance, 2) > 0 ?
                                        React.createElement(TimelineLogStat_1.TimelineLogStat, { title: "Pool Length", text: round_1.round(swimLog.poolLengthDistance, 2) + " m" })
                                        : null,
                                    swimLog.lengthsSwam > 0 ?
                                        React.createElement(TimelineLogStat_1.TimelineLogStat, { title: "Swam", text: swimLog.lengthsSwam + " lengths" })
                                        : null,
                                    round_1.round(swimLog.averageLengthTime, 2) > 0 ?
                                        React.createElement(TimelineLogStat_1.TimelineLogStat, { title: "Average length time", text: round_1.round(swimLog.averageLengthTime, 2) + " seconds" })
                                        : null));
                            case "ACTIVITY":
                                // This may never be hit if all activities are children in a group
                                const fitnessLog = this.props.payload.data;
                                return (React.createElement(ColouredBox_1.ColouredBox, { backgroundColor: "grayscale1" },
                                    React.createElement(TimelineLogStat_1.TimelineLogStat, { title: "Type", text: fitnessLog.description }),
                                    round_1.round(fitnessLog.distance / 1000, 2) > 0 ?
                                        React.createElement(TimelineLogStat_1.TimelineLogStat, { title: "Distance", text: round_1.round(fitnessLog.distance / 1000, 2) + " km" })
                                        : null,
                                    fitnessLog.duration > 0 ?
                                        React.createElement(TimelineLogStat_1.TimelineLogStat, { title: "Duration", text: Math.floor(fitnessLog.duration / 60)
                                                + " mins " + fitnessLog.duration % 60 + " secs" })
                                        : null,
                                    round_1.round(fitnessLog.calories) > 0 ?
                                        React.createElement(TimelineLogStat_1.TimelineLogStat, { title: "Burned", text: round_1.round(fitnessLog.calories) + " Cal" })
                                        : null));
                            default:
                                break;
                        }
                        break;
                    case "HEARTRATE":
                        const heartRateLog = this.props.payload.data;
                        return (React.createElement(ColouredBox_1.ColouredBox, { backgroundColor: "grayscale1" },
                            React.createElement(TimelineLogStat_1.TimelineLogStat, { title: "Type", text: heartRateLog.description }),
                            heartRateLog.low > 0 ?
                                React.createElement(TimelineLogStat_1.TimelineLogStat, { title: "Low HR", text: round_1.round(heartRateLog.low, 2) + " bpm" })
                                : null,
                            heartRateLog.peak > 0 ?
                                React.createElement(TimelineLogStat_1.TimelineLogStat, { title: "Peak HR", text: round_1.round(heartRateLog.peak, 2) + " bpm" })
                                : null,
                            heartRateLog.average > 0 ?
                                React.createElement(TimelineLogStat_1.TimelineLogStat, { title: "Average HR", text: round_1.round(heartRateLog.average, 2) + " bpm" })
                                : null,
                            heartRateLog.resting > 0 ?
                                React.createElement(TimelineLogStat_1.TimelineLogStat, { title: "Resting HR", text: round_1.round(heartRateLog.resting, 2) + " bpm" })
                                : null,
                            heartRateLog.duration > 0 ?
                                React.createElement(TimelineLogStat_1.TimelineLogStat, { title: "Duration", text: Math.floor(heartRateLog.duration / 60)
                                        + " mins " + heartRateLog.duration % 60 + " secs" })
                                : null));
                    case "ROUTINE":
                        const routineLog = this.props.payload.data;
                        return (React.createElement(ColouredBox_1.ColouredBox, { backgroundColor: "grayscale1" },
                            routineLog.steps > 0 ?
                                React.createElement(TimelineLogStat_1.TimelineLogStat, { title: "Walked", text: routineLog.steps + " steps" })
                                : null,
                            round_1.round(routineLog.distance / 1000, 2) > 0 ?
                                React.createElement(TimelineLogStat_1.TimelineLogStat, { title: "Distance", text: round_1.round(routineLog.distance / 1000, 2) + " km walked" })
                                : null,
                            round_1.round(routineLog.calories) > 0 ?
                                React.createElement(TimelineLogStat_1.TimelineLogStat, { title: "Burned", text: round_1.round(routineLog.calories) + " Cal" })
                                : null));
                    case "WEIGHT":
                        const weightLog = this.props.payload.data;
                        return (React.createElement(ColouredBox_1.ColouredBox, { backgroundColor: "grayscale1" },
                            weightLog.weight > 0 ?
                                React.createElement(TimelineLogStat_1.TimelineLogStat, { title: "Weight", text: round_1.round(weightLog.weight) + " kg" })
                                : null,
                            weightLog.massWeight > 0 ?
                                React.createElement(TimelineLogStat_1.TimelineLogStat, { title: "Muscle Mass", text: round_1.round(weightLog.massWeight) + "%" })
                                : null,
                            weightLog.height > 0 ?
                                React.createElement(TimelineLogStat_1.TimelineLogStat, { title: "Height", text: round_1.round(weightLog.height) + " cm" })
                                : null,
                            weightLog.fatPercent > 0 ?
                                React.createElement(TimelineLogStat_1.TimelineLogStat, { title: "Body fat", text: round_1.round(weightLog.fatPercent) + "%" })
                                : null,
                            weightLog.bmi > 0 ?
                                React.createElement(TimelineLogStat_1.TimelineLogStat, { title: "BMI", text: round_1.round(weightLog.bmi).toString() })
                                : null));
                    case "SLEEP":
                        const sleepLog = this.props.payload.data;
                        return (React.createElement(ColouredBox_1.ColouredBox, { backgroundColor: "grayscale1" },
                            sleepLog.sleepLength > 0 ?
                                React.createElement(TimelineLogStat_1.TimelineLogStat, { title: "Total sleep", text: Math.floor(sleepLog.sleepLength / 3600) + " hours "
                                        + Math.floor((sleepLog.sleepLength % 3600) / 60) + " minutes total sleep " })
                                : null,
                            sleepLog.awake > 0 ?
                                React.createElement(TimelineLogStat_1.TimelineLogStat, { title: "Awake", text: Math.floor(sleepLog.awake / 3600) + " hours "
                                        + Math.floor((sleepLog.awake % 3600) / 60) + " minutes" })
                                : null,
                            sleepLog.deep > 0 ?
                                React.createElement(TimelineLogStat_1.TimelineLogStat, { title: "Deep sleep", text: Math.floor(sleepLog.deep / 3600) + " hours "
                                        + Math.floor((sleepLog.deep % 3600) / 60) + " minutes" })
                                : null,
                            sleepLog.light > 0 ?
                                React.createElement(TimelineLogStat_1.TimelineLogStat, { title: "Light sleep", text: Math.floor(sleepLog.light / 3600) + " hours "
                                        + Math.floor((sleepLog.light % 3600) / 60) + " minutes" })
                                : null,
                            sleepLog.rem > 0 ?
                                React.createElement(TimelineLogStat_1.TimelineLogStat, { title: "REM", text: Math.floor(sleepLog.rem / 3600) + " hours "
                                        + Math.floor((sleepLog.rem % 3600) / 60) + " minutes" })
                                : null,
                            sleepLog.timesWoken > 0 ?
                                React.createElement(TimelineLogStat_1.TimelineLogStat, { title: "Times woken", text: sleepLog.timesWoken.toString() })
                                : null));
                    default:
                        return null;
                }
            })()));
        }
        else {
            return null;
        }
    }
}
exports.TimelineLogDetailsContainer = TimelineLogDetailsContainer;
exports.default = react_redux_1.connect(exports.mapStateToProps)(TimelineLogDetailsContainer);

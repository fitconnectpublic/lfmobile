"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const React = require("react");
const react_1 = require("react");
const react_native_1 = require("react-native");
const DayWeekMonthPickerStyles_1 = require("./DayWeekMonthPickerStyles");
class DayWeekMonthPicker extends react_1.Component {
    render() {
        return (React.createElement(react_native_1.Picker, { style: DayWeekMonthPickerStyles_1.default.root, selectedValue: "days", onValueChange: this.props.onValueChange },
            React.createElement(react_native_1.Picker.Item, { label: "Days", value: "days" }),
            React.createElement(react_native_1.Picker.Item, { label: "Weeks", value: "weeks" }),
            React.createElement(react_native_1.Picker.Item, { label: "Months", value: "months" })));
    }
}
exports.DayWeekMonthPicker = DayWeekMonthPicker;

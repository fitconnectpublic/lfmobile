"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const React = require("react");
const react_1 = require("react");
const react_native_1 = require("react-native");
const TextBoldTitleStyles_1 = require("./TextBoldTitleStyles");
class TextBoldTitle extends react_1.Component {
    render() {
        return (React.createElement(react_native_1.Text, { style: TextBoldTitleStyles_1.TextBoldTitleStyles.title }, this.props.children));
    }
}
exports.TextBoldTitle = TextBoldTitle;

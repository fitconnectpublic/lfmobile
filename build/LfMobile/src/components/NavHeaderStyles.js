"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const react_native_1 = require("react-native");
exports.NavHeaderStyles = react_native_1.StyleSheet.create({
    container: {
        backgroundColor: "#0079C3",
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "center",
        padding: 15,
    },
    button: {
        position: "absolute",
        right: 0,
    },
    heading: {
        color: "#fff",
        fontSize: 20,
        fontWeight: "bold",
    },
    chevron: {
        tintColor: "#fff",
        height: 20,
        width: 20,
    },
});

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const React = require("react");
const react_1 = require("react");
const react_native_1 = require("react-native");
const react_native_svg_1 = require("react-native-svg");
const d3_shape_1 = require("d3-shape");
const VariablesColors_1 = require("./../VariablesColors");
/*tslint:disable */ // TODO Fix once package is fixed. Stop in react-native-svg is missing the offset prop in typings.
const RNSVG = require("react-native-svg");
const Stop = RNSVG.Stop;
class CircularProgressGradient extends react_1.Component {
    render() {
        const radius = 150;
        const circlePath = d3_shape_1.arc()
            .innerRadius(100)
            .outerRadius(115)
            .cornerRadius(20)
            .startAngle(0)
            .endAngle(((this.props.percent / 100) * 2) * (Math.PI));
        // calculate values needed for linearGradient offest
        const chord = (radius * 2 * Math.sin(((this.props.percent / 100) * 2 * (Math.PI)) / 2));
        // height of the arc
        const sagitta = radius + Math.sqrt((Math.pow(radius, 2)) - (Math.pow((chord / 2), 2)));
        return (React.createElement(react_native_1.View, null,
            React.createElement(react_native_svg_1.default, { width: radius * 2, height: radius * 2 },
                React.createElement(react_native_svg_1.Defs, null,
                    React.createElement(react_native_svg_1.LinearGradient, { id: "grad", x1: 0, y1: 0, x2: sagitta / 2, y2: 0 },
                        React.createElement(Stop, { offset: "0", stopColor: this.props.startColor, stopOpacity: "1 " }),
                        React.createElement(Stop, { offset: "1", stopColor: this.props.endColor, stopOpacity: "1" }))),
                React.createElement(react_native_svg_1.Text, { fill: VariablesColors_1.colors.darkGray, fontSize: "50", fontWeight: "bold", x: "150", y: "165", textAnchor: "middle" }, this.props.percent + "%"),
                React.createElement(react_native_svg_1.Circle, { r: 107.5, strokeWidth: "15", fill: "none", stroke: VariablesColors_1.colors.grayscale3, x: radius, y: radius }),
                React.createElement(react_native_svg_1.Path, { d: circlePath(), fill: "url(#grad)", x: radius, y: radius }))));
    }
}
exports.CircularProgressGradient = CircularProgressGradient;

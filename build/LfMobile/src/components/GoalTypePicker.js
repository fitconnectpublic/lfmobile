"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const React = require("react");
const react_1 = require("react");
const react_native_1 = require("react-native");
const GoalTypePickerStyles_1 = require("./GoalTypePickerStyles");
class GoalTypePicker extends react_1.Component {
    render() {
        return (React.createElement(react_native_1.Picker, { style: GoalTypePickerStyles_1.default.root, selectedValue: "CaloriesConsumed", onValueChange: this.props.onValueChange },
            React.createElement(react_native_1.Picker.Item, { label: "Calories Consumed", value: "CaloriesConsumed" }),
            React.createElement(react_native_1.Picker.Item, { label: "Steps", value: "Steps" }),
            React.createElement(react_native_1.Picker.Item, { label: "Sleep", value: "Sleep" }),
            React.createElement(react_native_1.Picker.Item, { label: "Workout Time", value: "WorkoutTime" }),
            React.createElement(react_native_1.Picker.Item, { label: "Number Of Workouts", value: "NumberOfWorkouts" }),
            React.createElement(react_native_1.Picker.Item, { label: "Weight", value: "Weight" }),
            React.createElement(react_native_1.Picker.Item, { label: "BodyFat", value: "BodyFat" })));
    }
}
exports.GoalTypePicker = GoalTypePicker;
